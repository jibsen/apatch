//
// JASG - Just Another Scanner Generator
//
// generates scan tables for simple languages
//
// Copyright 2002-2004 Joergen Ibsen
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

// A word of warning - this was just thrown together to get the
// job done .. i.e. I was not going for the nice-code-award :-)
// It should compile nicely on most decent c++ compilers -- I had
// success with g++, vc++.net, and bcc32 (all includes + .h).

// TODO:
//   - support for float numbers?

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <list>
#include <algorithm>
#include <cstdio>
#include <cctype>
#include <cstring>

using namespace std;

// --- constants and global data -------------------------------------

// predefined accept values
const int NOT_ACCEPTED      = 0;
const int ACCEPT_WHITESPACE = 1;
const int ACCEPT_COMMENT    = 2;
const int ACCEPT_UNKNOWN    = 3;
const int ACCEPT_NEWLINE    = 4;
const int ACCEPT_INTEGER    = 5;
const int ACCEPT_HEX        = 6;
const int ACCEPT_STRING     = 7;
const int ACCEPT_IDENTIFIER = 8;

// predefined character equivalence classes:
const int EC_END        =  0; // end of input
const int EC_UNKNOWN    =  1; // unknown values
const int EC_WHITESPACE =  2; // [ \t\f\r]
const int EC_NEWLINE    =  3; // \n
const int EC_QUOTE      =  4; // "
const int EC_COMMENT    =  5; // /
const int EC_ZERO       =  6; // 0
const int EC_INTEGER    =  7; // [1-9]
const int EC_HEXCHAR    =  8; // [a-fA-F]
const int EC_CHAR       =  9; // [g-zG-Z_]
const int EC_BACKSLASH  = 10; // backslash
const int EC_X          = 11; // [xX]

int next_ec = 12;

int ec[256] = {
	0,  1,  1,  1,  1,  1,  1,  1,  1,  2,  3,  1,  2,  2,  1,  1,
	1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,
	2,  1,  4,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  5,
	6,  7,  7,  7,  7,  7,  7,  7,  7,  7,  1,  1,  1,  1,  1,  1,
	1,  8,  8,  8,  8,  8,  8,  9,  9,  9,  9,  9,  9,  9,  9,  9,
	9,  9,  9,  9,  9,  9,  9,  9, 11,  9,  9,  1, 10,  1,  1,  9,
	1,  8,  8,  8,  8,  8,  8,  9,  9,  9,  9,  9,  9,  9,  9,  9,
	9,  9,  9,  9,  9,  9,  9,  9, 11,  9,  9,  1,  1,  1,  1,  1,
	1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,
	1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,
	1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,
	1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,
	1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,
	1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,
	1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,
	1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1
};

size_t total_size = 0;

int SWITCHa = 0; // use ';' for comments
int SWITCHb = 0; // do not produce base table
int SWITCHc = 0; // case in-sensitive
int SWITCHe = 0; // compact endings
int SWITCHm = 0; // masm style
int SWITCHs = 0; // compact simple states
int SWITCHq = 0; // be quiet

FILE *of = 0;

// --- state data structures -----------------------------------------

struct state;

typedef vector<state*> statevector;

struct state {

	// constructor
	state(const size_t _size, state *_iv, const int _accept, const char *_desc = 0)
		: size(_size), table(_size, _iv), accept(_accept),
		  desc(strdup(_desc)), same_as(0), simple(0)
	{
		/* nothing */
	}

	// methods

	// data
	const size_t size;
	statevector table;
	int accept;
	const char *desc;
	int id;
	int base;
	state *same_as; // state which this state is almost identical to
	int simple;     // flags possibly simple, and index of difference
};

statevector states;

// --- keyword data structures ---------------------------------------

struct keyword {

	// constructor
	keyword(const char *_name, const int _val, const bool _text)
		: name(strdup(_name)), val(_val), text(_text)
	{
		/* nothing */
	}

	// data
	const char *name;
	const int val;
	const bool text; // flags keyword or punctuation symbol
};

typedef vector<keyword*> keywordvector;

keywordvector keywords;

struct keyword_lt {
	inline bool operator()(const keyword *e1, const keyword *e2) const
	{
		return(strcmp(e1->name, e2->name) < 0);
	}
};

typedef list<char> charlist;

charlist usedchars;

// --- data output ---------------------------------------------------

class dataprinter {

public:
	dataprinter(const char *_pre, const char *_format, const size_t _wrap)
		: num(0), wrap(_wrap), pre(_pre), format(_format)
	{
		/* nothing */
	}

	void print(const int val)
	{
		if (num == 0)
		{
			fprintf(of, "\t%s ", pre);
			fprintf(of, format, val);
			num++;
		} else {
			fprintf(of, ", ");
			fprintf(of, format, val);
			num++;
			if (num == wrap)
			{
				fprintf(of, "\n");
				num = 0;
			}
		}
	}

	void end() const
	{
		if (num) fprintf(of, "\n");
	}

protected:
	size_t num;
	const size_t wrap;
	const char *pre;
	const char *format;
};

void print_ec()
{
	fprintf(of, "\nLEX_NUM_EC\tequ %d\n\n", next_ec);

	fprintf(of, SWITCHm ? "lex_ec " : "lex_ec:\n");

	dataprinter dp("db", "%3u", 10);

	for (size_t i = 0; i < 256; ++i) dp.print(ec[i]);

	dp.end();

	total_size += 256;
}

void print_accept()
{
	fprintf(of, SWITCHm ? "\nlex_accept " : "\nlex_accept:\n");

	dataprinter dp("dw", "%u", 16);

	for (statevector::const_iterator s = states.begin(); s != states.end(); ++s)
	{
		dp.print((*s)->accept);
		total_size += 2;
	}

	dp.end();
}

void print_base()
{
	fprintf(of, SWITCHm ? "\nlex_base " : "\nlex_base:\n");

	dataprinter dp("dd", "%u", 10);

	for (statevector::const_iterator s = states.begin(); s != states.end(); ++s)
	{
		dp.print((*s)->base);
		total_size += 4;
	}

	dp.end();
}

void print_state(const state *s)
{
	if (s->desc) fprintf(of, "\t; %s  -  state %d => accept %d\n", s->desc, s->id, s->accept);

	if ((SWITCHs && s->simple) || (SWITCHe && s->same_as)) return;

	dataprinter dp("dw", "%d", 16);

	for (size_t i = 0; i < s->size; ++i)
	{
		const state *ts = s->table[i];

		if (ts)
			dp.print(ts->id);
		else
			dp.print(-1);

		total_size += 2;
	}

	dp.end();
}

void print_check()
{
	fprintf(of, SWITCHm ? "\nlex_check " : "\nlex_check:\n");

	dataprinter dp("dw", "%u", 10);

	for (statevector::const_iterator s = states.begin(); s != states.end(); ++s)
	{
		if ((*s)->simple)
		{
			dp.print((*s)->simple);
		} else {
			dp.print(0);
		}

		total_size += 2;
	}

	dp.end();
}

void print_snext()
{
	fprintf(of, SWITCHm ? "\nlex_snext " : "\nlex_snext:\n");

	dataprinter dp("dw", "%d", 10);

	for (statevector::const_iterator s = states.begin(); s != states.end(); ++s)
	{
		const state *ts = (*s)->table[(*s)->simple];

		if (ts)
			dp.print(ts->id);
		else
			dp.print(-1);

		total_size += 2;
	}

	dp.end();
}

void print_next()
{
	fprintf(of, SWITCHm ? "\nlex_next " : "\nlex_next:\n");

	for (statevector::const_iterator s = states.begin(); s != states.end(); ++s)
	{
		if ((*s)->id)
		{
			print_state(*s);
		} else {
			if (!SWITCHm) fprintf(of, "\t; NULL state\n");

			dataprinter dp("dw", "%d", 16);

			for (size_t i = 0; i < (*s)->size; ++i) dp.print(0);

			total_size += 2*(*s)->size;

			dp.end();
		}
	}
}

void print_tables()
{
	if (SWITCHb) fprintf(of, SWITCHm ? "\nLEX_NOBASE equ 1" : "\n%%define LEX_NOBASE\n");
	if (SWITCHs) fprintf(of, SWITCHm ? "\nLEX_SIMPLE equ 1" : "\n%%define LEX_SIMPLE\n");

	print_ec();
	print_accept();
	if (!SWITCHb) print_base();
	if (SWITCHs)
	{
		print_check();
		print_snext();
	}
	print_next();
}

// --- misc ----------------------------------------------------------

void show_syntax(void)
{
	printf("Licensed under the Apache License, Version 2.0.\n\n"
	       "  Syntax:  jasg <keyword file> [output file]\n\n"
	       "  Options:\n"
	       "    -a     use ';' for comments\n"
	       "    -c     case in-sensitive\n"
	       "    -m     create masm compatible output (default is nasm)\n"
	       "    -q     be quiet\n\n"
	       "  Size/speed related options:\n"
	       "    -b     do not produce base table\n"
	       "    -e     compact endings (not together with -b)\n"
	       "    -s     compact simple states (not together with -b)\n\n"
	       "Output file defaults to 'lextab.inc'.\n\n");
}

void show_banner(void)
{
	printf("JASG - Just Another Scanner Generator (version 2004.06.02)\n"
	       "Copyright 2002-2004 Joergen Ibsen (www.ibsensoftware.com)\n\n");
}

// --- main ----------------------------------------------------------

int main(int argc, char *argv[])
{
	if (argc < 2)
	{
		show_banner();
		show_syntax();
		return 1;
	}

	int infilearg = 0, outfilearg = 0;

	// parse command line
	for (int i = 1; i < argc; i++)
	{
		const char *arg = argv[i];
		if ((*arg == '-') || (*arg == '/'))
		{
			while (*(++arg))
			{
				switch (*arg)
				{
				case '?':
				case 'h':
				case 'H':
					show_syntax();
					return 0;
					break;
				case 'a':
				case 'A':
					SWITCHa = 1;
					break;
				case 'b':
				case 'B':
					SWITCHb = 1;
					break;
				case 'c':
				case 'C':
					SWITCHc = 1;
					break;
				case 'e':
				case 'E':
					SWITCHe = 1;
					break;
				case 'm':
				case 'M':
					SWITCHm = 1;
					break;
				case 's':
				case 'S':
					SWITCHs = 1;
					break;
				case 'q':
				case 'Q':
					SWITCHq = 1;
					break;
				default :
					show_syntax();
					printf("ERR: unknown switch '-%c'\n", *arg);
					return 1;
				}
			}
		} else {
			if (infilearg != 0)
			{
				if (outfilearg == 0) outfilearg = i;
			} else infilearg = i;
		}
	}

	if (!SWITCHq) show_banner();

	if (SWITCHe || SWITCHs)
	{
		if (SWITCHb)
		{
			if (!SWITCHq) printf("- switch '-b' ignored\n");
			SWITCHb = 0;
		}
	}

	if (SWITCHa)
	{
		ec[(unsigned char)'/'] = EC_UNKNOWN;
		ec[(unsigned char)';'] = EC_COMMENT;
	}

	if (infilearg == 0)
	{
		show_syntax();
		return 1;
	}

	if (outfilearg)
		of = fopen(argv[outfilearg], "w");
	else
		of = fopen("lextab.inc", "w");

	if (!of)
	{
		printf("ERR: unable to open output file\n");
		return 1;
	}

	if (!SWITCHq) printf("- reading keywords\n");

	{
		ifstream from(argv[infilearg]);

		if (!from)
		{
			printf("ERR: load: unable to open file\n");
			return 1;
		}

		int num_keywords = 0;
		string name;
		int val;

		while (from >> name >> val)
		{
			const char *cp = name.c_str();
			keywords.push_back(new keyword(cp, val,
			                               (ec[(unsigned int)cp[0]] == EC_HEXCHAR) ||
			                               (ec[(unsigned int)cp[0]] == EC_CHAR) ||
			                               (ec[(unsigned int)cp[0]] == EC_X)));
			num_keywords++;
		}
		if (!SWITCHq) printf("   - %d keywords read\n", num_keywords);
	}

	// sort keywords (will make sure common prefixes are added first)
	sort(keywords.begin(), keywords.end(), keyword_lt());

	// associate equivalence classes with new characters
	for (keywordvector::const_iterator k = keywords.begin(); k != keywords.end(); ++k)
	{
		for (const unsigned char *c = (unsigned char *)(*k)->name; *c; ++c)
		{
			const unsigned char lc = SWITCHc ? tolower(*c) : *c;

			if ((ec[lc] == EC_UNKNOWN) || (ec[lc] == EC_HEXCHAR) || (ec[lc] == EC_CHAR) || (ec[lc] == EC_X))
			{
				if (SWITCHc)
				{
					ec[toupper(lc)] = ec[lc] = next_ec++;
					if ((*k)->text)
					{
						usedchars.push_back(toupper(lc));
						usedchars.push_back(lc);
					}
				} else {
					ec[lc] = next_ec++;
					if ((*k)->text) usedchars.push_back(lc);
				}
			}
		}
	}

	if (!SWITCHq) printf("- adding default states\n");

	// --- main states ---
	state *null_state = new state(next_ec, 0, NOT_ACCEPTED, "NULL");
	state *unknown_state = new state(next_ec, 0, ACCEPT_UNKNOWN, "unknown");
	state *main_state = new state(next_ec, unknown_state, NOT_ACCEPTED, "main");

	// all looping entries for unknown_state are added later

	main_state->table[0] = null_state;

	states.push_back(null_state);
	states.push_back(main_state);
	states.push_back(unknown_state);

	// --- newline state ---
	state *newline_state = new state(next_ec, 0, ACCEPT_NEWLINE, "newline");

	main_state->table[EC_NEWLINE] = newline_state;

	states.push_back(newline_state);

	// --- whitespace state ---
	state *ws_state = new state(next_ec, 0, ACCEPT_WHITESPACE, "whitespace");

	ws_state->simple = 1; // mark as possibly simple
	ws_state->same_as = newline_state;

	ws_state->table[EC_WHITESPACE] = ws_state;

	main_state->table[EC_WHITESPACE] = ws_state;

	states.push_back(ws_state);

	// --- string states ---
	{
		state *quote_state = new state(next_ec, null_state, NOT_ACCEPTED, "string start");
		state *escape_state = new state(next_ec, quote_state, NOT_ACCEPTED, "string escape");
		state *endquote_state = new state(next_ec, 0, ACCEPT_STRING, "string end");

		fill(quote_state->table.begin(), quote_state->table.end(), quote_state);

		quote_state->table[0] = null_state;
		quote_state->table[EC_QUOTE] = endquote_state;
		quote_state->table[EC_BACKSLASH] = escape_state;

		escape_state->table[0] = null_state;

		endquote_state->same_as = newline_state;

		main_state->table[EC_QUOTE] = quote_state;

		states.push_back(quote_state);
		states.push_back(escape_state);
		states.push_back(endquote_state);
	}

	// --- comment states ---
	if (SWITCHa)
	{
		state *comment_state = new state(next_ec, 0, ACCEPT_COMMENT, "comment");

		fill(comment_state->table.begin(), comment_state->table.end(), comment_state);

		comment_state->table[EC_END] = 0;
		comment_state->table[EC_NEWLINE] = 0;

		main_state->table[EC_COMMENT] = comment_state;

		states.push_back(comment_state);

	} else {

		state *slash_state = new state(next_ec, unknown_state, NOT_ACCEPTED, "slash");
		state *comment_state = new state(next_ec, 0, ACCEPT_COMMENT, "comment");

		slash_state->table[0] = null_state;
		slash_state->table[EC_COMMENT] = comment_state;

		fill(comment_state->table.begin(), comment_state->table.end(), comment_state);

		comment_state->table[EC_END] = 0;
		comment_state->table[EC_NEWLINE] = 0;

		main_state->table[EC_COMMENT] = slash_state;

		states.push_back(slash_state);
		states.push_back(comment_state);
	}

	// --- integer state ---
	state *num_state = new state(next_ec, 0, ACCEPT_INTEGER, "integer");

	num_state->table[EC_ZERO] = num_state;
	num_state->table[EC_INTEGER] = num_state;

	main_state->table[EC_INTEGER] = num_state;

	states.push_back(num_state);

	// --- hex state ---
	state *hex_state = new state(next_ec, 0, ACCEPT_HEX, "hex");

	hex_state->table[EC_ZERO] = hex_state;
	hex_state->table[EC_INTEGER] = hex_state;

	for (size_t i = (unsigned char)'a'; i <= (unsigned char)'f'; ++i) hex_state->table[ec[i]] = hex_state;
	for (size_t i = (unsigned char)'A'; i <= (unsigned char)'F'; ++i) hex_state->table[ec[i]] = hex_state;

	states.push_back(hex_state);

	// --- zero state ---
	state *zero_state = new state(next_ec, 0, ACCEPT_INTEGER, "zero");

	zero_state->table[EC_ZERO] = num_state;
	zero_state->table[EC_INTEGER] = num_state;

	zero_state->table[ec[(unsigned char)'x']] = hex_state;
	zero_state->table[ec[(unsigned char)'X']] = hex_state;

	main_state->table[EC_ZERO] = zero_state;

	states.push_back(zero_state);

	// --- identifier state ---
	state *id_state = new state(next_ec, 0, ACCEPT_IDENTIFIER, "identifier");

	id_state->table[EC_ZERO] = id_state;
	id_state->table[EC_INTEGER] = id_state;

	for (size_t i = (unsigned char)'a'; i <= (unsigned char)'z'; ++i) id_state->table[ec[i]] = id_state;
	for (size_t i = (unsigned char)'A'; i <= (unsigned char)'Z'; ++i) id_state->table[ec[i]] = id_state;
	id_state->table[ec[(unsigned char)'_']] = id_state;

	main_state->table[EC_HEXCHAR] = id_state;
	main_state->table[EC_CHAR] = id_state;
	main_state->table[EC_X] = id_state;

	states.push_back(id_state);

	// ===============================================================
	//
	//  add keyword states
	//
	// ===============================================================

	if (!SWITCHq) printf("- adding keywords\n");

	state *first_state_1 = id_state;
	state *first_state_2 = newline_state;

	for (keywordvector::const_iterator k = keywords.begin(); k != keywords.end(); ++k)
	{
		state *current = main_state;

		if ((*k)->text)
		{
			for (const unsigned char *c = (unsigned char *)(*k)->name; *c; ++c)
			{
				if ((current->table[ec[*c]] == id_state) ||
				        (current->table[ec[*c]] == unknown_state) ||
				        (current->table[ec[*c]] == 0))
				{
					char desc[] = "x\0";
					desc[0] = *c;
					const bool last = *(c + 1) == '\0';
					state *new_state = new state(next_ec, 0, last ? (*k)->val : ACCEPT_IDENTIFIER, desc);

					if (last)
					{
						// end state of keywords might be identical to id
						if (first_state_1)
						{
							new_state->same_as = first_state_1;
						} else {
							first_state_1 = new_state;
						}
					} else {
						// states inside keywords might be simple
						new_state->same_as = id_state;
						new_state->simple = 1; // mark as possibly simple
					}

					current->table[ec[*c]] = new_state;

					// set all text entries which are not EC_(HEX)CHAR to id_state
					for (charlist::const_iterator u = usedchars.begin(); u != usedchars.end(); ++u)
					{
						new_state->table[ec[(unsigned char)*u]] = id_state;
					}

					new_state->table[EC_HEXCHAR] = id_state;
					new_state->table[EC_CHAR] = id_state;
					new_state->table[EC_X] = id_state;
					new_state->table[EC_ZERO] = id_state;
					new_state->table[EC_INTEGER] = id_state;

					states.push_back(new_state);

					current = new_state;

				} else {

					state *next_state = current->table[ec[*c]];

					if (*(c + 1) == '\0') current->accept = (*k)->val;

					current = next_state;
				}
			}

		} else {

			for (const unsigned char *c = (unsigned char *)(*k)->name; *c; ++c)
			{
				if ((current->table[ec[*c]] == id_state) ||
				        (current->table[ec[*c]] == unknown_state) ||
				        (current->table[ec[*c]] == 0))
				{
					char desc[] = "x\0";
					desc[0] = *c;
					const bool last = *(c + 1) == '\0';
					state *new_state = new state(next_ec, last ? 0 : unknown_state, last ? (*k)->val : NOT_ACCEPTED, desc);

					if (last)
					{
						// end state of ps might be identical to zero
						if (first_state_2)
						{
							new_state->same_as = first_state_2;
						} else {
							first_state_2 = new_state;
						}
					} else {
						// states inside ps might be simple
						new_state->same_as = zero_state;
						new_state->simple = 1; // mark as possibly simple
					}

					current->table[ec[*c]] = new_state;

					states.push_back(new_state);

					current = new_state;

				} else {

					state *next_state = current->table[ec[*c]];

					if (*(c + 1) == '\0') current->accept = (*k)->val;

					current = next_state;
				}
			}
		}
	}

	// ===============================================================
	//
	//  finish up, build tables, and output them
	//
	// ===============================================================

	if (!SWITCHq) printf("- writing tables\n");

	// set id_state->table[ec[c]] = id_state; for all c in keywords
	for (charlist::const_iterator u = usedchars.begin(); u != usedchars.end(); ++u)
	{
		id_state->table[ec[(unsigned char)*u]] = id_state;
		if (main_state->table[ec[(unsigned char)*u]] == unknown_state) main_state->table[ec[(unsigned char)*u]] = id_state;
	}

	// let all unused symbols loop in unknown_state
	for (size_t i = 0; i < main_state->size; ++i) if (main_state->table[i] == unknown_state) unknown_state->table[i] = unknown_state;

	// re-check identical and simple states
	for (statevector::iterator s = states.begin(); s != states.end(); ++s)
	{
		if ((*s)->simple && SWITCHs)
		{
			const state *sas = (*s)->same_as;

			int diffs = 0;
			int pos = 0;

			for (size_t i = 0; i < (*s)->size; ++i)
			{
				if ((*s)->table[i] != sas->table[i])
				{
					pos = i;
					++diffs;
				}
			}

			if ((diffs == 1) && pos)
			{
				(*s)->simple = pos;
			} else {
				(*s)->simple = 0;
				(*s)->same_as = 0;
			}

		} else {

			(*s)->simple = 0;

			if ((*s)->same_as && SWITCHe)
			{
				const state *sas = (*s)->same_as;

				int diffs = 0;

				for (size_t i = 0; i < (*s)->size; ++i)
				{
					if ((*s)->table[i] != sas->table[i]) ++diffs;
				}

				if (diffs) (*s)->same_as = 0;

			} else {

				(*s)->same_as = 0;
			}
		}
	}

	// base states
	{
		int next_base = 0;

		for (statevector::iterator s = states.begin(); s != states.end(); ++s)
		{
			if ((SWITCHs && (*s)->simple) || (SWITCHe && (*s)->same_as))
			{
				(*s)->base = (*s)->same_as->base;
			} else {
				(*s)->base = 2*next_base;
				next_base += next_ec;
			}
		}
	}

	// enumerate states
	{
		int next_id = 0;

		for (statevector::iterator s = states.begin(); s != states.end(); ++s)
		{
			(*s)->id = next_id;
			next_id += 2;
		}
	}

	fprintf(of, "; scanner tables  -  automagically generated by JASG\n"
	        ";\n"
	        "; JASG is Copyright 2002-2004 Joergen Ibsen\n"
	        ";\n"
	        "; http://www.ibsensoftware.com/\n");

	print_tables();

	if (!SWITCHq) printf("   - %d bytes used\n", total_size);

	fclose(of);

	// free mem
	for (keywordvector::iterator k = keywords.begin(); k != keywords.end(); ++k)
		delete *k;
	for (statevector::iterator s = states.begin(); s != states.end(); ++s)
		delete *s;

	if (!SWITCHq) printf("- done\n");

	return 0;
}
