/*
 * JASG - Just Another Scanner Generator
 *
 * FASM scanner (lexer)
 *
 * Copyright 2002-2006 Joergen Ibsen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LEX_H_INCLUDED
#define LEX_H_INCLUDED

/* calling convention */
#ifndef LEXCC
# define LEXCC __cdecl
#endif

#ifdef __cplusplus
extern "C" {
#endif

/* prototypes */

void LEXCC JASG_init(char *buffer);

int  LEXCC JASG_lex();

void LEXCC JASG_restart(char *buffer);

extern char *lex_ptr;

extern int lex_len;

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* LEX_H_INCLUDED */
