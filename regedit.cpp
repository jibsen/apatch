//
// apatch - simple patching engine
//
// regedit import
//
// Copyright 1999-2014 Joergen Ibsen
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include <fstream>
#include <string>
#include <cstdlib>
#include <cstdio>

#include "regedit.hpp"

using namespace std;

namespace {

// error handler
void regedit_error(char *what)
{
	printf("\n\007[import] error : %s\n", what);

	exit(-1);
}

void regedit_error_line(char *what, int line)
{
	printf("\n\007[import] reg parser error at line %d : %s\n", line, what);

	exit(-1);
}

void remove_quotes(string &s)
{
	s = s.substr(s.find('"') + 1, s.rfind('"') - 1);
}

void fix_escaped_quotes(string &s)
{
	bool escaped = false;

	string res;

	res.reserve(s.size());

	for (string::iterator i = s.begin(); i != s.end(); ++i)
	{
		if (escaped)
		{
			escaped = false;

			switch (*i)
			{
			case '"':
				res.push_back('"');
				break;
			case '\\':
				res.push_back('\\');
				break;
			default:
				res.push_back('\\');
				res.push_back(*i);
				break;
			}

		} else {

			if (*i == '\\')
			{
				escaped = true;

			} else {

				res.push_back(*i);
			}
		}
	}

	if (escaped) res.push_back('\\');

	s.swap(res);
}

void parse_and_add_bytes(vector<char> &data, const string &s)
{
	const char *p = s.c_str();

	unsigned int value;

	while (sscanf(p, "%x", &value) == 1)
	{
		data.push_back((unsigned char)strtoul(p, 0, 16));

		while (*p && (*p != ',')) ++p;
		if (*p) ++p;
	}
}

}

std::vector<apatch::regkey_t> apatch::import_regedit(const char *name)
{
	ifstream from(name);

	if (!from) regedit_error("unable to open file");

	vector<regkey_t> result;

	vector<regval_t> values;

	unsigned int line = 1;

	string s;

	if (!getline(from, s)) regedit_error("unable to read from file");

	if (strncmp("REGEDIT4", s.c_str(), 8) != 0)
	{
		regedit_error("no REGEDIT4 tag found");
	}

	regkey_t key;
	regval_t val;

	bool continued = false;

	while (getline(from, s))
	{
		++line;

		// skip empty lines
		if (s == "") continue;

		if (s[0] == '[')
		{
			if (!values.empty())
			{
				result.push_back(key);
				result.back().values.swap(values);
			}

			string::size_type idx = s.rfind(']');

			if (idx == string::npos) regedit_error_line("subkey", line);

			s = s.substr(1, idx - 1);

			idx = s.find('\\');

			if (idx == string::npos) regedit_error_line("no base found", line);

			string base = s.substr(0, idx);

			if (base == "HKEY_CLASSES_ROOT")
			{
				key.base = 0;
			} else if (base == "HKEY_CURRENT_USER") {
				key.base = 1;
			} else if (base == "HKEY_LOCAL_MACHINE") {
				key.base = 2;
			} else if (base == "HKEY_USERS") {
				key.base = 3;
			} else {
				regedit_error_line("invalid base", line);
			}

			key.subkey = s.substr(idx + 1);

			continued = false;

		} else {

			if (continued)
			{
				continued = false;

				parse_and_add_bytes(values.back().D, s);
				if (s.find('\\') != string::npos) continued = true;

			} else {

				string::size_type idx = s.find('=');

				if (idx == string::npos) regedit_error_line("value", line);

				string name = s.substr(0, idx);
				string value = s.substr(idx + 1);
				int mode = 1;

				if (value == "") regedit_error_line("no value", line);

				if (value[0] != '"')
				{
					idx = value.find(':');

					if (idx != string::npos)
					{
						string type = value.substr(0, idx);
						value = value.substr(idx + 1);

						if (type == "dword") mode = 0;
						else if (type == "hex") mode = 2;
						else regedit_error_line("unknown value type", line);
					}
				}

				if (name[0] == '@') name = "";
				else remove_quotes(name);

				val.name = name;

				switch (mode)
				{
				case 0:
					val.kind = regval_t::dwordK;
					val.I = strtoul(value.c_str(), 0, 16);
					values.push_back(val);
					break;
				case 1:
					val.kind = regval_t::stringK;
					remove_quotes(value);
					fix_escaped_quotes(value);
					val.S = value;
					values.push_back(val);
					break;
				case 2:
					val.kind = regval_t::binaryK;
					values.push_back(val);
					parse_and_add_bytes(values.back().D, value);
					if (value.find('\\') != string::npos) continued = true;
					break;
				}
			}
		}
	}

	if (!values.empty())
	{
		result.push_back(key);
		result.back().values.swap(values);
	}

	return result;
}
