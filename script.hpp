//
// apatch - simple patching engine
//
// SCRIPT struct
//
// Copyright 1999-2014 Joergen Ibsen
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef SCRIPT_HPP_INCLUDED
#define SCRIPT_HPP_INCLUDED

#include <vector>

#include "statement.hpp"

struct SCRIPT {

	// constructor
	SCRIPT()
		: statements(0)
	{
		buffer.reserve(32768);
		text_color = 0x00A0D0A0;
		bk_color = 0x00000000;
		dlgw = 60;
		dlgh = 24;
	}

	// destructor (make sure to remove all statements)
	~SCRIPT() {
		if (statements) delete statements;
	}

	std::vector<char> buffer;

	std::vector<char> code;

	std::vector<char> data;

	std::vector<bool> used_codes;

	STATEMENT *statements;

	unsigned int text_color;
	unsigned int bk_color;
	unsigned int dlgw;
	unsigned int dlgh;
};

#endif // SCRIPT_HPP_INCLUDED
