//
// apatch - simple patching engine
//
// file comparison
//
// Copyright 1999-2014 Joergen Ibsen
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include <algorithm>
#include <fstream>
#include <string>
#include <cstdlib>
#include <cstdio>

#include "filecmp.hpp"

using namespace std;

namespace {

const size_t BUFFER_SIZE = 1024*1024;

// error handler
void cmp_error(char *what)
{
	printf("\n\007[compare] error : %s\n", what);

	exit(-1);
}

void cmp_error_line(char *what, int line)
{
	printf("\n\007[compare] fc parser error at line %d : %s\n", line, what);
}

}

vector<apatch::diff_t> apatch::file_compare(const char *name1, const char *name2)
{
	vector<char> buffer1(BUFFER_SIZE);

	vector<char> buffer2(BUFFER_SIZE);

	FILE *file1 = fopen(name1, "rb");

	if (file1 == 0) cmp_error("unable to open source file");

	fseek(file1, 0, SEEK_SET);

	FILE *file2 = fopen(name2, "rb");

	if (file2 == 0) cmp_error("unable to open destination file");

	fseek(file2, 0, SEEK_SET);

	size_t num_read1, num_read2;

	bool inside_diff = false;

	size_t offs = 0, diff_offs = 0;

	vector<char> diffs;
	vector<char> equals;

	vector<diff_t> result;

	while (((num_read1 = fread(&buffer1[0], 1, BUFFER_SIZE, file1)) > 0) &&
	        ((num_read2 = fread(&buffer2[0], 1, BUFFER_SIZE, file2)) > 0))
	{
		size_t num_read = min(num_read1, num_read2);

		for (size_t i = 0; i < num_read; ++i)
		{
			if (buffer1[i] == buffer2[i])
			{
				if (inside_diff)
				{
					equals.push_back(buffer1[i]);
					inside_diff = false;
				} else {
					if (!diffs.empty())
					{
						equals.push_back(buffer1[i]);

						// if the gap between two differences is greater
						// than the code for offset + write, then we
						// output the difference
						if (equals.size() > 14)
						{
							diff_t d;

							d.offset = diff_offs;

							result.push_back(d);

							result.back().bytes.swap(diffs);

							equals.clear();
						}
					}
				}

			} else {

				if (!inside_diff)
				{
					if (diffs.empty())
					{
						diff_offs = offs;
					} else {
						// the gap between the last difference and this
						// was less than the code for offset + write, so
						// we append the equal bytes to the last difference
						// and continue on that
						diffs.insert(diffs.end(), equals.begin(), equals.end());
						equals.clear();
					}
					inside_diff = true;
				}
				diffs.push_back(buffer2[i]);
			}

			/* the simple version
				   if (buffer1[i] == buffer2[i])
				   {
				       if (diff)
				       {
					   diff_t d;

					   d.offset = diff_offs;

					   result.push_back(d);

					   result.back().bytes.swap(diffs);

					   diff = false;
				       }
				   } else {
				       if (!diff)
				       {
					   diff_offs = offs;
					   diff = true;
				       }
				       diffs.push_back(buffer2[i]);
				   }
			*/

			++offs;
		}
	}

	if (!diffs.empty())
	{
		diff_t d;

		d.offset = diff_offs;

		result.push_back(d);

		result.back().bytes.swap(diffs);
	}

	fclose(file2);
	fclose(file1);

	return result;
}

std::vector<apatch::diff_t> apatch::file_compare_fc(const char *name)
{
	ifstream from(name);

	if (!from) cmp_error("unable to open file");

	vector<diff_t> result;

	vector<char> diffs;

	unsigned int start_offs = -2;
	unsigned int prev_offs  = -2;

	unsigned int line = 0;

	string s;

	while (getline(from, s))
	{
		++line;

		unsigned int new_offs, old_val, new_val;

		if (sscanf(s.c_str(), "%x: %x %x",
		           &new_offs, &old_val, &new_val) != 3) continue;

		if (new_offs != prev_offs + 1)
		{
			if (!diffs.empty())
			{
				diff_t d;

				d.offset = start_offs;

				result.push_back(d);

				result.back().bytes.swap(diffs);

			}

			start_offs = new_offs;
		}

		prev_offs = new_offs;

		diffs.push_back((unsigned char)new_val);
	}

	if (!diffs.empty())
	{
		diff_t d;

		d.offset = start_offs;

		result.push_back(d);

		result.back().bytes.swap(diffs);
	}

	return result;
}
