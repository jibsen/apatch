//
// apatch - simple patching engine
//
// misc
//
// Copyright 1999-2014 Joergen Ibsen
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef UTIL_HPP_INCLUDED
#define UTIL_HPP_INCLUDED

#include <vector>
#include <string>

namespace apatch {

inline unsigned int get_dword(char *data)
{
	return *((unsigned int *)data);
}

inline void set_dword(char *data, unsigned int val)
{
	*((unsigned int *)data) = val;
}

void fix_string(std::string &s);

void append_data(std::vector<char> &v, const char *data, int len);

void append_dword(std::vector<char> &v, unsigned int n);

}

#endif // UTIL_HPP_INCLUDED
