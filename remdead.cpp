//
// apatch - simple patching engine
//
// remove dead code from patch
//
// Copyright 1999-2014 Joergen Ibsen
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include <iostream>
#include <string>

#include "remdead.hpp"

namespace {

using namespace std;
using namespace apatch;

// error handler
void remdead_error(char *what, unsigned int num)
{
	printf("\n\007[rd] error : %s (%u)\n", what, num);

	exit(-1);
}

bool remove_tagged_code(char *data, unsigned int size, char id)
{
	unsigned int tag = 0x29002128 | (id << 16);

	const char filler[] = { 'a', 'p', 'a', 't', 'c', 'h', 0 };

	unsigned int start = 0, end = 0;

	bool made_change = false;

	while (true)
	{
		start = 0;

		for (unsigned int i = end + 1; i < size - 4; ++i)
		{
			if (*(unsigned int *)(data + i) == tag)
			{
				start = i;
				break;
			}
		}

		if (start == 0) return made_change;

		end = 0;

		for (unsigned int i = start + 1; i < size - 4; ++i)
		{
			if (*(unsigned int *)(data + i) == tag)
			{
				end = i;
				break;
			}
		}

		if (end == 0)
		{
			remdead_error("phase error", id);
			return false;
		}

		for (unsigned int i = start; i < end + 4; ++i)
		{
			data[i] = filler[i % (sizeof(filler)/sizeof(filler[0]))];
		}

		made_change = true;
	}
}

bool remove_tag(char *data, unsigned int size, char id)
{
	unsigned int tag = 0x29002128 | (id << 16);

	unsigned int start = 0, end = 0;

	bool made_change = false;

	while (true)
	{
		start = 0;

		for (unsigned int i = end + 1; i < size - 8; ++i)
		{
			if (*(unsigned int *)(data + i) == tag)
			{
				start = i;
				break;
			}
		}

		if (start == 0) return made_change;

		*(unsigned int *)(data + start) = 0x90909090;

		end = 0;

		for (unsigned int i = start + 4; i < size - 4; ++i)
		{
			if (*(unsigned int *)(data + i) == tag)
			{
				end = i;
				break;
			}
		}

		if (end == 0)
		{
			remdead_error("phase error", id);
			return false;
		}

		*(unsigned int *)(data + end) = 0x90909090;

		made_change = true;
	}
}
}

void apatch::remdead::remove_dead(vector<char> &stub,
                                  const vector<bool> &used,
                                  unsigned int num)
{
	for (unsigned int i = 0; i < num; ++i)
	{
		if (used[i])
		{
			remove_tag(&stub[0], stub.size(), i);

		} else {

			remove_tagged_code(&stub[0], stub.size(), i);
		}
	}
}
