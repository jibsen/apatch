;;
;; JASG - Just Another Scanner Generator
;;
;; FASM scanner (lexer)
;;
;; Copyright 2002-2006 Joergen Ibsen
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.
;;

format MS COFF

public JASG_init as '_JASG_init'
public JASG_lex as '_JASG_lex'
public JASG_restart as '_JASG_restart'

public lex_ptr as '_lex_ptr'
public lex_len as '_lex_len'

; =============================================================

section '.data' data readable writable

lex_ptr    dd 0
lex_len    dd 0

la_state   dd 0
la_ptr     dd 0
last_char  db 0

include 'lextab.inc'

; =============================================================

section '.text' code readable executable

JASG_init:
    ; void JASG_init(char *script);

    .scrt$ equ 4

    mov    edx, [esp + .scrt$] ; edx -> script[]

    mov    al, [edx]          ;
    mov    [last_char], al    ; last_char = script[0]

    dec    edx

    mov    [la_ptr], edx      ; set initial position to scan

    ret

; =============================================================

JASG_lex:
    ; int JASG_lex();

    push   esi
    push   edi
    push   ebx

    mov    esi, [la_ptr]      ; esi -> where to continue scanning

    inc    esi

    mov    al, [last_char]    ; restore last character of previous match
    mov    [esi], al          ;

  .lex_loop:

    mov    [lex_ptr], esi     ; lex_ptr -> what we are about to scan

    mov    ebx, 2             ; ebx = main state

    jmp    short .enter_scan_loop

  .scan_loop:
    cmp    word [lex_accept + ebx], 0
    je     short .accept_done

    mov    [la_state], ebx    ; save last accepting state and pos
    mov    [la_ptr], esi      ;

  .accept_done:
    inc    esi                ; advance to next character

  .enter_scan_loop:
    movzx  eax, byte [esi]    ; get character

    mov    edx, ebx           ; edx = prev_state

    movzx  eax, byte [lex_ec + eax] ; get equivalence class

if defined LEX_SIMPLE
    cmp    [lex_check + ebx], ax ; check if simple
    jne    short .not_simple

    movsx  ebx, word [lex_snext + ebx] ; ebx = next state

    jmp    short .check_state
end if

  .not_simple:
    add    eax, eax           ; eax = word offset

if defined LEX_NOBASE
    imul   ebx, ebx, LEX_NUM_EC ; ebx = base offset of state
else
    mov    ebx, [lex_base + 2*ebx] ; ebx = base offset of state
end if

    movsx  ebx, word [lex_next + ebx + eax] ; ebx = next state

  .check_state:
    cmp    ebx, 0             ; should we continue scanning?
    jg     short .scan_loop   ;

    mov    eax, esi           ; lex_len = length of match
    sub    eax, [lex_ptr]     ;
    mov    [lex_len], eax     ;

    movzx  eax, word [lex_accept + edx] ; eax = lex_accept[prev_state]

    cmp    eax, 3             ; return accept value if >= 3
    jge    short .return_eax  ; (replace with 2 to return comments)

    test   eax, eax           ; continue on 1 (= whitespaces)
    jnz    short .lex_loop    ;

  .return_zero:
    xor    eax, eax

  .return_eax:

    mov    dl, [esi]          ; zero-terminate lex_ptr, and
    mov    byte [esi], 0      ; save character at end
    mov    [last_char], dl    ;

    pop    ebx
    pop    edi
    pop    esi

    ret

; =============================================================

JASG_restart:
    ; void JASG_restart(char *script);

    .scrt$ equ 4

    mov    edx, [la_ptr]      ; edx -> where scanning ended

    inc    edx

    mov    al, [last_char]    ; restore last character of previous match
    mov    [edx], al          ;

    mov    edx, [esp + .scrt$] ; edx -> script[]

    mov    al, [edx]
    mov    [last_char], al    ; last_char = script[0]

    dec    edx

    mov    [la_ptr], edx      ; set initial position to scan

    ret

; =============================================================
