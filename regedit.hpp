//
// apatch - simple patching engine
//
// regedit import
//
// Copyright 1999-2014 Joergen Ibsen
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef REGEDIT_HPP_INCLUDED
#define REGEDIT_HPP_INCLUDED

#include <vector>
#include <string>

namespace apatch {

struct regval_t {
	std::string name;
	enum { dwordK = 0, stringK, binaryK } kind;
	unsigned int I;
	std::string S;
	std::vector<char> D;
};

struct regkey_t {
	unsigned int base;
	std::string subkey;
	std::vector<regval_t> values;
};

std::vector<regkey_t> import_regedit(const char *name);

}

#endif // REGEDIT_HPP_INCLUDED
