//
// apatch - simple patching engine
//
// emitter
//
// Copyright 1999-2014 Joergen Ibsen
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

// - compare and replace are not working

#include <windows.h>

#include <iostream>
#include <string>

#include "emitter.hpp"

#include "util.hpp"

namespace {

using namespace std;
using namespace apatch;

vector<char> code;
vector<char> data;
vector<bool> used_codes;

// error handler
void emit_error(char *what)
{
	printf("\n\007[e] error : %s\n", what);

	exit(-1);
}

// warning handler
void emit_warning(char *what)
{
	printf("[e] warning : %s\n", what);
}

void emit_label(STATEMENT *s)
{
	/* nothing */
}

void emit_quit(STATEMENT *s)
{
	used_codes[emitter::C_quit] = true;
	code.push_back(emitter::C_quit);
}

void emit_title(STATEMENT *s)
{
	used_codes[emitter::C_title] = true;
	code.push_back(emitter::C_title);
	append_dword(code, s->dpos);

	append_data(data, s->S1.c_str(), s->dsize);
}

void emit_dlgsize(STATEMENT *s)
{
	/* nothing */
}

void emit_colors(STATEMENT *s)
{
	/* nothing */
}

void emit_print(STATEMENT *s)
{
	used_codes[emitter::C_print] = true;
	code.push_back(emitter::C_print);
	append_dword(code, s->dpos);

	append_data(data, s->S1.c_str(), s->dsize);
}

void emit_cls(STATEMENT *s)
{
	used_codes[emitter::C_cls] = true;
	code.push_back(emitter::C_cls);
}

void emit_start(STATEMENT *s)
{
	used_codes[emitter::C_start] = true;
	code.push_back(emitter::C_start);
}

void emit_goto(STATEMENT *s)
{
	used_codes[emitter::C_goto] = true;
	code.push_back(emitter::C_goto);
	append_dword(code, s->target->cpos);
}

void emit_onerror(STATEMENT *s)
{
	used_codes[emitter::C_onerror] = true;
	code.push_back(emitter::C_onerror);
	if (s->I1 == 0)
		append_dword(code, 0);
	else
		append_dword(code, s->target->cpos);
}

void emit_attrib(STATEMENT *s)
{
	used_codes[emitter::C_attrib] = true;
	code.push_back(emitter::C_attrib);
	append_dword(code, s->dpos);
	append_dword(code, s->I1);
	append_dword(code, s->I2);

	append_data(data, s->S1.c_str(), s->dsize);
}

void emit_open(STATEMENT *s)
{
	if (s->I1 == 0)
	{
		// readwrite
		if (s->I2 == 0)
		{
			used_codes[emitter::C_open_rw] = true;
			code.push_back(emitter::C_open_rw);
			append_dword(code, s->dpos);

			append_data(data, s->S1.c_str(), s->dsize);

		} else if (s->I2 == 1) {

			used_codes[emitter::C_open_dialog_rw] = true;
			code.push_back(emitter::C_open_dialog_rw);
			append_dword(code, 0);

		} else if (s->I2 == 2) {

			used_codes[emitter::C_open_dialog_rw] = true;
			code.push_back(emitter::C_open_dialog_rw);
			append_dword(code, s->dpos);

			append_data(data, s->S1.c_str(), s->dsize);

		} else {

			used_codes[emitter::C_open_argv_rw] = true;
			code.push_back(emitter::C_open_argv_rw);
			append_dword(code, 0);

		}
	} else {
		// readonly
		if (s->I2 == 0)
		{
			used_codes[emitter::C_open_ro] = true;
			code.push_back(emitter::C_open_ro);
			append_dword(code, s->dpos);

			append_data(data, s->S1.c_str(), s->dsize);

		} else if (s->I2 == 1) {

			used_codes[emitter::C_open_dialog_ro] = true;
			code.push_back(emitter::C_open_dialog_ro);
			append_dword(code, 0);

		} else if (s->I2 == 2) {

			used_codes[emitter::C_open_dialog_ro] = true;
			code.push_back(emitter::C_open_dialog_ro);
			append_dword(code, s->dpos);

			append_data(data, s->S1.c_str(), s->dsize);

		} else {

			used_codes[emitter::C_open_argv_ro] = true;
			code.push_back(emitter::C_open_argv_ro);
			append_dword(code, 0);

		}
	}
}

void emit_backup(STATEMENT *s)
{
	used_codes[emitter::C_backup] = true;
	if (s->I1 == 1)
	{
		code.push_back(emitter::C_backup);
		append_dword(code, s->dpos);

		append_data(data, s->S1.c_str(), s->dsize);

	} else if (s->I1 == 2) {

		code.push_back(emitter::C_backup);
		append_dword(code, (unsigned int)-1);

	} else if (s->I1 == 3) {

		code.push_back(emitter::C_backup);
		append_dword(code, (unsigned int)-2);

	} else {

		code.push_back(emitter::C_backup);
		append_dword(code, 0);
	}
}

void emit_offset(STATEMENT *s)
{
	if (s->I2 == 0)
	{
		used_codes[emitter::C_offset] = true;
		code.push_back(emitter::C_offset);
	} else if (s->I2 == 1) {
		used_codes[emitter::C_offset_add] = true;
		code.push_back(emitter::C_offset_add);
	} else {
		used_codes[emitter::C_offset_sub] = true;
		code.push_back(emitter::C_offset_sub);
	}
	append_dword(code, s->I1);
}

void emit_size(STATEMENT *s)
{
	used_codes[emitter::C_size] = true;
	code.push_back(emitter::C_size);
	append_dword(code, s->I1);
}

void emit_crc(STATEMENT *s)
{
	used_codes[emitter::C_crc] = true;
	code.push_back(emitter::C_crc);
	append_dword(code, s->I1);
}

void emit_write(STATEMENT *s)
{
	used_codes[emitter::C_write] = true;
	code.push_back(emitter::C_write);
	append_dword(code, s->dsize);
	append_dword(code, s->dpos);

	append_data(data, &(s->D1[0]), s->dsize);
}

void emit_test(STATEMENT *s)
{
	used_codes[emitter::C_test] = true;
	code.push_back(emitter::C_test);
	append_dword(code, s->I1);
	append_dword(code, s->dpos);

	append_data(data, &(s->D1[0]), s->dsize);
}

void emit_search(STATEMENT *s)
{
	used_codes[emitter::C_search] = true;
	code.push_back(emitter::C_search);
	append_dword(code, s->I1);
	append_dword(code, s->dpos);

	append_data(data, &(s->D1[0]), s->dsize);
}

void emit_yesno(STATEMENT *s)
{
	used_codes[emitter::C_yesno] = true;
	code.push_back(emitter::C_yesno);
	if (s->I1 == 0)
		append_dword(code, 0);
	else
		append_dword(code, s->target->cpos);
}

void emit_regcreate(STATEMENT *s)
{
	static const HKEY hkeys[] = {
		HKEY_CLASSES_ROOT,
		HKEY_CURRENT_USER,
		HKEY_LOCAL_MACHINE,
		HKEY_USERS
	};

	used_codes[emitter::C_regcreate] = true;
	code.push_back(emitter::C_regcreate);
	append_dword(code, s->I1);
	append_dword(code, (unsigned int)hkeys[s->I2]);
	append_dword(code, s->dpos);

	append_data(data, s->S1.c_str(), s->dsize);
}

void emit_regset(STATEMENT *s)
{
	used_codes[emitter::C_regset] = true;
	code.push_back(emitter::C_regset);
	append_dword(code, s->dpos);

	switch(s->I1)
	{
	case 0:
		append_dword(code, 4);
		append_dword(code, REG_DWORD);
		append_dword(data, s->I2);
		break;
	case 1:
		append_dword(code, s->S2.size() + 1);
		append_dword(code, REG_SZ);
		append_data(data, s->S2.c_str(), s->S2.size() + 1);
		break;
	case 2:
		append_dword(code, s->D2.size());
		append_dword(code, REG_BINARY);
		append_data(data, &(s->D2[0]), s->D2.size());
		break;
	}

	append_data(data, s->S1.c_str(), s->S1.size() + 1);
}

void emit_regdel(STATEMENT *s)
{
	used_codes[emitter::C_regdel] = true;
	code.push_back(emitter::C_regdel);
	append_dword(code, s->dpos);

	append_data(data, s->S1.c_str(), s->dsize);
}

void emit_end(STATEMENT *s)
{
	used_codes[emitter::C_quit] = true;
	code.push_back(emitter::C_quit);
}

void emit_statement(STATEMENT *s)
{
	if (s == 0) return;

	if (s->next) emit_statement(s->next);

	switch (s->kind) {
	case STATEMENT::labelK:
		emit_label(s);
		break;
	case STATEMENT::quitK:
		emit_quit(s);
		break;
	case STATEMENT::titleK:
		emit_title(s);
		break;
	case STATEMENT::dlgsizeK:
		emit_dlgsize(s);
		break;
	case STATEMENT::colorsK:
		emit_colors(s);
		break;
	case STATEMENT::printK:
		emit_print(s);
		break;
	case STATEMENT::clsK:
		emit_cls(s);
		break;
	case STATEMENT::startK:
		emit_start(s);
		break;
	case STATEMENT::gotoK:
		emit_goto(s);
		break;
	case STATEMENT::onerrorK:
		emit_onerror(s);
		break;
	case STATEMENT::attribK:
		emit_attrib(s);
		break;
	case STATEMENT::openK:
		emit_open(s);
		break;
	case STATEMENT::backupK:
		emit_backup(s);
		break;
	case STATEMENT::offsetK:
		emit_offset(s);
		break;
	case STATEMENT::sizeK:
		emit_size(s);
		break;
	case STATEMENT::crcK:
		emit_crc(s);
		break;
	case STATEMENT::compareK:
		emit_error("unhandled compare");
		break;
	case STATEMENT::writeK:
		emit_write(s);
		break;
	case STATEMENT::testK:
		emit_test(s);
		break;
	case STATEMENT::searchK:
		emit_search(s);
		break;
	case STATEMENT::replaceK:
		emit_error("unhandled replace");
		break;
	case STATEMENT::yesnoK:
		emit_yesno(s);
		break;
	case STATEMENT::importK:
		emit_error("unhandled import");
		break;
	case STATEMENT::regcreateK:
		emit_regcreate(s);
		break;
	case STATEMENT::regsetK:
		emit_regset(s);
		break;
	case STATEMENT::regdelK:
		emit_regdel(s);
		break;
	case STATEMENT::endK:
		emit_end(s);
		break;
	default:
		emit_error("unknown statement type");
		break;
	}
}

}

void apatch::emitter::emit(SCRIPT *script)
{
	used_codes.swap(vector<bool>(emitter::NUM_CODES, false));

	// reserve memory
	if (script->statements)
	{
		code.reserve(script->statements->cpos + 128);
		data.reserve(script->statements->dpos + 128);
	}

	// add initial quit
	code.push_back(0);

	// add single unused data byte
	data.push_back(0);

	emit_statement(script->statements);

	// make C_open_dialog_rw reflect use of dialog open
	if (used_codes[emitter::C_open_dialog_ro])
	{
		used_codes[emitter::C_open_dialog_rw] = true;
	}

	// make C_open_argv_rw reflect use of argv open
	if (used_codes[emitter::C_open_argv_ro])
	{
		used_codes[emitter::C_open_argv_rw] = true;
	}

	// make C_open_rw reflect use of open
	if (used_codes[emitter::C_open_ro] ||
	        used_codes[emitter::C_open_argv_rw] ||
	        used_codes[emitter::C_open_dialog_rw])
	{
		used_codes[emitter::C_open_rw] = true;
	}

	// make C_offset reflect use of offset
	if (used_codes[emitter::C_offset_add] ||
	        used_codes[emitter::C_offset_sub])
	{
		used_codes[emitter::C_offset] = true;
	}

	script->code.swap(code);
	script->data.swap(data);
	script->used_codes.swap(used_codes);
}
