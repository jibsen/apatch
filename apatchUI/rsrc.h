
#ifndef RSRC_H_INCLUDED
#define RSRC_H_INCLUDED

#define IDI_MAINICON         500

#define IDD_MAIN             2000
#define IDD_ABOUT            2001

#define IDC_FILE             2500
#define IDC_BROWSE           2501
#define IDC_DEBUG            2502
#define IDC_VERSION          2503
#define IDC_STATUS           2504
#define IDC_ABOUT            2505
#define IDC_COMPILE          2506
#define IDC_QUIT             2507
#define IDC_LINK             2508

#endif /* RSRC_H_INCLUDED */
