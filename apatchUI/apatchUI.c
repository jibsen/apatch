/*
 * apatch - simple patching engine
 *
 * apatch GUI wrapper
 *
 * Copyright 2004-2014 Joergen Ibsen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <windows.h>

#include "rsrc.h"

#ifdef _MSC_VER
/* include win32api libraries */
#pragma comment(lib,"kernel32.lib")
#pragma comment(lib,"user32.lib")
#pragma comment(lib,"shell32.lib")
#pragma comment(lib,"comdlg32.lib")
#pragma comment(lib,"gdi32.lib")
#endif

HINSTANCE hInst;
HWND hStatus;
HBRUSH hBkBrush;
char path[MAX_PATH + 1];
char spec[1024];

void memzero(void *p, size_t len)
{
	unsigned char *cp = (unsigned char *)p;

	while (len--) *cp++ = 0;
}

BOOL execute_piped(char *cmdline, HWND hEdit)
{
	SECURITY_ATTRIBUTES sat = { 0 };
	PROCESS_INFORMATION pinfo = { 0 };
	STARTUPINFO sinfo = { 0 };
	HANDLE hPipeRd, hPipeWr, hPipeRdDup;
	BOOL bRet;
	char buffer[1024];
	DWORD dwCount;

	/* create security attributes that inherit handles */
	sat.nLength              = sizeof(SECURITY_ATTRIBUTES);
	sat.lpSecurityDescriptor = NULL;
	sat.bInheritHandle       = TRUE;

	/* create pipe for stdout */
	bRet = CreatePipe(&hPipeRd, &hPipeWr, &sat, 0);

	if (!bRet)
	{
		MessageBox(0, "Unable to create pipe.", "Error", MB_ICONERROR + MB_OK);
		return FALSE;
	}

	/* create noninheritable handle and close original */
	bRet = DuplicateHandle(GetCurrentProcess(), hPipeRd, GetCurrentProcess(), &hPipeRdDup, 0, FALSE, DUPLICATE_SAME_ACCESS);

	CloseHandle(hPipeRd);

	if (!bRet)
	{
		MessageBox(0, "Unable to duplicate handle.", "Error", MB_ICONERROR + MB_OK);
		return FALSE;
	}

	/* fill in startup info */
	sinfo.cb = sizeof(sinfo);
	GetStartupInfo(&sinfo);

	sinfo.hStdOutput  = hPipeWr;
	sinfo.hStdError   = hPipeWr;
	sinfo.dwFlags     = STARTF_USESHOWWINDOW + STARTF_USESTDHANDLES;
	sinfo.wShowWindow = SW_HIDE;

	/* create process */
	bRet = CreateProcess(NULL, cmdline, NULL, NULL, TRUE, 0, NULL, NULL,
	                     &sinfo, &pinfo);

	if (!bRet)
	{
		MessageBox(0, "Unable to create apatch process.\n\nPlease make sure 'apatch.exe' is available.", "Error",
		           MB_ICONERROR + MB_OK);
		return FALSE;
	}

	/* close write end of pipe */
	CloseHandle(hPipeWr);

	/* read from pipe */
	while (ReadFile(hPipeRdDup, buffer, sizeof(buffer) - 1, &dwCount, NULL) && dwCount)
	{
		buffer[dwCount] = 0;
		SendMessage(hEdit, EM_SETSEL, -1, 0);
		SendMessage(hEdit, EM_REPLACESEL, FALSE, (DWORD)buffer);
	}

	/* wait for process to terminate */
	WaitForSingleObject(pinfo.hProcess, INFINITE);

	/* close handles */
	CloseHandle(pinfo.hProcess);
	CloseHandle(pinfo.hThread);

	return TRUE;
}

BOOL CALLBACK AboutDlgProc(HWND hDlg, UINT message,
                           WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_INITDIALOG:
		return TRUE;

	case WM_CLOSE:
		PostMessage(hDlg, WM_COMMAND, IDOK, 0);
		return TRUE;

	case WM_COMMAND:
		switch (LOWORD (wParam))
		{
		case IDOK:
		case IDCANCEL:
			EndDialog(hDlg, 0);
			return TRUE;

		case IDC_LINK:
			ShellExecute(NULL, "open", "http://www.ibsensoftware.com/", NULL, NULL, 0);
			return TRUE;

		default:
			return FALSE;
		}

	default:
		return FALSE;
	}

	return TRUE;
}

BOOL CALLBACK MainDlgProc(HWND hDlg, UINT message,
                          WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_INITDIALOG:
		hBkBrush = CreateSolidBrush(0x00400000);
		hStatus  = GetDlgItem(hDlg, IDC_STATUS);
		SendMessage(hStatus, WM_SETFONT, (WPARAM) GetStockObject(OEM_FIXED_FONT), 0);
		DragAcceptFiles(hDlg, TRUE);
		return TRUE;

	case WM_CLOSE:
		PostMessage(hDlg, WM_COMMAND, IDC_QUIT, 0);
		return TRUE;

	case WM_CTLCOLORSTATIC:
		if ((HWND) lParam != hStatus) return FALSE;
		SetTextColor((HDC) wParam, 0x00A0D0A0);
		SetBkColor((HDC) wParam, 0x00400000);
		return (DWORD) hBkBrush;

	case WM_DROPFILES:
	{
		HANDLE hDrop = (HANDLE) wParam;
		if (DragQueryFile(hDrop, 0, path, sizeof(path)))
		{
			SetDlgItemText(hDlg, IDC_FILE, path);
		}
		DragFinish(hDrop);
	}
	return 0;

	case WM_COMMAND:
		switch (LOWORD (wParam))
		{
		case IDC_BROWSE:
		{
			OPENFILENAME ofn;
			memzero(&ofn, sizeof(ofn));
			ofn.lStructSize     = sizeof(ofn);
			ofn.hwndOwner       = hDlg;
			ofn.lpstrFilter     = "Script files (*.aps)\0*.aps\0All files (*.*)\0*.*\0";
			ofn.nFilterIndex    = 1;
			ofn.lpstrFile       = path;
			ofn.lpstrFile[0]    = '\0';
			ofn.nMaxFile        = sizeof(path);
			ofn.lpstrTitle      = "Choose script file...";
			ofn.lpstrInitialDir = ".";
			ofn.Flags           = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY;
			if (GetOpenFileName(&ofn))
			{
				SetDlgItemText(hDlg, IDC_FILE, ofn.lpstrFile);
			}
		}
		return TRUE;

		case IDC_ABOUT:
			DialogBoxParam(hInst, MAKEINTRESOURCE(IDD_ABOUT), hDlg, AboutDlgProc, 0);
			return TRUE;

		case IDC_VERSION:
			SendMessage(hStatus, EM_SETSEL, 0, -1);
			SendMessage(hStatus, EM_REPLACESEL, FALSE, (DWORD)"");
			execute_piped("apatch -v", hStatus);
			return TRUE;

		case IDC_DEBUG:
			SendMessage(hStatus, EM_SETSEL, 0, -1);
			SendMessage(hStatus, EM_REPLACESEL, FALSE, (DWORD)"");

			GetDlgItemText(hDlg, IDC_FILE, path, sizeof(path));
			wsprintf(spec, "apatch -d \"%s\"", path);

			execute_piped(spec, hStatus);
			return TRUE;

		case IDC_COMPILE:
			SendMessage(hStatus, EM_SETSEL, 0, -1);
			SendMessage(hStatus, EM_REPLACESEL, FALSE, (DWORD)"");

			GetDlgItemText(hDlg, IDC_FILE, path, sizeof(path));
			wsprintf(spec, "apatch \"%s\"", path);

			execute_piped(spec, hStatus);
			return TRUE;

		case IDCANCEL:
		case IDC_QUIT:
			EndDialog(hDlg, 0);
			return TRUE;

		default:
			return FALSE;
		}

	default:
		return FALSE;
	}

	return TRUE;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	hInst = hInstance;

	DialogBoxParam(hInst, MAKEINTRESOURCE(IDD_MAIN), NULL, MainDlgProc, 0);

	return 0;
}
