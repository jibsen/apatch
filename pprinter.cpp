//
// apatch - simple patching engine
//
// pretty-printer
//
// Copyright 1999-2014 Joergen Ibsen
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include <iostream>
#include <string>

#include "pprinter.hpp"

namespace {

using namespace std;
using namespace apatch;

// error handler
void pprint_error(char *what)
{
	printf("\n\007[pp] error : %s\n", what);

	exit(-1);
}

// warning handler
void pprint_warning(char *what)
{
	printf("[pp] warning : %s\n", what);
}

void pprint_label(STATEMENT *s)
{
	cout << s->S1 << ":\n";
}

void pprint_quit(STATEMENT *s)
{
	cout << "  quit\n";
}

void pprint_title(STATEMENT *s)
{
	cout << "  title \"" << s->S1 << "\"\n";
}

void pprint_dlgsize(STATEMENT *s)
{
	cout << "  dlgsize " << s->I1 << ' ' << s->I2 << '\n';
}

void pprint_colors(STATEMENT *s)
{
	cout << "  colors";
	for (vector<char>::iterator i = s->D1.begin(); i != s->D1.end(); ++i)
	{
		printf(" 0x%02X", (unsigned char)*i);
	}
	cout << '\n';
}

void pprint_print(STATEMENT *s)
{
	cout << "  print \"" << s->S1 << "\"\n";
}

void pprint_cls(STATEMENT *s)
{
	cout << "  cls\n";
}

void pprint_start(STATEMENT *s)
{
	cout << "  start\n";
}

void pprint_goto(STATEMENT *s)
{
	cout << "  goto " << s->S1 << '\n';
}

void pprint_onerror(STATEMENT *s)
{
	if (s->I1 == 0)
		cout << "  onerror quit\n";
	else
		cout << "  onerror goto " << s->S1 << '\n';
}

void pprint_attrib(STATEMENT *s)
{
	cout << "  attrib " << s->S2 << " \"" << s->S1 << "\"\n";
}

void pprint_open(STATEMENT *s)
{
	if (s->I1 == 0)
		cout << "  open ";
	else
		cout << "  open ro ";

	switch (s->I2)
	{
	case 0:
		cout << '"' << s->S1 << "\"\n";
		break;
	case 1:
		cout << "dialog\n";
		break;
	case 2:
		cout << "dialog \"" << s->S1 << "\"\n";
		break;
	case 3:
		cout << "argv\n";
		break;
	}
}

void pprint_backup(STATEMENT *s)
{
	if (s->I1 == 1)
	{
		cout << "  backup \"" << s->S1 << "\"\n";
	} else if (s->I1 == 2) {
		cout << "  backup on\n";
	} else if (s->I1 == 3) {
		cout << "  backup off\n";
	} else {
		cout << "  backup\n";
	}
}

void pprint_offset(STATEMENT *s)
{
	if (s->I2 == 0)
	{
		cout << "  offset " << s->I1 << '\n';
	} else if (s->I2 == 1) {
		cout << "  offset + " << s->I1 << '\n';
	} else {
		cout << "  offset - " << s->I1 << '\n';
	}
}

void pprint_size(STATEMENT *s)
{
	if (s->I2 == 0)
		cout << "  size " << s->I1 << '\n';
	else
		cout << "  size \"" << s->S1 << "\"\n";
}

void pprint_crc(STATEMENT *s)
{
	if (s->I2 == 0)
		printf("  crc 0x%08X\n", s->I1);
	else
		cout << "  crc \"" << s->S1 << "\"\n";
}

void pprint_compare(STATEMENT *s)
{
	cout << "  compare \"" << s->S1 << "\" to \"" << s->S2 << "\"\n";
}

void pprint_write(STATEMENT *s)
{
	cout << "  write";
	for (vector<char>::iterator i = s->D1.begin(); i != s->D1.end(); ++i)
	{
		printf(" 0x%02X", (unsigned char)*i);
	}
	cout << '\n';
}

void pprint_test(STATEMENT *s)
{
	cout << "  test";
	bool escaped = false;
	for (vector<char>::iterator i = s->D1.begin(); i != s->D1.end(); ++i)
	{
		if (escaped)
		{
			escaped = false;
			if (*i == 0)
				printf(" 0x00");
			else
				printf(" ?");

		} else {

			if (*i == 0)
				escaped = true;
			else
				printf(" 0x%02X", (unsigned char)*i);
		}
	}
	cout << '\n';
}

void pprint_search(STATEMENT *s)
{
	cout << "  search";
	bool escaped = false;
	for (vector<char>::iterator i = s->D1.begin(); i != s->D1.end(); ++i)
	{
		if (escaped)
		{
			escaped = false;
			if (*i == 0)
				printf(" 0x00");
			else
				printf(" ?");

		} else {

			if (*i == 0)
			{
				if (i == s->D1.begin())
					printf(" 0x00");
				else
					escaped = true;

			} else {

				printf(" 0x%02X", (unsigned char)*i);
			}
		}
	}
	cout << '\n';
}

void pprint_replace(STATEMENT *s)
{
	cout << "  replace";
	for (vector<char>::iterator i = s->D1.begin(); i != s->D1.end(); ++i)
	{
		printf(" 0x%02X", (unsigned char)*i);
	}
	cout << " with";
	for (vector<char>::iterator i = s->D2.begin(); i != s->D2.end(); ++i)
	{
		printf(" 0x%02X", (unsigned char)*i);
	}
	cout << '\n';
}

void pprint_yesno(STATEMENT *s)
{
	if (s->I1 == 0)
		cout << "  yesno ifno quit\n";
	else
		cout << "  yesno ifno goto " << s->S1 << '\n';
}

void pprint_import(STATEMENT *s)
{
	if (s->I1 == 0)
		cout << "  import fc \"" << s->S1 << "\"\n";
	else
		cout << "  import reg \"" << s->S1 << "\"\n";
}

void pprint_regcreate(STATEMENT *s)
{
	static const char *classes[] = {
		"HKEY_CLASSES_ROOT",
		"HKEY_CURRENT_USER",
		"HKEY_LOCAL_MACHINE",
		"HKEY_USERS"
	};

	cout << ((s->I1 == 0) ? "  regopen " : "  regcreate ");
	cout << classes[s->I2] << " \"" << s->S1 << "\"\n";
}

void pprint_regset(STATEMENT *s)
{
	cout << "  regset \"" << s->S1 << '\"';

	switch (s->I1)
	{
	case 0:
		cout << ' ' << s->I2;
		break;
	case 1:
		cout << " \"" << s->S2 << '\"';
		break;
	case 2:
		cout << " binary";
		for (vector<char>::iterator i = s->D2.begin(); i != s->D2.end(); ++i)
		{
			printf(" 0x%02X", (unsigned char)*i);
		}
		break;
	}
	cout << '\n';
}

void pprint_regdel(STATEMENT *s)
{
	cout << "  regdel \"" << s->S1 << "\"\n";
}

void pprint_end(STATEMENT *s)
{
	cout << "  end\n";
}

void pprint_statement(STATEMENT *s)
{
	if (s == 0) return;

	if (s->next) pprint_statement(s->next);

	switch (s->kind) {
	case STATEMENT::labelK:
		pprint_label(s);
		break;
	case STATEMENT::quitK:
		pprint_quit(s);
		break;
	case STATEMENT::titleK:
		pprint_title(s);
		break;
	case STATEMENT::dlgsizeK:
		pprint_dlgsize(s);
		break;
	case STATEMENT::colorsK:
		pprint_colors(s);
		break;
	case STATEMENT::printK:
		pprint_print(s);
		break;
	case STATEMENT::clsK:
		pprint_cls(s);
		break;
	case STATEMENT::startK:
		pprint_start(s);
		break;
	case STATEMENT::gotoK:
		pprint_goto(s);
		break;
	case STATEMENT::onerrorK:
		pprint_onerror(s);
		break;
	case STATEMENT::attribK:
		pprint_attrib(s);
		break;
	case STATEMENT::openK:
		pprint_open(s);
		break;
	case STATEMENT::backupK:
		pprint_backup(s);
		break;
	case STATEMENT::offsetK:
		pprint_offset(s);
		break;
	case STATEMENT::sizeK:
		pprint_size(s);
		break;
	case STATEMENT::crcK:
		pprint_crc(s);
		break;
	case STATEMENT::compareK:
		pprint_compare(s);
		break;
	case STATEMENT::writeK:
		pprint_write(s);
		break;
	case STATEMENT::testK:
		pprint_test(s);
		break;
	case STATEMENT::searchK:
		pprint_search(s);
		break;
	case STATEMENT::replaceK:
		pprint_replace(s);
		break;
	case STATEMENT::yesnoK:
		pprint_yesno(s);
		break;
	case STATEMENT::importK:
		pprint_import(s);
		break;
	case STATEMENT::regcreateK:
		pprint_regcreate(s);
		break;
	case STATEMENT::regsetK:
		pprint_regset(s);
		break;
	case STATEMENT::regdelK:
		pprint_regdel(s);
		break;
	case STATEMENT::endK:
		pprint_end(s);
		break;
	default:
		pprint_error("unknown statement type");
		break;
	}
}

}

void apatch::pprinter::pretty_print(SCRIPT *script)
{
	pprint_statement(script->statements);
}
