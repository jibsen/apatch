//
// apatch - simple patching engine
//
// scanner (lexer)
//
// Copyright 1999-2014 Joergen Ibsen
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef SCANNER_HPP_INCLUDED
#define SCANNER_HPP_INCLUDED

namespace apatch {

namespace scanner {

enum {
    T_EOF = 0, T_UNKNOWN = 3, T_NEWLINE, T_INT,
    T_HEXINT, T_STRING, T_IDENTIFIER,
    T_PLUS = 9, T_DASH, T_COLON, T_WILD, T_title, T_print, T_cls,
    T_start, T_goto, T_onerror, T_quit, T_open, T_dialog,
    T_backup, T_offset, T_size, T_crc, T_write, T_test,
    T_search, T_yesno, T_ifno, T_end,
    T_attrib, T_compare, T_argv,
    T_dlgsize, T_colors,
    T_import,
    T_regopen, T_regcreate, T_regset, T_binary, T_regdel
    /* T_replace, T_with */
};

void init(char *buffer);
int scan();
void unscan();
void restart(char *buffer);

const char *get_string();
int get_string_len();
int get_line_number();

}

}

#endif // SCANNER_HPP_INCLUDED
