//
// apatch - simple patching engine
//
// remove dead code from patch
//
// Copyright 1999-2014 Joergen Ibsen
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef REMDEAD_HPP_INCLUDED
#define REMDEAD_HPP_INCLUDED

#include <vector>

#include "script.hpp"

namespace apatch {

namespace remdead {

void remove_dead(std::vector<char> &stub,
                 const std::vector<bool> &used,
                 unsigned int num);

}

}

#endif // REMDEAD_HPP_INCLUDED
