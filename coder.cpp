//
// apatch - simple patching engine
//
// coder
//
// Copyright 1999-2014 Joergen Ibsen
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

// - replace

#include <iostream>
#include <fstream>
#include <string>
#include <cstdio>

#include <windows.h>

#include "coder.hpp"

#include "util.hpp"
#include "crcfile.hpp"
#include "filecmp.hpp"
#include "regedit.hpp"

namespace {

SCRIPT *current_script;

using namespace std;
using namespace apatch;

// error handler
void code_error(char *what)
{
	printf("\n\007[c] error : %s\n", what);

	exit(-1);
}

// warning handler
void code_warning(char *what)
{
	printf("[c] warning : %s\n", what);
}

void code_label(STATEMENT **s)
{
}

void code_quit(STATEMENT **s)
{
}

void code_title(STATEMENT **s)
{
	fix_string((*s)->S1);
}

void code_dlgsize(STATEMENT **s)
{
	current_script->dlgw = (*s)->I1;
	current_script->dlgh = (*s)->I2;
}

void code_colors(STATEMENT **s)
{
	unsigned int text_color;
	text_color = (*s)->D1[2] & 0x00ff;
	text_color = (text_color << 8) + ((*s)->D1[1] & 0x00ff);
	text_color = (text_color << 8) + ((*s)->D1[0] & 0x00ff);

	unsigned int bk_color;
	bk_color = (*s)->D1[5] & 0x00ff;
	bk_color = (bk_color << 8) + ((*s)->D1[4] & 0x00ff);
	bk_color = (bk_color << 8) + ((*s)->D1[3] & 0x00ff);

	current_script->text_color = text_color;
	current_script->bk_color = bk_color;
}

void code_print(STATEMENT **s)
{
	fix_string((*s)->S1);
}

void code_cls(STATEMENT **s)
{
}

void code_start(STATEMENT **s)
{
}

void code_goto(STATEMENT **s)
{
}

void code_onerror(STATEMENT **s)
{
}

void code_attrib(STATEMENT **s)
{
	STATEMENT *ss = *s;

	unsigned int and_mask = 0xffffffff;
	unsigned int or_mask = 0;

	string::const_iterator i = ss->S2.begin();

	while (i != ss->S2.end())
	{
		if (*i == '+')
		{
			++i;
			if (*i == 'a') or_mask |= FILE_ATTRIBUTE_ARCHIVE;
			if (*i == 'h') or_mask |= FILE_ATTRIBUTE_HIDDEN;
			if (*i == 'r') or_mask |= FILE_ATTRIBUTE_READONLY;
			if (*i == 's') or_mask |= FILE_ATTRIBUTE_SYSTEM;
		} else {
			++i;
			if (*i == 'a') and_mask &= ~FILE_ATTRIBUTE_ARCHIVE;
			if (*i == 'h') and_mask &= ~FILE_ATTRIBUTE_HIDDEN;
			if (*i == 'r') and_mask &= ~FILE_ATTRIBUTE_READONLY;
			if (*i == 's') and_mask &= ~FILE_ATTRIBUTE_SYSTEM;
		}
		++i;
	}

	ss->I1 = and_mask;
	ss->I2 = or_mask;
}

void code_open(STATEMENT **s)
{
}

void code_backup(STATEMENT **s)
{
}

void code_offset(STATEMENT **s)
{
}

void code_size(STATEMENT **s)
{
	STATEMENT *ss = *s;

	/* check size if filename given */
	if (ss->I2)
	{
		FILE *file = fopen(ss->S1.c_str(), "rb");

		if (file == 0) code_error("unable to open file to check size");

		fseek(file, 0, SEEK_END);

		ss->I1 = ftell(file);
		ss->I2 = 0;

		fclose(file);
	}
}

void code_crc(STATEMENT **s)
{
	STATEMENT *ss = *s;

	/* check crc if filename given */
	if (ss->I2)
	{
		ss->I1 = crc_file(ss->S1.c_str());
		ss->I2 = 0;
	}
}

void code_compare(STATEMENT **s)
{
	STATEMENT *ss = *s;

	// compare files and get differences
	vector<diff_t> diffs = file_compare(ss->S1.c_str(), ss->S2.c_str());

	STATEMENT *first = 0, *last = 0;

	// transform differences into offset and write statements
	for (vector<diff_t>::iterator i = diffs.begin(); i != diffs.end(); ++i)
	{
		STATEMENT *offset = new STATEMENT;
		STATEMENT *write = new STATEMENT;

		offset->I1 = i->offset;
		offset->I2 = 0;
		offset->kind = STATEMENT::offsetK;

		write->D1.swap(i->bytes);
		write->kind = STATEMENT::writeK;
		write->next = offset;

		if (!last) last = offset;

		offset->next = first;
		first = write;
	}

	// replace s with offsets and writes
	if (first)
	{
		*s = first;
		last->next = ss->next;
	} else {
		code_warning("no differences in 'compare'");
	}

	// delete ss
	ss->next = 0;
	delete ss;
}

void code_write(STATEMENT **s)
{
}

void code_test(STATEMENT **s)
{
}

void code_search(STATEMENT **s)
{
}

void code_replace(STATEMENT **s)
{
	STATEMENT *ss = *s;

	/* transform into two statements: test, write */

	STATEMENT *write = new STATEMENT;
	STATEMENT *test = new STATEMENT;

	test->kind = STATEMENT::testK;
	test->D1.swap(ss->D1);
	test->I1 = ss->I1;
	test->lineno = ss->lineno;
	test->next = write;

	write->kind = STATEMENT::writeK;
	write->D1.swap(ss->D2);
	write->lineno = ss->lineno;
	write->next = ss->next;

	// replace s with test and write
	*s = test;
	ss->next = 0;
	delete ss;
}

void code_yesno(STATEMENT **s)
{
}

void code_import(STATEMENT **s)
{
	STATEMENT *ss = *s;

	if (ss->I1 == 0)
	{
		// parse fc /b file
		vector<diff_t> diffs = file_compare_fc(ss->S1.c_str());

		STATEMENT *first = 0, *last = 0;

		// transform differences into offset and write statements
		for (vector<diff_t>::iterator i = diffs.begin(); i != diffs.end(); ++i)
		{
			STATEMENT *offset = new STATEMENT;
			STATEMENT *write = new STATEMENT;

			offset->I1 = i->offset;
			offset->I2 = 0;
			offset->kind = STATEMENT::offsetK;

			write->D1.swap(i->bytes);
			write->kind = STATEMENT::writeK;
			write->next = offset;

			if (!last) last = offset;

			offset->next = first;
			first = write;
		}

		// replace s with offsets and writes
		if (first)
		{
			*s = first;
			last->next = ss->next;
		} else {
			code_warning("no differences in 'import fc'");
		}

		// delete ss
		ss->next = 0;
		delete ss;

	} else {

		// parse reg file
		vector<regkey_t> keys = import_regedit(ss->S1.c_str());

		STATEMENT *first = 0, *last = 0;

		// transform differences into offset and write statements
		for (vector<regkey_t>::iterator k = keys.begin(); k != keys.end(); ++k)
		{
			STATEMENT *regcreate = new STATEMENT;

			regcreate->I1 = 1;
			regcreate->I2 = k->base;
			regcreate->S1 = k->subkey;
			regcreate->kind = STATEMENT::regcreateK;

			if (!last) last = regcreate;

			regcreate->next = first;
			first = regcreate;

			for (vector<regval_t>::iterator v = k->values.begin(); v != k->values.end(); ++v)
			{
				STATEMENT *regset = new STATEMENT;

				regset->kind = STATEMENT::regsetK;

				regset->S1 = v->name;

				switch (v->kind)
				{
				case regval_t::dwordK:
					regset->I1 = 0;
					regset->I2 = v->I;
					break;

				case regval_t::stringK:
					regset->I1 = 1;
					regset->S2 = v->S;
					break;

				case regval_t::binaryK:
					regset->I1 = 2;
					regset->D2 = v->D;
					break;
				}

				regset->next = first;
				first = regset;
			}
		}

		// replace s with regcreates and regsets
		if (first)
		{
			*s = first;
			last->next = ss->next;
		} else {
			code_warning("no registry data in 'import reg'");
		}

		// delete ss
		ss->next = 0;
		delete ss;

	}
}

void code_regcreate(STATEMENT **s)
{
}

void code_regset(STATEMENT **s)
{
	if ((*s)->I1 == 1) fix_string((*s)->S2);
}

void code_regdel(STATEMENT **s)
{
}

void code_end(STATEMENT **s)
{
}

void code_statement(STATEMENT **s)
{
	if (*s == 0) return;

	if ((*s)->next) code_statement(&((*s)->next));

	switch ((*s)->kind) {
	case STATEMENT::labelK:
		code_label(s);
		break;
	case STATEMENT::quitK:
		code_quit(s);
		break;
	case STATEMENT::titleK:
		code_title(s);
		break;
	case STATEMENT::dlgsizeK:
		code_dlgsize(s);
		break;
	case STATEMENT::colorsK:
		code_colors(s);
		break;
	case STATEMENT::printK:
		code_print(s);
		break;
	case STATEMENT::clsK:
		code_cls(s);
		break;
	case STATEMENT::startK:
		code_start(s);
		break;
	case STATEMENT::gotoK:
		code_goto(s);
		break;
	case STATEMENT::onerrorK:
		code_onerror(s);
		break;
	case STATEMENT::attribK:
		code_attrib(s);
		break;
	case STATEMENT::openK:
		code_open(s);
		break;
	case STATEMENT::backupK:
		code_backup(s);
		break;
	case STATEMENT::offsetK:
		code_offset(s);
		break;
	case STATEMENT::sizeK:
		code_size(s);
		break;
	case STATEMENT::crcK:
		code_crc(s);
		break;
	case STATEMENT::compareK:
		code_compare(s);
		break;
	case STATEMENT::writeK:
		code_write(s);
		break;
	case STATEMENT::testK:
		code_test(s);
		break;
	case STATEMENT::searchK:
		code_search(s);
		break;
	case STATEMENT::replaceK:
		code_replace(s);
		break;
	case STATEMENT::yesnoK:
		code_yesno(s);
		break;
	case STATEMENT::importK:
		code_import(s);
		break;
	case STATEMENT::regcreateK:
		code_regcreate(s);
		break;
	case STATEMENT::regsetK:
		code_regset(s);
		break;
	case STATEMENT::regdelK:
		code_regdel(s);
		break;
	case STATEMENT::endK:
		code_end(s);
		break;
	default:
		code_error("unknown statement type");
		break;
	}
}

}

void apatch::coder::code(SCRIPT *script)
{
	current_script = script;

	code_statement(&(script->statements));
}
