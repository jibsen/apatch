//
// apatch - simple patching engine
//
// misc
//
// Copyright 1999-2014 Joergen Ibsen
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include "util.hpp"

using namespace std;

void apatch::fix_string(string &s)
{
	bool escaped = false;

	string res;

	res.reserve(s.size());

	for (string::iterator i = s.begin(); i != s.end(); ++i)
	{
		if (escaped)
		{
			escaped = false;

			switch (*i) {
			case '\\':
				res.push_back('\\');
				break;
			case '"':
				res.push_back('"');
				break;
			case 'n':
				res.push_back(0x0d);
				res.push_back(0x0a);
				break;
			case 'r':
				res.push_back(0x0d);
				break;
			case 't':
				res.push_back(0x09);
				break;
			case 'x':
			{
				char valstr[] = "0x??\0";
				if (++i == s.end()) return;
				valstr[2] = *i;
				if (++i == s.end()) return;
				valstr[3] = *i;
				unsigned int val = strtoul(valstr, 0, 0);
				res.push_back(val & 0x00ff);
			}
			break;
			default:
				res.push_back('\\');
				res.push_back(*i);
				break;
			}

		} else {

			if (*i == '\\')
			{
				escaped = true;

			} else {

				res.push_back(*i);
			}
		}
	}

	if (escaped) res.push_back('\\');

	s.swap(res);
}

void apatch::append_data(vector<char> &v, const char *data, int len)
{
	v.insert(v.end(), data, data + len);
}

void apatch::append_dword(vector<char> &v, unsigned int n)
{
	v.push_back((n)       & 0x00ff);
	v.push_back((n >> 8)  & 0x00ff);
	v.push_back((n >> 16) & 0x00ff);
	v.push_back((n >> 24) & 0x00ff);
}
