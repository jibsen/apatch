//
// apatch - simple patching engine
//
// parser
//
// Copyright 1999-2014 Joergen Ibsen
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

// - testing

#include <string>
#include <cstdio>

#include "parser.hpp"
#include "scanner.hpp"
#include "util.hpp"

namespace {

using namespace std;
using namespace apatch;

// error handler
void parse_error(char *what, int line)
{
	printf("\n\007[p] error at line %d : %s\n", line, what);

	exit(-1);
}

// warning handler
void parse_warning(char *what, int line)
{
	printf("[p] warning at line %d : %s\n", line, what);
}

void remove_quotes(string &s)
{
	s = s.substr(s.find('"') + 1, s.rfind('"') - 1);
}

STATEMENT *parse_label(STATEMENT *next)
{
	int lineno = scanner::get_line_number();

	string label(scanner::get_string());

	int tok = scanner::scan();

	if (tok != scanner::T_COLON)
	{
		parse_error("unknown token (or missing ':' in label)", lineno);
	}

	STATEMENT *s = new STATEMENT;

	s->kind = STATEMENT::labelK;
	s->S1 = label;
	s->I1 = 0;
	s->target = 0;
	s->lineno = lineno;
	s->next = next;

	return s;
}

STATEMENT *parse_quit(STATEMENT *next)
{
	int lineno = scanner::get_line_number();

	STATEMENT *s = new STATEMENT;

	s->kind = STATEMENT::quitK;
	s->lineno = lineno;
	s->next = next;

	return s;
}

STATEMENT *parse_title(STATEMENT *next)
{
	int lineno = scanner::get_line_number();

	int tok = scanner::scan();

	if (tok != scanner::T_STRING)
	{
		parse_error("title: expected string", lineno);
	}

	string title(scanner::get_string());
	remove_quotes(title);

	STATEMENT *s = new STATEMENT;

	s->kind = STATEMENT::titleK;
	s->S1 = title;
	s->lineno = lineno;
	s->next = next;

	return s;
}

STATEMENT *parse_dlgsize(STATEMENT *next)
{
	int lineno = scanner::get_line_number();

	STATEMENT *s = new STATEMENT;

	int tok = scanner::scan();

	if ((tok == scanner::T_INT) || (tok == scanner::T_HEXINT))
	{
		unsigned int val = strtoul(scanner::get_string(), 0, 0);

		s->I1 = val;

	} else {

		parse_error("dlgsize: expected width", lineno);
	}

	tok = scanner::scan();

	if ((tok == scanner::T_INT) || (tok == scanner::T_HEXINT))
	{
		unsigned int val = strtoul(scanner::get_string(), 0, 0);

		s->I2 = val;

	} else {

		parse_error("dlgsize: expected height", lineno);
	}

	s->kind = STATEMENT::dlgsizeK;
	s->lineno = lineno;
	s->next = next;

	return s;
}

STATEMENT *parse_colors(STATEMENT *next)
{
	int lineno = scanner::get_line_number();

	int tok = scanner::scan();

	if ((tok != scanner::T_INT) && (tok != scanner::T_HEXINT))
	{
		parse_error("colors: expected value", lineno);
	}

	STATEMENT *s = new STATEMENT;

	while ((tok == scanner::T_INT) || (tok == scanner::T_HEXINT))
	{
		unsigned int val = strtoul(scanner::get_string(), 0, 0);

		if (val & ~0x00ff)
		{
			parse_error("colors: value larger than byte", lineno);
		}

		s->D1.push_back(val);

		tok = scanner::scan();
	}

	if (s->D1.size() != 6)
	{
		parse_error("colors: six values required", lineno);
	}

	scanner::unscan();

	s->kind = STATEMENT::colorsK;
	s->lineno = lineno;
	s->next = next;

	return s;
}

STATEMENT *parse_print(STATEMENT *next)
{
	int lineno = scanner::get_line_number();

	int tok = scanner::scan();

	if (tok != scanner::T_STRING)
	{
		parse_error("print: expected string", lineno);
	}

	STATEMENT *s = new STATEMENT;

	while (tok == scanner::T_STRING)
	{
		string text(scanner::get_string());
		remove_quotes(text);

		s->S1 += text;

		tok = scanner::scan();
	}

	scanner::unscan();

	s->kind = STATEMENT::printK;
	s->lineno = lineno;
	s->next = next;

	return s;
}

STATEMENT *parse_cls(STATEMENT *next)
{
	int lineno = scanner::get_line_number();

	STATEMENT *s = new STATEMENT;

	s->kind = STATEMENT::clsK;
	s->lineno = lineno;
	s->next = next;

	return s;
}

STATEMENT *parse_start(STATEMENT *next)
{
	int lineno = scanner::get_line_number();

	STATEMENT *s = new STATEMENT;

	s->kind = STATEMENT::startK;
	s->lineno = lineno;
	s->next = next;

	return s;
}

STATEMENT *parse_goto(STATEMENT *next)
{
	int lineno = scanner::get_line_number();

	int tok = scanner::scan();

	if (tok != scanner::T_IDENTIFIER)
	{
		parse_error("goto: expected label", lineno);
	}

	string target(scanner::get_string());

	STATEMENT *s = new STATEMENT;

	s->kind = STATEMENT::gotoK;
	s->S1 = target;
	s->lineno = lineno;
	s->next = next;

	return s;
}

STATEMENT *parse_onerror(STATEMENT *next)
{
	int lineno = scanner::get_line_number();

	STATEMENT *s = new STATEMENT;

	int tok = scanner::scan();

	if (tok == scanner::T_goto)
	{
		tok = scanner::scan();

		if (tok != scanner::T_IDENTIFIER)
		{
			parse_error("onerror goto: expected label", lineno);
		}

		string target(scanner::get_string());

		s->S1 = target;
		s->I1 = 1;

	} else if (tok == scanner::T_quit) {

		s->I1 = 0;

	} else {

		parse_error("onerror: expected 'goto' or 'quit'", lineno);
	}

	s->kind = STATEMENT::onerrorK;
	s->lineno = lineno;
	s->next = next;

	return s;
}

STATEMENT *parse_attrib(STATEMENT *next)
{
	int lineno = scanner::get_line_number();

	int tok = scanner::scan();

	if ((tok != scanner::T_PLUS) &&
	        (tok != scanner::T_DASH))
	{
		parse_error("attrib: expected attributes", lineno);
	}

	STATEMENT *s = new STATEMENT;

	string attribs;

	while ((tok == scanner::T_PLUS) ||
	        (tok == scanner::T_DASH))
	{
		if (tok == scanner::T_PLUS) attribs += '+';
		if (tok == scanner::T_DASH) attribs += '-';

		tok = scanner::scan();

		if ((tok != scanner::T_IDENTIFIER) ||
		        (scanner::get_string_len() != 1))
		{
			parse_error("attrib: expected attribute character (a,h,r,s)", lineno);
		}

		char c = scanner::get_string()[0];

		if (c == 'A') c = 'a';
		if (c == 'H') c = 'h';
		if (c == 'R') c = 'r';
		if (c == 'S') c = 's';

		if ((c != 'a') && (c != 'h') && (c != 'r') && (c != 's'))
		{
			parse_error("attrib: expected attribute character (a,h,r,s)", lineno);
		}

		if (attribs.find(c) != string::npos)
		{
			parse_error("attrib: repeated attribute character", lineno);
		}

		attribs += c;

		tok = scanner::scan();
	}

	if (tok != scanner::T_STRING)
	{
		parse_error("attrib: expected string", lineno);
	}

	string file(scanner::get_string());
	remove_quotes(file);

	s->kind = STATEMENT::attribK;
	s->S1 = file;
	s->S2 = attribs;
	s->lineno = lineno;
	s->next = next;

	return s;
}

STATEMENT *parse_open(STATEMENT *next)
{
	int lineno = scanner::get_line_number();

	STATEMENT *s = new STATEMENT;

	int tok = scanner::scan();

	s->I1 = 0;

	if (tok == scanner::T_IDENTIFIER &&
	        !stricmp(scanner::get_string(), "ro")) s->I1 = 1;
	else scanner::unscan();

	tok = scanner::scan();

	if (tok == scanner::T_STRING)
	{
		string file(scanner::get_string());
		remove_quotes(file);

		s->S1 = file;
		s->I2 = 0;

	} else if (tok == scanner::T_dialog) {

		s->I2 = 1;

		tok = scanner::scan();

		if (tok == scanner::T_STRING)
		{
			string title(scanner::get_string());
			remove_quotes(title);
			fix_string(title);

			s->I2 = 2;
			s->S1 = title;

		} else {

			scanner::unscan();
		}

	} else if (tok == scanner::T_argv) {

		s->I2 = 3;

	} else {
		parse_error("open: expected string, 'dialog' or 'argv'", lineno);
	}

	s->kind = STATEMENT::openK;
	s->lineno = lineno;
	s->next = next;

	return s;
}

STATEMENT *parse_backup(STATEMENT *next)
{
	int lineno = scanner::get_line_number();

	STATEMENT *s = new STATEMENT;

	int tok = scanner::scan();

	s->I1 = 0;

	if (tok == scanner::T_STRING)
	{
		string file(scanner::get_string());
		remove_quotes(file);

		s->I1 = 1;
		s->S1 = file;

	} else if (tok == scanner::T_IDENTIFIER) {

		if (!stricmp(scanner::get_string(), "on"))
		{
			s->I1 = 2;
		} else if (!stricmp(scanner::get_string(), "off")) {
			s->I1 = 3;
		}
	}

	if (s->I1 == 0) scanner::unscan();

	s->kind = STATEMENT::backupK;
	s->lineno = lineno;
	s->next = next;

	return s;
}

STATEMENT *parse_offset(STATEMENT *next)
{
	int lineno = scanner::get_line_number();

	STATEMENT *s = new STATEMENT;

	int tok = scanner::scan();

	s->I2 = 0;

	if (tok == scanner::T_PLUS)
	{
		s->I2 = 1;
	} else if (tok == scanner::T_DASH) {
		s->I2 = 2;
	} else {
		scanner::unscan();
	}

	tok = scanner::scan();

	if ((tok == scanner::T_INT) || (tok == scanner::T_HEXINT))
	{
		unsigned int val = strtoul(scanner::get_string(), 0, 0);

		s->I1 = val;

	} else {

		parse_error("offset: expected value", lineno);
	}

	s->kind = STATEMENT::offsetK;
	s->lineno = lineno;
	s->next = next;

	return s;
}

STATEMENT *parse_size(STATEMENT *next)
{
	int lineno = scanner::get_line_number();

	STATEMENT *s = new STATEMENT;

	int tok = scanner::scan();

	if ((tok == scanner::T_INT) || (tok == scanner::T_HEXINT))
	{
		unsigned int val = strtoul(scanner::get_string(), 0, 0);

		s->I1 = val;
		s->I2 = 0;

	} else if (tok == scanner::T_STRING) {

		string file(scanner::get_string());
		remove_quotes(file);

		s->S1 = file;
		s->I2 = 1;

	} else {

		parse_error("size: expected value or string", lineno);
	}

	s->kind = STATEMENT::sizeK;
	s->lineno = lineno;
	s->next = next;

	return s;
}

STATEMENT *parse_crc(STATEMENT *next)
{
	int lineno = scanner::get_line_number();

	STATEMENT *s = new STATEMENT;

	int tok = scanner::scan();

	if ((tok == scanner::T_INT) || (tok == scanner::T_HEXINT))
	{
		unsigned int val = strtoul(scanner::get_string(), 0, 0);

		s->I1 = val;
		s->I2 = 0;

	} else if (tok == scanner::T_STRING) {

		string file(scanner::get_string());
		remove_quotes(file);

		s->S1 = file;
		s->I2 = 1;

	} else {

		parse_error("crc: expected value or string", lineno);
	}

	s->kind = STATEMENT::crcK;
	s->lineno = lineno;
	s->next = next;

	return s;
}

STATEMENT *parse_compare(STATEMENT *next)
{
	int lineno = scanner::get_line_number();

	STATEMENT *s = new STATEMENT;

	int tok = scanner::scan();

	if (tok != scanner::T_STRING)
	{
		parse_error("compare: expected string", lineno);
	}

	string file1(scanner::get_string());
	remove_quotes(file1);

	tok = scanner::scan();

	if (tok != scanner::T_IDENTIFIER ||
	        stricmp(scanner::get_string(), "to"))
	{
		parse_error("compare: expected 'to'", lineno);
	}

	tok = scanner::scan();

	if (tok != scanner::T_STRING)
	{
		parse_error("compare: expected string", lineno);
	}

	string file2(scanner::get_string());
	remove_quotes(file2);

	s->kind = STATEMENT::compareK;
	s->S1 = file1;
	s->S2 = file2;
	s->lineno = lineno;
	s->next = next;

	return s;
}

STATEMENT *parse_write(STATEMENT *next)
{
	int lineno = scanner::get_line_number();

	int tok = scanner::scan();

	if ((tok != scanner::T_INT) &&
	        (tok != scanner::T_HEXINT) &&
	        (tok != scanner::T_STRING))
	{
		parse_error("write: expected data", lineno);
	}

	STATEMENT *s = new STATEMENT;

	while ((tok == scanner::T_INT) ||
	        (tok == scanner::T_HEXINT) ||
	        (tok == scanner::T_STRING))
	{
		if (tok == scanner::T_STRING)
		{
			string text(scanner::get_string());
			remove_quotes(text);
			fix_string(text);

			s->D1.insert(s->D1.end(), text.begin(), text.end());

		} else {

			unsigned int val = strtoul(scanner::get_string(), 0, 0);

			if (val & ~0x00ff)
			{
				parse_error("write: value larger than byte", lineno);
			}

			s->D1.push_back(val);
		}

		tok = scanner::scan();
	}

	scanner::unscan();

	if (s->D1.empty())
	{
		parse_error("write: expected data", lineno);
	}

	s->kind = STATEMENT::writeK;
	s->lineno = lineno;
	s->next = next;

	return s;
}

STATEMENT *parse_test(STATEMENT *next)
{
	int lineno = scanner::get_line_number();

	int tok = scanner::scan();

	if ((tok != scanner::T_INT) &&
	        (tok != scanner::T_HEXINT) &&
	        (tok != scanner::T_WILD) &&
	        (tok != scanner::T_STRING))
	{
		parse_error("test: expected data", lineno);
	}

	STATEMENT *s = new STATEMENT;

	s->I1 = 0;

	while ((tok == scanner::T_INT) ||
	        (tok == scanner::T_HEXINT) ||
	        (tok == scanner::T_WILD) ||
	        (tok == scanner::T_STRING))
	{
		s->I1++;

		if (tok == scanner::T_STRING)
		{
			string text(scanner::get_string());
			remove_quotes(text);
			fix_string(text);

			s->I1 = (s->I1 - 1) + text.size();

			for (string::const_iterator c = text.begin(); c != text.end(); ++c)
			{
				if (*c)
				{
					s->D1.push_back(*c);
				} else {
					s->D1.push_back(0);
					s->D1.push_back(0);
				}
			}

		} else if (tok == scanner::T_WILD) {

			s->D1.push_back(0);
			s->D1.push_back((char) 0xff);

		} else {

			unsigned int val = strtoul(scanner::get_string(), 0, 0);

			if (val & ~0x00ff)
			{
				parse_error("test: value larger than byte", lineno);
			}

			if (val == 0)
			{
				s->D1.push_back(0);
				s->D1.push_back(0);
			} else {
				s->D1.push_back(val);
			}
		}

		tok = scanner::scan();
	}

	scanner::unscan();

	if (s->D1.empty())
	{
		parse_error("test: expected data", lineno);
	}

	s->kind = STATEMENT::testK;
	s->lineno = lineno;
	s->next = next;

	return s;
}

STATEMENT *parse_search(STATEMENT *next)
{
	int lineno = scanner::get_line_number();

	int tok = scanner::scan();

	if ((tok != scanner::T_INT) &&
	        (tok != scanner::T_HEXINT) &&
	        (tok != scanner::T_STRING))
	{
		parse_error("search: expected data", lineno);
	}

	STATEMENT *s = new STATEMENT;

	s->I1 = 0;

	while ((tok == scanner::T_INT) ||
	        (tok == scanner::T_HEXINT) ||
	        (tok == scanner::T_WILD) ||
	        (tok == scanner::T_STRING))
	{
		s->I1++;

		if (tok == scanner::T_STRING)
		{
			string text(scanner::get_string());
			remove_quotes(text);
			fix_string(text);

			s->I1 = (s->I1 - 1) + text.size();

			for (string::const_iterator c = text.begin(); c != text.end(); ++c)
			{
				if (*c)
				{
					s->D1.push_back(*c);
				} else {
					s->D1.push_back(0);
					s->D1.push_back(0);
				}
			}

		} else if (tok == scanner::T_WILD) {

			s->D1.push_back(0);
			s->D1.push_back((char) 0xff);

		} else {

			unsigned int val = strtoul(scanner::get_string(), 0, 0);

			if (val & ~0x00ff)
			{
				parse_error("search: value larger than byte", lineno);
			}

			if (val == 0)
			{
				if (s->D1.empty())
				{
					s->D1.push_back(0);
				} else {
					s->D1.push_back(0);
					s->D1.push_back(0);
				}
			} else {
				s->D1.push_back(val);
			}
		}

		tok = scanner::scan();
	}

	scanner::unscan();

	if (s->D1.empty())
	{
		parse_error("search: expected data", lineno);
	}

	s->kind = STATEMENT::searchK;
	s->lineno = lineno;
	s->next = next;

	return s;
}

/*
   STATEMENT *parse_replace(STATEMENT *next)
   {
      int lineno = scanner::get_line_number();

      int tok = scanner::scan();

      if ((tok != scanner::T_INT) &&
	  (tok != scanner::T_HEXINT) &&
	  (tok != scanner::T_WILD))
      {
	 parse_error("replace: expected data", lineno);
      }

      STATEMENT *s = new STATEMENT;

      s->I1 = 0;

      while ((tok == scanner::T_INT) ||
	     (tok == scanner::T_HEXINT) ||
	     (tok == scanner::T_WILD))
      {
	 s->I1++;

	 if (tok == scanner::T_WILD)
	 {
	    s->D1.push_back(0);
	    s->D1.push_back(0xff);

	 } else {

	    unsigned int val = strtoul(scanner::get_string(), 0, 0);

	    if (val & ~0x00ff)
	    {
	       parse_error("replace: value larger than byte", lineno);
	    }

	    if (val == 0)
	    {
	       s->D1.push_back(0);
	       s->D1.push_back(0);
	    } else {
	       s->D1.push_back(val);
	    }
	 }

	 tok = scanner::scan();
      }

      if (tok != scanner::T_IDENTIFIER ||
          stricmp(scanner::get_string(), "with"))
      {
	 parse_error("replace: expected 'with'", lineno);
      }

      tok = scanner::scan();

      if ((tok != scanner::T_INT) && (tok != scanner::T_HEXINT))
      {
	 parse_error("replace with: expected data", lineno);
      }

      while ((tok == scanner::T_INT) || (tok == scanner::T_HEXINT))
      {
         unsigned int val = strtoul(scanner::get_string(), 0, 0);

	 if (val & ~0x00ff)
	 {
	    parse_error("replace with: value larger than byte", lineno);
	 }

	 s->D2.push_back(val);

	 tok = scanner::scan();
      }

      scanner::unscan();

      s->kind = STATEMENT::replaceK;
      s->lineno = lineno;
      s->next = next;

      return s;
   }
*/

STATEMENT *parse_yesno(STATEMENT *next)
{
	int lineno = scanner::get_line_number();

	STATEMENT *s = new STATEMENT;

	int tok = scanner::scan();

	if (tok != scanner::T_ifno)
	{
		parse_error("yesno: expected 'ifno'", lineno);
	}

	tok = scanner::scan();

	if (tok == scanner::T_goto)
	{
		tok = scanner::scan();

		if (tok != scanner::T_IDENTIFIER)
		{
			parse_error("yesno ifno goto: expected label", lineno);
		}

		string target(scanner::get_string());

		s->S1 = target;
		s->I1 = 1;

	} else if (tok == scanner::T_quit) {

		s->I1 = 0;

	} else {

		parse_error("yesno ifno: expected 'goto' or 'quit'", lineno);
	}

	s->kind = STATEMENT::yesnoK;
	s->lineno = lineno;
	s->next = next;

	return s;
}

STATEMENT *parse_import(STATEMENT *next)
{
	int lineno = scanner::get_line_number();

	STATEMENT *s = new STATEMENT;

	int tok = scanner::scan();

	if (tok != scanner::T_IDENTIFIER)
	{
		parse_error("import: expected 'fc' or 'reg'", lineno);
	}

	if (!stricmp(scanner::get_string(), "fc"))
	{
		s->I1 = 0;
	} else if (!stricmp(scanner::get_string(), "reg")) {
		s->I1 = 1;
	} else {
		parse_error("import: expected 'fc' or 'reg'", lineno);
	}

	tok = scanner::scan();

	if (tok != scanner::T_STRING)
	{
		parse_error("import: expected string", lineno);
	}

	string file(scanner::get_string());
	remove_quotes(file);

	s->S1 = file;
	s->kind = STATEMENT::importK;
	s->lineno = lineno;
	s->next = next;

	return s;
}

STATEMENT *parse_regopen(STATEMENT *next)
{
	int lineno = scanner::get_line_number();

	STATEMENT *s = new STATEMENT;

	int tok = scanner::scan();

	if (tok != scanner::T_IDENTIFIER)
	{
		parse_error("regopen: expected base key", lineno);
	}

	if (!strcmp(scanner::get_string(), "HKEY_CLASSES_ROOT"))
	{
		s->I2 = 0;
	} else if (!strcmp(scanner::get_string(), "HKEY_CURRENT_USER")) {
		s->I2 = 1;
	} else if (!strcmp(scanner::get_string(), "HKEY_LOCAL_MACHINE")) {
		s->I2 = 2;
	} else if (!strcmp(scanner::get_string(), "HKEY_USERS")) {
		s->I2 = 3;
	} else {
		parse_error("regopen: invalid base key", lineno);
	}

	tok = scanner::scan();

	if (tok != scanner::T_STRING)
	{
		parse_error("regopen: expected string", lineno);
	}

	string subkey(scanner::get_string());
	remove_quotes(subkey);

	s->I1 = 0;
	s->S1 = subkey;
	s->kind = STATEMENT::regcreateK;
	s->lineno = lineno;
	s->next = next;

	return s;
}

STATEMENT *parse_regcreate(STATEMENT *next)
{
	int lineno = scanner::get_line_number();

	STATEMENT *s = new STATEMENT;

	int tok = scanner::scan();

	if (tok != scanner::T_IDENTIFIER)
	{
		parse_error("regcreate: expected base key", lineno);
	}

	if (!strcmp(scanner::get_string(), "HKEY_CLASSES_ROOT"))
	{
		s->I2 = 0;
	} else if (!strcmp(scanner::get_string(), "HKEY_CURRENT_USER")) {
		s->I2 = 1;
	} else if (!strcmp(scanner::get_string(), "HKEY_LOCAL_MACHINE")) {
		s->I2 = 2;
	} else if (!strcmp(scanner::get_string(), "HKEY_USERS")) {
		s->I2 = 3;
	} else {
		parse_error("regcreate: invalid base key", lineno);
	}

	tok = scanner::scan();

	if (tok != scanner::T_STRING)
	{
		parse_error("regcreate: expected string", lineno);
	}

	string subkey(scanner::get_string());
	remove_quotes(subkey);

	s->I1 = 1;
	s->S1 = subkey;
	s->kind = STATEMENT::regcreateK;
	s->lineno = lineno;
	s->next = next;

	return s;
}

STATEMENT *parse_regset(STATEMENT *next)
{
	int lineno = scanner::get_line_number();

	STATEMENT *s = new STATEMENT;

	int tok = scanner::scan();

	if (tok != scanner::T_STRING)
	{
		parse_error("regset: expected string", lineno);
	}

	string name(scanner::get_string());
	remove_quotes(name);

	tok = scanner::scan();

	if ((tok == scanner::T_INT) || (tok == scanner::T_HEXINT))
	{
		unsigned int val = strtoul(scanner::get_string(), 0, 0);

		s->I1 = 0;
		s->I2 = val;

	} else if (tok == scanner::T_STRING) {

		string val(scanner::get_string());
		remove_quotes(val);

		s->I1 = 1;
		s->S2 = val;

	} else if (tok == scanner::T_binary) {

		s->I1 = 2;

		tok = scanner::scan();

		while ((tok == scanner::T_INT) ||
		        (tok == scanner::T_HEXINT) ||
		        (tok == scanner::T_STRING))
		{
			if (tok == scanner::T_STRING)
			{
				string text(scanner::get_string());
				remove_quotes(text);
				fix_string(text);

				s->D2.insert(s->D2.end(), text.begin(), text.end());

			} else {

				unsigned int val = strtoul(scanner::get_string(), 0, 0);

				if (val & ~0x00ff)
				{
					parse_error("regset: value larger than byte", lineno);
				}

				s->D2.push_back(val);
			}

			tok = scanner::scan();
		}

		scanner::unscan();

		if (s->D2.empty())
		{
			parse_error("regset: expected data", lineno);
		}

	} else {

		parse_error("regset: expected value, string or 'binary'", lineno);
	}

	s->S1 = name;
	s->kind = STATEMENT::regsetK;
	s->lineno = lineno;
	s->next = next;

	return s;
}

STATEMENT *parse_regdel(STATEMENT *next)
{
	int lineno = scanner::get_line_number();

	STATEMENT *s = new STATEMENT;

	int tok = scanner::scan();

	if (tok != scanner::T_STRING)
	{
		parse_error("regdel: expected string", lineno);
	}

	string name(scanner::get_string());
	remove_quotes(name);

	s->S1 = name;
	s->kind = STATEMENT::regdelK;
	s->lineno = lineno;
	s->next = next;

	return s;
}

STATEMENT *parse_end(STATEMENT *next)
{
	int lineno = scanner::get_line_number();

	STATEMENT *s = new STATEMENT;

	s->kind = STATEMENT::endK;
	s->lineno = lineno;
	s->next = next;

	return s;
}

STATEMENT *parse_statements(STATEMENT *s)
{
	int token;

	while ((token = scanner::scan()) != scanner::T_EOF)
	{
		switch (token) {
		case scanner::T_IDENTIFIER:
			s = parse_label(s);
			break;
		case scanner::T_quit:
			s = parse_quit(s);
			break;
		case scanner::T_title:
			s = parse_title(s);
			break;
		case scanner::T_dlgsize:
			s = parse_dlgsize(s);
			break;
		case scanner::T_colors:
			s = parse_colors(s);
			break;
		case scanner::T_print:
			s = parse_print(s);
			break;
		case scanner::T_cls:
			s = parse_cls(s);
			break;
		case scanner::T_start:
			s = parse_start(s);
			break;
		case scanner::T_goto:
			s = parse_goto(s);
			break;
		case scanner::T_onerror:
			s = parse_onerror(s);
			break;
		case scanner::T_attrib:
			s = parse_attrib(s);
			break;
		case scanner::T_open:
			s = parse_open(s);
			break;
		case scanner::T_backup:
			s = parse_backup(s);
			break;
		case scanner::T_offset:
			s = parse_offset(s);
			break;
		case scanner::T_size:
			s = parse_size(s);
			break;
		case scanner::T_crc:
			s = parse_crc(s);
			break;
		case scanner::T_compare:
			s = parse_compare(s);
			break;
		case scanner::T_write:
			s = parse_write(s);
			break;
		case scanner::T_test:
			s = parse_test(s);
			break;
		case scanner::T_search:
			s = parse_search(s);
			break;
//         case scanner::T_replace:
//            s = parse_replace(s);
//            break;
		case scanner::T_yesno:
			s = parse_yesno(s);
			break;
		case scanner::T_import:
			s = parse_import(s);
			break;
		case scanner::T_regopen:
			s = parse_regopen(s);
			break;
		case scanner::T_regcreate:
			s = parse_regcreate(s);
			break;
		case scanner::T_regset:
			s = parse_regset(s);
			break;
		case scanner::T_regdel:
			s = parse_regdel(s);
			break;
		case scanner::T_end:
			s = parse_end(s);
			return s;
			break;
		case scanner::T_UNKNOWN:
			parse_error("unknown token", scanner::get_line_number());
			break;
		default:
			parse_error("unknown or unexpected token", scanner::get_line_number());
			break;

		}
	}

	return s;
}

}

void apatch::parser::parse(SCRIPT *script)
{
	apatch::scanner::init(&(script->buffer[0]));

	script->statements = parse_statements(script->statements);
}
