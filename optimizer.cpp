//
// apatch - simple patching engine
//
// optimizer
//
// Copyright 1999-2014 Joergen Ibsen
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include <iostream>
#include <string>

#include "optimizer.hpp"

namespace {

using namespace std;
using namespace apatch;

bool changed;

typedef bool(*OPTI)(STATEMENT **);

// error handler
void optimize_error(char *what)
{
	printf("\n\007[pp] error : %s\n", what);

	exit(-1);
}

// warning handler
void optimize_warning(char *what)
{
	printf("[pp] warning : %s\n", what);
}


bool remove_dead_label(STATEMENT **s)
{
	if ((*s)->kind == STATEMENT::labelK)
	{
		if ((*s)->I1 == 0)
		{
			STATEMENT *tmp = (*s);

			// unlink s
			*s = (*s)->next;
			tmp->next = 0;

			delete tmp;

			return true;
		}
	}

	return false;
}

bool simplify_write_write(STATEMENT **s)
{
	STATEMENT *ss = *s;

	if ((ss->kind == STATEMENT::writeK) && ss->next)
	{
		if (ss->next->kind == STATEMENT::writeK)
		{
			STATEMENT *tmp = ss->next;

			// concatenate data
			tmp->D1.insert(tmp->D1.end(), ss->D1.begin(), ss->D1.end());
			ss->D1.swap(tmp->D1);

			// unlink tmp
			ss->next = tmp->next;
			tmp->next = 0;

			delete tmp;

			return true;
		}
	}

	return false;
}

bool simplify_print_print(STATEMENT **s)
{
	STATEMENT *ss = *s;

	if ((ss->kind == STATEMENT::printK) && ss->next)
	{
		if (ss->next->kind == STATEMENT::printK)
		{
			STATEMENT *tmp = ss->next;

			// concatenate strings
			ss->S1 = tmp->S1 + ss->S1;

			// unlink tmp
			ss->next = tmp->next;
			tmp->next = 0;

			delete tmp;

			return true;
		}
	}

	return false;
}

bool simplify_cls_cls(STATEMENT **s)
{
	STATEMENT *ss = *s;

	if ((ss->kind == STATEMENT::clsK) && ss->next)
	{
		if (ss->next->kind == STATEMENT::clsK)
		{
			STATEMENT *tmp = ss->next;

			ss->next = tmp->next;
			tmp->next = 0;

			delete tmp;

			return true;
		}
	}

	return false;
}

bool swap_print_title(STATEMENT **s)
{
	STATEMENT *ss = *s;

	if ((ss->kind == STATEMENT::titleK) && ss->next)
	{
		if (ss->next->kind == STATEMENT::printK)
		{
			STATEMENT *tmp = ss->next;

			*s = tmp;
			ss->next = tmp->next;
			tmp->next = ss;

			return true;
		}
	}

	return false;
}

bool simplify_print_cls(STATEMENT **s)
{
	STATEMENT *ss = *s;

	if ((ss->kind == STATEMENT::clsK) && ss->next)
	{
		if (ss->next->kind == STATEMENT::printK)
		{
			STATEMENT *tmp = ss->next;

			ss->next = tmp->next;
			tmp->next = 0;

			delete tmp;

			return true;
		}
	}

	return false;
}

bool simplify_title_title(STATEMENT **s)
{
	STATEMENT *ss = *s;

	if ((ss->kind == STATEMENT::titleK) && ss->next)
	{
		if (ss->next->kind == STATEMENT::titleK)
		{
			STATEMENT *tmp = ss->next;

			ss->next = tmp->next;
			tmp->next = 0;

			delete tmp;

			return true;
		}
	}

	return false;
}

const int OPTS = 5;

OPTI optimizations[OPTS] = {
	remove_dead_label,

//      simplify_write_write,
//      simplify_offset_offset,
//      simplify_onerror_onerror,
	simplify_print_print,
	simplify_title_title,
	simplify_cls_cls,
//      simplify_goto_goto,

//      simplify_quit_statement,
	simplify_print_cls,

	// print before write, offset, onerror
//      swap_write_offset_onerror__print,

	// title before write, offset, onerror, print
//      swap_write_offset_onerror_print__title,

	// onerror after write, offset
//      swap_onerror__write_offset,
};

void optimize_statement_traverse(STATEMENT **s)
{
	bool change = true;

	if (*s)
	{
		while (change)
		{
			change = false;
			for (int i = 0; i < OPTS; ++i)
			{
				change = change || optimizations[i](s);
			}
			changed = changed || change;
		}

		optimize_statement_traverse(&((*s)->next));
	}
}

void optimize_statement(STATEMENT **s)
{
	changed = true;

	while (changed)
	{
		changed = false;
		optimize_statement_traverse(s);
	}
}



void optimize_statements(STATEMENT *s)
{
	if (s == 0) return;

	if (s->next) optimize_statements(s->next);

	optimize_statement(&s);
}

}

void apatch::optimizer::optimize(SCRIPT *script)
{
	optimize_statement(&(script->statements));
}
