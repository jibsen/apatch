//
// apatch - simple patching engine
//
// symbol
//
// Copyright 1999-2014 Joergen Ibsen
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include <string>
#include <map>
#include <cstdio>

#include "symbol.hpp"

namespace {

using namespace std;
using namespace apatch;

map<string, STATEMENT *> labels;

// error handler
void symbol_error(char *what, int line)
{
	printf("\n\007[s] error at line %d : %s\n", line, what);

	exit(-1);
}

// warning handler
void symbol_warning(char *what, int line)
{
	printf("[s] warning at line %d : %s\n", line, what);
}

void scan_labels(STATEMENT *s)
{
	if (s->next) scan_labels(s->next);

	if (s->kind == STATEMENT::labelK)
	{
		if (!labels.insert(make_pair(s->S1, s)).second)
		{
			symbol_error("redefinition of label", s->lineno);
		}
	}
}

void symbol_goto(STATEMENT *s)
{
	map<string, STATEMENT *>::iterator idx = labels.find(s->S1);

	if (idx == labels.end())
	{
		symbol_error("goto: undefined label", s->lineno);
	}

	s->target = idx->second;
	idx->second->I1++;
}

void symbol_onerror(STATEMENT *s)
{
	// skip if: onerror quit
	if (s->I1 == 0) return;

	map<string, STATEMENT *>::iterator idx = labels.find(s->S1);

	if (idx == labels.end())
	{
		symbol_error("onerror goto: undefined label", s->lineno);
	}

	s->target = idx->second;
	idx->second->I1++;
}

void symbol_yesno(STATEMENT *s)
{
	// skip if: yesno ifno quit
	if (s->I1 == 0) return;

	map<string, STATEMENT *>::iterator idx = labels.find(s->S1);

	if (idx == labels.end())
	{
		symbol_error("yesno ifno goto: undefined label", s->lineno);
	}

	s->target = idx->second;
	idx->second->I1++;
}

void symbol_statement(STATEMENT *s)
{
	if (s == 0) return;

	if (s->next) symbol_statement(s->next);

	switch (s->kind) {
	case STATEMENT::gotoK:
		symbol_goto(s);
		break;
	case STATEMENT::onerrorK:
		symbol_onerror(s);
		break;
	case STATEMENT::yesnoK:
		symbol_yesno(s);
		break;
	default:
		/* nothing */
		break;
	}
}

}

void apatch::symbol::symbol(SCRIPT *script)
{
	labels.clear();

	scan_labels(script->statements);

	symbol_statement(script->statements);

	labels.clear();
}
