//
// apatch - simple patching engine
//
// weeder
//
// Copyright 1999-2014 Joergen Ibsen
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include <iostream>
#include <string>

#include "weeder.hpp"

namespace {

using namespace std;
using namespace apatch;

bool has_seen_start;
bool has_seen_end;

// error handler
void weed_error(char *what, int line)
{
	printf("\n\007[w] error at line %d : %s\n", line, what);

	exit(-1);
}

void weed_error(char *what)
{
	printf("\n\007[w] error : %s\n", what);

	exit(-1);
}

// warning handler
void weed_warning(char *what, int line)
{
	printf("[w] warning at line %d : %s\n", line, what);
}

void weed_warning(char *what)
{
	printf("[w] warning : %s\n", what);
}

// add weed functions

void weed_label(STATEMENT *s)
{
	if (!has_seen_start) weed_error("label before 'start'", s->lineno);
}

void weed_quit(STATEMENT *s)
{
	if (!has_seen_start) weed_error("'quit' before 'start'", s->lineno);
}

void weed_title(STATEMENT *s)
{
	/* nothing */
}

void weed_dlgsize(STATEMENT *s)
{
	if (has_seen_start) weed_error("'dlgsize' after 'start'", s->lineno);
}

void weed_colors(STATEMENT *s)
{
	if (has_seen_start) weed_error("'colors' after 'start'", s->lineno);
}

void weed_print(STATEMENT *s)
{
	/* nothing */
}

void weed_cls(STATEMENT *s)
{
	/* nothing */
}

void weed_start(STATEMENT *s)
{
	/* nothing */
}

void weed_goto(STATEMENT *s)
{
	if (!has_seen_start) weed_error("'goto' before 'start'", s->lineno);
}

void weed_onerror(STATEMENT *s)
{
	/* nothing */
}

void weed_attrib(STATEMENT *s)
{
	if (!has_seen_start) weed_error("'attrib' before 'start'", s->lineno);
}

void weed_open(STATEMENT *s)
{
	if (!has_seen_start) weed_error("'open' before 'start'", s->lineno);
}

void weed_backup(STATEMENT *s)
{
	if (!has_seen_start) weed_error("'backup' before 'start'", s->lineno);
}

void weed_offset(STATEMENT *s)
{
	if (!has_seen_start) weed_error("'offset' before 'start'", s->lineno);
}

void weed_size(STATEMENT *s)
{
	if (!has_seen_start) weed_error("'size' before 'start'", s->lineno);
}

void weed_crc(STATEMENT *s)
{
	if (!has_seen_start) weed_error("'crc' before 'start'", s->lineno);
}

void weed_compare(STATEMENT *s)
{
	if (!has_seen_start) weed_error("'compare' before 'start'", s->lineno);
}

void weed_write(STATEMENT *s)
{
	if (!has_seen_start) weed_error("'write' before 'start'", s->lineno);
}

void weed_test(STATEMENT *s)
{
	if (!has_seen_start) weed_error("'test' before 'start'", s->lineno);
}

void weed_search(STATEMENT *s)
{
	if (!has_seen_start) weed_error("'search' before 'start'", s->lineno);
}

void weed_replace(STATEMENT *s)
{
	if (!has_seen_start) weed_error("'replace' before 'start'", s->lineno);
}

void weed_yesno(STATEMENT *s)
{
	if (!has_seen_start) weed_error("'yesno' before 'start'", s->lineno);
}

void weed_import(STATEMENT *s)
{
	if (!has_seen_start) weed_error("'import' before 'start'", s->lineno);
}

void weed_regcreate(STATEMENT *s)
{
	if (!has_seen_start) weed_error("'regcreate' before 'start'", s->lineno);
}

void weed_regset(STATEMENT *s)
{
	if (!has_seen_start) weed_error("'regset' before 'start'", s->lineno);
}

void weed_regdel(STATEMENT *s)
{
	if (!has_seen_start) weed_error("'regdel' before 'start'", s->lineno);
}

void weed_end(STATEMENT *s)
{
	if (!has_seen_start) weed_error("'end' before 'start'", s->lineno);
}

void weed_statement(STATEMENT *s)
{
	if (s == 0) return;

	if (s->next) weed_statement(s->next);

	switch (s->kind) {
	case STATEMENT::labelK:
		weed_label(s);
		break;
	case STATEMENT::quitK:
		weed_quit(s);
		break;
	case STATEMENT::titleK:
		weed_title(s);
		break;
	case STATEMENT::dlgsizeK:
		weed_dlgsize(s);
		break;
	case STATEMENT::colorsK:
		weed_colors(s);
		break;
	case STATEMENT::printK:
		weed_print(s);
		break;
	case STATEMENT::clsK:
		weed_cls(s);
		break;
	case STATEMENT::startK:
		has_seen_start = true;
		weed_start(s);
		break;
	case STATEMENT::gotoK:
		weed_goto(s);
		break;
	case STATEMENT::onerrorK:
		weed_onerror(s);
		break;
	case STATEMENT::attribK:
		weed_attrib(s);
		break;
	case STATEMENT::openK:
		weed_open(s);
		break;
	case STATEMENT::backupK:
		weed_backup(s);
		break;
	case STATEMENT::offsetK:
		weed_offset(s);
		break;
	case STATEMENT::sizeK:
		weed_size(s);
		break;
	case STATEMENT::crcK:
		weed_crc(s);
		break;
	case STATEMENT::compareK:
		weed_compare(s);
		break;
	case STATEMENT::writeK:
		weed_write(s);
		break;
	case STATEMENT::testK:
		weed_test(s);
		break;
	case STATEMENT::searchK:
		weed_search(s);
		break;
	case STATEMENT::replaceK:
		weed_replace(s);
		break;
	case STATEMENT::yesnoK:
		weed_yesno(s);
		break;
	case STATEMENT::importK:
		weed_import(s);
		break;
	case STATEMENT::regcreateK:
		weed_regcreate(s);
		break;
	case STATEMENT::regsetK:
		weed_regset(s);
		break;
	case STATEMENT::regdelK:
		weed_regdel(s);
		break;
	case STATEMENT::endK:
		has_seen_end = true;
		weed_end(s);
		break;
	default:
		/* nothing */
		break;
	}
}

}

void apatch::weeder::weed(SCRIPT *script)
{
	has_seen_start = false;
	has_seen_end = false;

	weed_statement(script->statements);

	if (!has_seen_start) weed_error("no 'start' found");
	if (!has_seen_end) weed_error("no 'end' found");
}
