//
// apatch - simple patching engine
//
// emitter
//
// Copyright 1999-2014 Joergen Ibsen
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef EMITTER_HPP_INCLUDED
#define EMITTER_HPP_INCLUDED

#include "script.hpp"
#include "statement.hpp"

namespace apatch {

namespace emitter {

const int C_quit           = 0;
const int C_title          = 1;
const int C_print          = 2;
const int C_cls            = 3;
const int C_start          = 4;
const int C_onerror        = 5;
const int C_open_rw        = 6;
const int C_open_ro        = 7;
const int C_open_dialog_rw = 8;
const int C_open_dialog_ro = 9;
const int C_open_argv_rw   = 10;
const int C_open_argv_ro   = 11;
const int C_backup         = 12;
const int C_size           = 13;
const int C_crc            = 14;
const int C_offset         = 15;
const int C_offset_add     = 16;
const int C_offset_sub     = 17;
const int C_write          = 18;
const int C_test           = 19;
const int C_search         = 20;
const int C_yesno          = 21;
const int C_goto           = 22;
const int C_attrib         = 23;
const int C_regcreate      = 24;
const int C_regset         = 25;
const int C_regdel         = 26;

const unsigned int NUM_CODES = 27;

void emit(SCRIPT *script);

}

}

#endif // EMITTER_HPP_INCLUDED
