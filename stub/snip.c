/*
 * snip - file cutter
 *
 * Copyright 1998-2013 Joergen Ibsen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define BUFSIZE (1024 * 1024)

FILE *infile = NULL;
FILE *outfile = NULL;

unsigned char buffer[BUFSIZE];

int nr = 1;
int first = 1;

const char *stylename[] = {
	"(BIN)", "(ASM)", "(C)", "(DEC)"
};

char *lineend = "\r\n";

void close_open_files(void)
{
	if (infile != NULL) {
		fclose(infile);
	}

	if (outfile != NULL) {
		fclose(outfile);
	}
}

void convert(const unsigned char *buffer, size_t len, FILE *ofile, int style, int last)
{
	size_t dist = len;

	switch (style) {
	case 0:
		if (fwrite(buffer, len, 1, ofile) != 1) {
			close_open_files();
			printf("Error writing output file!\n");
			exit(7);
		}
		break;

	case 1:
		if (first) {
			first = 0;
			fprintf(ofile, "   db 0%02Xh", *(buffer++));
			dist--;
		}

		for (; dist; dist--) {
			if (nr) {
				fprintf(ofile, ", 0%02Xh", *(buffer++));
			}
			else {
				fprintf(ofile, "%s   db 0%02Xh", lineend, *(buffer++));
			}
			nr = (nr + 1) % 10;
		}

		if (last) {
			fprintf(ofile, lineend);
		}
		break;

	case 2:
		if (first) {
			first = 0;
			fprintf(ofile, "   0x%02X", *(buffer++));
			dist--;
		}

		for (; dist; dist--) {
			if (nr) {
				fprintf(ofile, ", 0x%02X", *(buffer++));
			}
			else {
				fprintf(ofile, ",%s   0x%02X", lineend, *(buffer++));
			}
			nr = (nr + 1) % 10;
		}

		if (last) {
			fprintf(ofile, lineend);
		}
		break;

	case 3:
		if (first) {
			first = 0;
			fprintf(ofile, "%u", *(buffer++));
			dist--;
		}

		for (; dist; dist--) {
			if (nr) {
				fprintf(ofile, ",%u", *(buffer++));
			}
			else {
				fprintf(ofile, ",%s%u", lineend, *(buffer++));
			}
			nr = (nr + 1) % 20;
		}

		if (last) {
			fprintf(ofile, lineend);
		}
		break;
	}
}

int main(int argc, char *argv[])
{
	size_t length, copy_length;
	size_t start_point, end_point;
	int datastyle;
	int argid[] = { 0, 0, 0, 0 };
	int numargs = 0;
	int i;

	printf("snip file cutter v1.15\n"
	       "Copyright 1998-2013 Joergen Ibsen (www.ibsensoftware.com)\n\n");

	datastyle = 0;

	/* parse command line */
	for (i = 1; i < argc; ++i) {
		if ((*argv[i] == '-') || (*argv[i] == '/')) {
			char *p = argv[i];

			while (*(++p)) {
				switch (*p) {
				case 'a':
				case 'A':
					datastyle = 1;
					break;
				case 'c':
				case 'C':
					datastyle = 2;
					break;
				case 'd':
				case 'D':
					datastyle = 3;
					break;
				case 'u':
				case 'U':
					lineend = "\n";
					break;
				default:
					printf("Error: unknown switch '-%c'\n\n", *p);
					return 1;
				}
			}
		}
		else {
			if (numargs < 4) {
				argid[numargs++] = i;
			}
			else {
				printf("Error: too many arguments at '%s'\n\n", argv[i]);
				return 1;
			}
		}
	}

	if (numargs < 2) {
		printf("Licensed under the Apache License, Version 2.0.\n\n"
		       "  Syntax:  snip [options] <Input-file> <Output-file> [Start] [End]\n\n"
		       "  Options: (default output is binary)\n\n"
		       "    -a : Output as assembler style data\n"
		       "    -c : Output as hex C style data\n"
		       "    -d : Output as compact decimal C style data\n"
		       "    -u : Output LF only instead of CRLF\n\n"
		       "  If no positions are given, snip will process the whole file.\n"
		       "  If only one position is given, it will be used as the starting position.\n\n");
		return 0;
	}


	/* calculate start and end */
	start_point = 0;
	if (numargs > 2) {
		start_point = strtoul(argv[argid[2]], NULL, 0);
	}

	end_point = 0;
	if (numargs > 3) {
		end_point = strtoul(argv[argid[3]], NULL, 0);
	}

	if ((end_point != 0) && (start_point >= end_point)) {
		printf("Trying to copy negative length!\n");
		return 1;
	}

	printf(" - opening files\n");

	if ((infile = fopen(argv[argid[0]], "rb")) == NULL) {
		close_open_files();
		printf("Unable to open input file!\n");
		return 2;
	}

	if ((outfile = fopen(argv[argid[1]], "wb")) == NULL) {
		close_open_files();
		printf("Unable to open output file!\n");
		return 3;
	}

	if (fseek(infile, 0, SEEK_END)) {
		close_open_files();
		printf("Error seeking input file!\n");
		return 5;
	}
	length = ftell(infile);

	if ((start_point > length) || (end_point > length)) {
		close_open_files();
		printf("Pointers exceed file size!\n");
		return 4;
	}

	if (end_point == 0) {
		end_point = length;
	}

	copy_length = end_point - start_point;

	printf("   - start point = %-10lu (%lXh)\n"
	       "   - end point   = %-10lu (%lXh)\n"
	       "   - total bytes = %-10lu (%lXh)\n",
	       (unsigned long) start_point, (unsigned long) start_point,
	       (unsigned long) end_point, (unsigned long) end_point,
	       (unsigned long) copy_length, (unsigned long) copy_length);

	if (fseek(infile, start_point, SEEK_SET)) {
		close_open_files();
		printf("Error seeking input file!\n");
		return 5;
	}

	printf(" - copying data %s", stylename[datastyle]);

	while (copy_length > BUFSIZE) {
		if (fread(buffer, BUFSIZE, 1, infile) != 1) {
			printf("Error reading input file!\n");
			return 6;
		}
		convert(buffer, BUFSIZE, outfile, datastyle, 0);
		copy_length -= BUFSIZE;
	}

	if (copy_length) {
		if (fread(buffer, copy_length, 1, infile) != 1) {
			printf("Error reading input file!\n");
			return 6;
		}
		convert(buffer, copy_length, outfile, datastyle, 1);
	}

	printf("\n - closing files\n");

	close_open_files();

	return 0;
}
