;;
;; apatch - simple patching engine
;;
;; Win32 patch stub
;;
;; Copyright 1999-2014 Joergen Ibsen
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.
;;

format PE GUI 4.0 on 'stub64.exe'

entry __ENTRY32

include 'win32a.inc'

include 'patch.inc'

CRYPTED = 1

macro remtag value
{
    db '(!', value, ')'
}

section '.rsrc' data readable resource from 'rsrc.res'

; --- resources ---------------------------------------------------

section '.flat' code data readable writeable executable import

__SECTION_START:

; --- imports -----------------------------------------------------

    library kernel32, 'kernel32.dll'

    import kernel32,\
           LoadLibrary,    'LoadLibraryA'   ,\
           GetProcAddress, 'GetProcAddress' ,\
           ExitProcess,    'ExitProcess'

; --- entrypoint --------------------------------------------------

align 16

__ENTRY32:

__INIT:
    push   edx

    xor    eax, eax

if CRYPTED = 0
    call   esi_label
esi_label:
    inc    edi
    pop    esi
    sub    esi, 0fh
else
    db     0e8h, 0ffh, 0ffh, 0ffh, 0ffh
esi_label:
    db     0c7h, 05eh, 083h, 0eeh, 00fh ; <- this last 00fh can be changed
end if

    add    eax, (__CRYPTED_DATA - esi_label + 0fh)

    pop    ecx ; ecx = edx

if CRYPTED = 0
    jmp    .label
    db     0c0h
  .label:
else
    db     0e9h, 001h, 000h, 000h, 000h
    db     0c0h
end if

    cld

    sub    ecx, edx ; ecx = 0

    ; edi -> __CRYPTED_DATA
    lea    edi, [esi + eax]

    add    eax, ecx ; does nothing

__DECRYPT:

    ; edi -> crypted data

    ; ecx must be zero

    ; at this point, eax should have a fixed value --
    ; none of the three random values below may trigger
    ; a jump!

    push   decrypt_four       ; push return address

fake_test:
    db     0xA9               ; test eax, x1 ; x1 != (__CRYPTED_DATA - esi_label + 0fh)
x1  dd     0x42424242         ;
    je     normalcodepair
    db     0x3d               ; cmp  eax, x2 ; x2 >= (__CRYPTED_DATA - esi_label + 0fh)
x2  dd     0x42424242         ; ja   shit
    ja     decrypt_dword
    db     0x35               ; xor  eax, x3 ; x3 != (__CRYPTED_DATA - esi_label + 0fh)
x3  dd     0x42424242         ;
    jz     mutex_check

    xor    eax, [edi]

    inc    edi

    mov    ebx, 2000h
    sub    ecx, 5
    jz     fake_test          ; will never happen

decrypt_dword:

    dec    edi

    ; Extended Congruential Generator
    ; from The Marsaglia Random Number CD-ROM
    ; x_n = 2^13 * (x_{n-1} + x_{n-2} + x_{n-3}) mod 2^32 - 5

    mov    eax, [x1]

    mov    edx, [x2]
    mov    [x2], eax
    add    eax, edx

    add    eax, [x3]
    mov    [x3], edx

    call   mul_ebx_ret

    div    ecx
    mov    [x1], edx

    ; edx = random dword

if CRYPTED = 0
    jmp    .label
    db     069h
  .label:
else
    db     0e9h, 001h, 000h, 000h, 000h
    db     069h
end if

if CRYPTED = 0
    xor    edx, edx
else
    xor    [edi], edx         ; sets carry = 0
end if

    sbb    edi, ecx           ; ecx = -5, so this does add edi, 5

mul_ebx_ret:
if CRYPTED = 0
    pop    ebp
    mul    ebx
    jmp    ebp
else
    db     05dh, 0f7h, 0e3h
    db     0e9h, 0ffh, 0ffh, 0ffh, 0ffh
    db     0e5h
end if

decrypt_four:
    call   decrypt_dword
    call   decrypt_dword

__CRYPTED_DATA:

    call   decrypt_dword
    call   decrypt_dword

    ; cmp    edi, __PACKED_DATA
    db     081h, 0ffh
cmp_edi_end:
    dd     __PACKED_DATA
    jbe    decrypt_four


__DEPACK:

    ; assumes edi -> __END

    mov    esi, __PACKED_DATA

    push   esi

    sub    ebx, 2000h ; ebx = 0

    push   esi

    lea    edx, [ecx + 85h] ; dl = 80h

    push   edi

literal:
    movsb
    mov    bl, 2
nexttag:
    call   getbit
    jnc    literal

    xor    ecx, ecx
    call   getbit
    jnc    codepair
    xor    eax, eax
    call   getbit
    jnc    shortmatch
    mov    bl, 2
    inc    ecx
    mov    al, 10h
  .getmorebits:
    call   getbit
    adc    al, al
    jnc    .getmorebits
    jnz    domatch
    stosb
    jmp    nexttag
codepair:
    call   getgamma_no_ecx
    sub    ecx, ebx
    jnz    normalcodepair
    call   getgamma
    jmp    domatch_lastpos

shortmatch:
    lodsb
    shr    eax, 1
    jz     donedepacking
    adc    ecx, ecx
    jmp    domatch_with_2inc

normalcodepair:
    xchg   eax, ecx
    dec    eax
    shl    eax, 8
    lodsb
    call   getgamma

    cmp    eax, 32000
    jae    domatch_with_2inc
    cmp    ah, 5
    jae    domatch_with_inc
    cmp    eax, 7fh
    ja     domatch_new_lastpos

domatch_with_2inc:
    inc    ecx

domatch_with_inc:
    inc    ecx

domatch_new_lastpos:
    xchg   eax, ebp
domatch_lastpos:
    mov    eax, ebp

    mov    bl, 1

domatch:
    push   esi
    mov    esi, edi
    sub    esi, eax
    rep    movsb
    pop    esi
    jmp    nexttag

getbit:
    add    dl, dl
    jnz    .stillbitsleft
    mov    dl, [esi]
    inc    esi
    adc    dl, dl
  .stillbitsleft:
    ret

getgamma:
    xor    ecx, ecx
getgamma_no_ecx:
    inc    ecx
  .getgammaloop:
    call   getbit
    adc    ecx, ecx
    call   getbit
    jc     .getgammaloop
    ret

donedepacking:
    mov    ecx, edi
    pop    esi      ; esi -> depacked data
    sub    ecx, esi ; edx = length of depacked data
    pop    edi      ; edi -> packed data
    rep    movsb    ; move depacked data into it's right place
    ret

SAFE_DIST_INIT = $ - __INIT

__PACKED_DATA:

__PATCH:

; --- handle imports ----------------------------------------------

    ; import strings are overwritten by imported addresses
handle_imports:
    mov    esi, import_strings
    mov    edi, esi
    jmp    .imports_test

  .next_import_library:
    invoke LoadLibrary, esi
    xchg   eax, ebx

    test   ebx, ebx
    jz     .import_error

  .next_import:
    lodsb
    cmp    al, 0
    jnz    .next_import

    cmp    byte [esi], 0
    je     .inc_imports_test

    invoke GetProcAddress, ebx, esi

    stosd

    test   eax, eax
    jnz    .next_import

  .import_error:
    invoke ExitProcess, 1

  .inc_imports_test:
    inc    esi
  .imports_test:
    cmp    byte [esi], 0
    jne    .next_import_library

; --- main --------------------------------------------------------

    call   fix_dialog_size

    invoke GetModuleHandle, 0
    mov    [hInst], eax

    ; create mutex to check if another instance is running
mutex_check:
    invoke CreateMutex, 0, FALSE, window_name
    mov    [hMutex], eax

    invoke GetLastError
    cmp    eax, ERROR_SUCCESS
    je     .mutex_ok

    cmp    eax, ERROR_ALREADY_EXISTS
    jne    .mutex_error

    invoke CloseHandle, [hMutex]

  .mutex_error:
    invoke ExitProcess, 1

  .mutex_ok:

    invoke GetCommandLine
    mov    [lpCmd], eax

    ; create suspended patch thread
thread_setup:
    invoke CreateThread, 0, 0, PatchThreadProc, 0, CREATE_SUSPENDED, ThreadId
    mov    [hThread], eax

    test   eax, eax
    jz     main_exit

SAFE_DIST_PATCH = $ - __PATCH

    invoke DialogBoxIndirectParam, [hInst], dlg_main, 0, MainDlgProc, 0

    mov    [dopatch], 0
    invoke ResumeThread, [hThread]
    invoke CloseHandle, [hThread]

main_exit:
    invoke ExitProcess, 0

; --- dialog proc -------------------------------------------------

MainDlgProc:
    mov    eax, [esp + 8]

    cmp    eax, WM_INITDIALOG
    je     .dlg_INITDIALOG

    cmp    eax, WM_CLOSE
    je     .dlg_CLOSE

    cmp    eax, WM_CTLCOLORSTATIC
    je     .dlg_CTLCOLORSTATIC

    cmp    eax, WM_COMMAND
    je     .dlg_COMMAND

    xor    eax, eax
    ret    16

  .dlg_INITDIALOG:
    mov    eax, [esp + 4]
    mov    [hDlg], eax

    invoke GetDlgItem, [hDlg], IDC_STATUS
    mov    [hStatus], eax

    invoke CreateSolidBrush, [bk_color]
    mov    [hBkBrush], eax

    invoke GetStockObject, OEM_FIXED_FONT
    invoke SendMessage, [hStatus], WM_SETFONT, eax, 0

    ; get ownership of the mutex
    ; - the patch thread will wait for it when
    ;   it gets to the start code
    ;   and we release it when the user presses
    ;   patch or quit
    invoke WaitForSingleObject, [hMutex], INFINITE

    ; start patch thread
    mov    [dopatch], 1
    invoke ResumeThread, [hThread]

    jmp    .dlg_exit

  .dlg_CLOSE:
    mov    eax, [esp + 4]
    invoke PostMessage, eax, WM_COMMAND, IDC_QUIT, 0
    jmp    .dlg_exit

  .dlg_CTLCOLORSTATIC:
    mov    eax, [esp + 16]
    cmp    eax, [hStatus]
    je     .set_back

    xor    eax, eax
    ret    16

  .set_back:
    mov    eax, [esp + 12]
    invoke SetTextColor, eax, [text_color]
    mov    eax, [esp + 12]
    invoke SetBkColor, eax, [bk_color]
    mov    eax, [hBkBrush]
    ret    16

  .dlg_COMMAND:
    mov    eax, [esp + 12]

    cmp    eax, IDC_PATCH
    je     .dlg_command_PATCH

    cmp    eax, IDC_QUIT
    je     .dlg_command_QUIT

    cmp    eax, CMD_START
    je     .dlg_command_START

    cmp    eax, CMD_CLSSTATUS
    je     .dlg_command_CLSSTATUS

    cmp    eax, CMD_ADDSTATUS
    je     .dlg_command_ADDSTATUS

    cmp    eax, CMD_ASK
    je     .dlg_command_ASK

    cmp    eax, IDC_YES
    je     .dlg_command_YES

    cmp    eax, IDC_NO
    je     .dlg_command_NO

    cmp    eax, CMD_DONE
    je     .dlg_command_DONE

    xor    eax, eax
    ret    16

  .dlg_command_PATCH:
    ; disable and hide patch button
    invoke GetDlgItem, [hDlg], IDC_PATCH
    push   eax
    invoke EnableWindow, eax, 0
    pop    eax
    invoke ShowWindow, eax, SW_HIDE

    ; disable and hide quit button
    invoke GetDlgItem, [hDlg], IDC_QUIT
    push   eax
    invoke EnableWindow, eax, 0
    pop    eax
    invoke ShowWindow, eax, SW_HIDE

    invoke ReleaseMutex, [hMutex]

    jmp    .dlg_exit

  .dlg_command_QUIT:

    mov    [dopatch], 0

    invoke ReleaseMutex, [hMutex]

    mov    edx, [esp + 4]
    invoke EndDialog, edx, eax

    jmp    .dlg_exit

  .dlg_command_START:
    invoke GetDlgItem, [hDlg], IDC_PATCH
    invoke EnableWindow, eax, 1

    invoke GetDlgItem, [hDlg], IDC_QUIT
    invoke EnableWindow, eax, 1

    invoke SendMessage, [hDlg], DM_SETDEFID, IDC_PATCH, 0
    invoke GetDlgItem, [hDlg], IDC_PATCH
    invoke SetFocus, eax

    jmp    .dlg_exit

  .dlg_command_CLSSTATUS:
    invoke SendMessage, [hStatus], WM_SETTEXT, 0, _empty

    jmp    .dlg_exit

  .dlg_command_ADDSTATUS:

    invoke SendMessage, [hStatus], EM_SETSEL, -1, 0
    mov    eax, [esp + 16]
    invoke SendMessage, [hStatus], EM_REPLACESEL, 0, eax

    jmp    .dlg_exit

  .dlg_command_ASK:
    invoke WaitForSingleObject, [hMutex], INFINITE

    mov    [dwAnswer], 0

    ; show and enable yes button
    invoke GetDlgItem, [hDlg], IDC_YES
    push   eax
    invoke ShowWindow, eax, SW_SHOW
    pop    eax
    invoke EnableWindow, eax, 1

    ; show and enable no button
    invoke GetDlgItem, [hDlg], IDC_NO
    push   eax
    invoke ShowWindow, eax, SW_SHOW
    pop    eax
    invoke EnableWindow, eax, 1

    invoke SendMessage, [hDlg], DM_SETDEFID, IDC_YES, 0
    invoke GetDlgItem, [hDlg], IDC_YES
    invoke SetFocus, eax

    jmp    .dlg_exit

  .dlg_command_YES:
    mov    [dwAnswer], 1

    ; fall through

  .dlg_command_NO:
    ; disable and hide yes button
    invoke GetDlgItem, [hDlg], IDC_YES
    push   eax
    invoke EnableWindow, eax, 0
    pop    eax
    invoke ShowWindow, eax, SW_HIDE

    ; disable and hide no button
    invoke GetDlgItem, [hDlg], IDC_NO
    push   eax
    invoke EnableWindow, eax, 0
    pop    eax
    invoke ShowWindow, eax, SW_HIDE

    invoke ReleaseMutex, [hMutex]

    jmp    .dlg_exit

  .dlg_command_DONE:
    ; show and enable quit button
    invoke GetDlgItem, [hDlg], IDC_QUIT
    push   eax
    invoke ShowWindow, eax, SW_SHOW
    pop    eax
    invoke EnableWindow, eax, 1

    invoke SendMessage, [hDlg], DM_SETDEFID, IDC_QUIT, 0
    invoke GetDlgItem, [hDlg], IDC_QUIT
    invoke SetFocus, eax

    jmp    .dlg_exit

  .dlg_exit:
    mov    eax, 1
    ret    16

; --- patch thread proc -------------------------------------------

PatchThreadProc:

    mov    eax, [hDlg]
    mov    [ofn.hwndOwner], eax

    mov    edx, __PATCHDATA
    mov    eax, edx
    add    eax, [edx]
    mov    [patch_CB], eax
    mov    eax, edx
    add    eax, [edx + 4]
    mov    [patch_DB], eax
    mov    [patch_IP], 1
    mov    [patch_EH], 0
    mov    [patch_FM.lpData], 0
    mov    [autobak], 0
    mov    [regkey], 0

    jmp    .patch_test

  .next_code:
    mov    eax, [patch_CB]    ; compute address of next code
    add    eax, [patch_IP]    ;

    movzx  edx, byte [eax]    ; read code

    inc    eax                ; eax -> code data

    call   [call_table + edx*4] ; call code function

  .patch_test:
    cmp    [dopatch], 0
    jne    .next_code

    ret    4

; ---

patch_quit:
    cmp    [patch_FM.lpData], 0
    je     .close_done

    call   patch_close

  .close_done:
    cmp    [regkey], 0
    je     .reg_close_done

    invoke RegCloseKey, [regkey]

  .reg_close_done:
    mov    [dopatch], 0
    invoke SendMessage, [hDlg], WM_COMMAND, CMD_DONE, 0
    ret

remtag C_open_rw ; <== TAG

patch_open_ro:
    ; set readonly access
    mov    [CFFlags], GENERIC_READ
    mov    [CFMFlags], PAGE_READONLY
    mov    [MVFlags], FILE_MAP_READ
    jmp    patch_file

patch_open_rw:
    ; set readwrite access
    mov    [CFFlags], GENERIC_READ or GENERIC_WRITE
    mov    [CFMFlags], PAGE_READWRITE
    mov    [MVFlags], FILE_MAP_ALL_ACCESS

patch_file:
    push   eax
    cmp    [patch_FM.lpData], 0
    je     .close_done

    call   patch_close

  .close_done:
    pop    eax
    mov    edx, [eax]
    add    edx, [patch_DB]

patch_open_create:
    and    [autobak], 0x7fffffff ; zero or positive

    ; copy filename to sz_filename, expanding environment-variable strings
    invoke ExpandEnvironmentStrings, edx, sz_filename, 1024

    test   eax, eax
    jz     patch_error
    cmp    eax, 1024
    ja     patch_error

    mov    edx, sz_filename
    add    eax, edx

    push   eax ; push position of end of filename

    invoke CreateFile, \
               edx, \
               [CFFlags], \
               FILE_SHARE_READ, \
               0, \
               OPEN_EXISTING, \
               FILE_ATTRIBUTE_NORMAL, \
               0

    mov    [patch_FM.hFile], eax

    pop    ecx ; ecx = position of end of filename

    ; append '.bak' to the filename in sz_filename
    mov    dword [ecx - 1], '.bak'
    mov    byte [ecx + 3], 0

    cmp    eax, INVALID_HANDLE_VALUE
    je     patch_error

    invoke GetFileSize, eax, 0
    mov    [patch_FM.dwSize], eax

    cmp    eax, 0xFFFFFFFF
    je     .error_close_file

    invoke CreateFileMapping, \
               [patch_FM.hFile], \
               0, \
               [CFMFlags], \
               0, \
               0, \
               0

    mov    [patch_FM.hFileMap], eax

    test   eax, eax
    jz     .error_close_file

    invoke MapViewOfFile, \
               eax, \
               [MVFlags], \
               0, \
               0, \
               0

    mov    [patch_FM.lpData], eax

    test   eax, eax
    jz     .error_close_filemap

    mov    [patch_FP], 0

    add    [patch_IP], 5
    ret

  .error_close_filemap:
    invoke CloseHandle, [patch_FM.hFileMap]
  .error_close_file:
    invoke CloseHandle, [patch_FM.hFile]
    jmp    patch_error

remtag C_open_rw ; <== TAG

remtag C_open_dialog_rw ; <== TAG

patch_open_dialog_ro:
    ; set readonly access
    mov    [CFFlags], GENERIC_READ
    mov    [CFMFlags], PAGE_READONLY
    mov    [MVFlags], FILE_MAP_READ
    jmp    patch_open_dialog

patch_open_dialog_rw:
    ; set readwrite access
    mov    [CFFlags], GENERIC_READ or GENERIC_WRITE
    mov    [CFMFlags], PAGE_READWRITE
    mov    [MVFlags], FILE_MAP_ALL_ACCESS

patch_open_dialog:

    mov    edx, [eax]

    test   edx, edx
    jz     .set_ofn_title

    add    edx, [patch_DB]

  .set_ofn_title:
    mov    [ofn.lpstrTitle], edx

    cmp    [patch_FM.lpData], 0
    je     .close_done

    call   patch_close

  .close_done:
    mov    [sz_filename + 1024], 0

    invoke GetOpenFileName, ofn

    test   eax, eax
    jz     patch_error

    mov    edx, sz_filename + 1024
    jmp    patch_open_create

remtag C_open_dialog_rw ; <== TAG

remtag C_open_argv_rw ; <== TAG

patch_open_argv_ro:
    ; set readonly access
    mov    [CFFlags], GENERIC_READ
    mov    [CFMFlags], PAGE_READONLY
    mov    [MVFlags], FILE_MAP_READ
    jmp    patch_open_argv

patch_open_argv_rw:
    ; set readwrite access
    mov    [CFFlags], GENERIC_READ or GENERIC_WRITE
    mov    [CFMFlags], PAGE_READWRITE
    mov    [MVFlags], FILE_MAP_ALL_ACCESS

patch_open_argv:
    cmp    [patch_FM.lpData], 0
    je     .close_done

    call   patch_close

  .close_done:
    call   next_lpCmd         ; get next cl arg into sz_filename + 1024

    mov    edx, sz_filename + 1024
    jmp    patch_open_create

remtag C_open_argv_rw ; <== TAG

remtag C_offset ; <== TAG

patch_offset:
    mov    edx, [eax]
    cmp    edx, [patch_FM.dwSize]
    jae    patch_error

    mov    [patch_FP], edx

    add    [patch_IP], 5
    ret

patch_offset_add:
    mov    edx, [patch_FP]
    add    edx, [eax]

    cmp    edx, [patch_FM.dwSize]
    jae    patch_error

    mov    [patch_FP], edx

    add    [patch_IP], 5
    ret

patch_offset_sub:
    mov    edx, [patch_FP]
    sub    edx, [eax]

    cmp    edx, [patch_FM.dwSize]
    jae    patch_error

    mov    [patch_FP], edx

    add    [patch_IP], 5
    ret

remtag C_offset ; <== TAG

remtag C_backup ; <== TAG

patch_backup:
    mov    edx, [eax]

    cmp    edx, -1
    jne    .not_backup_on

    mov    [autobak], 1
    jmp    .backup_done

  .not_backup_on:
    cmp    edx, -2
    jne    .not_backup_off

    mov    [autobak], 0
    jmp    .backup_done

  .not_backup_off:
    cmp    [patch_FM.lpData], 0
    je     patch_error

    test   edx, edx
    jz     .use_sz_filename

    add    edx, [patch_DB]

    mov    eax, sz_filename + 1024

    push   eax

    ; copy filename, expanding environment-variable strings
    invoke ExpandEnvironmentStrings, edx, eax, 1024

    pop    edx

    test   eax, eax
    jz     .expand_error
    cmp    eax, 1024
    jbe    .perform_backup

  .expand_error:
    jmp    patch_error

  .use_sz_filename:
    mov    edx, sz_filename

  .perform_backup:
    invoke CreateFile, \
               edx, \
               GENERIC_WRITE, \
               FILE_SHARE_READ, \
               0, \
               CREATE_ALWAYS, \
               FILE_ATTRIBUTE_NORMAL, \
               0

    mov    [hBFile], eax

    cmp    eax, INVALID_HANDLE_VALUE
    je     patch_error

    invoke WriteFile, \
               [hBFile], \
               [patch_FM.lpData], \
               [patch_FM.dwSize], \
               dwNum, \
               0

    test   eax, eax
    jz     .error_close_file

    mov    eax, [dwNum]
    cmp    eax, [patch_FM.dwSize]
    jne    .error_close_file

    invoke CloseHandle, [hBFile]

  .backup_done:
    add    [patch_IP], 5
    ret

  .error_close_file:
    invoke CloseHandle, [hBFile]
    jmp    patch_error

remtag C_backup ; <== TAG

remtag C_write ; <== TAG

patch_write:
    cmp    [patch_FM.lpData], 0
    je     patch_error

    cmp    [CFMFlags], PAGE_READONLY
    je     patch_error

    mov    ecx, [eax]

    mov    edx, [patch_FP]
    add    edx, ecx
    cmp    edx, [patch_FM.dwSize]
    ja     patch_error

    cmp    [autobak], 0       ; if zero or negative, no backup
    jle    .write_backup_done

    neg    [autobak] ; only backup on first write

    push   eax

    invoke CreateFile, \
               sz_filename, \
               GENERIC_WRITE, \
               FILE_SHARE_READ, \
               0, \
               CREATE_ALWAYS, \
               FILE_ATTRIBUTE_NORMAL, \
               0

    mov    [hBFile], eax

    cmp    eax, INVALID_HANDLE_VALUE
    je     .pop_patch_error

    invoke WriteFile, \
               [hBFile], \
               [patch_FM.lpData], \
               [patch_FM.dwSize], \
               dwNum, \
               0

    test   eax, eax
    jz     .pop_error_close_file

    mov    eax, [dwNum]
    cmp    eax, [patch_FM.dwSize]
    jne    .pop_error_close_file

    invoke CloseHandle, [hBFile]

    pop    eax
    jmp    .write_backup_done

  .pop_error_close_file:
    invoke CloseHandle, [hBFile]
  .pop_patch_error:
    pop    eax
    jmp    patch_error

  .write_backup_done:
    mov    ecx, [eax]

    push   esi
    push   edi

    mov    esi, [eax + 4]
    add    esi, [patch_DB]

    mov    edi, [patch_FM.lpData]
    add    edi, [patch_FP]

    add    [patch_FP], ecx    ; advance file pointer

    rep    movsb

    pop    edi
    pop    esi

    add    [patch_IP], 9
    ret

remtag C_write ; <== TAG

remtag C_test ; <== TAG

patch_test:
    mov    edx, [eax]

    mov    ecx, [patch_FP]
    add    ecx, edx
    cmp    ecx, [patch_FM.dwSize]
    ja     patch_error

    push   esi
    push   edi

    mov    esi, [eax + 4]
    add    esi, [patch_DB]

    mov    edi, [patch_FP]
    add    edi, [patch_FM.lpData]

  .test_more:
    mov    al, [esi]
    test   al, al
    jnz    .escape_done

    inc    esi
    mov    al, [esi]
    test   al, al
    jnz    .test_inc_test

  .escape_done:
    cmp    al, [edi]
    jne    .test_done

  .test_inc_test:
    inc    esi
    inc    edi

    dec    edx
    jnz    .test_more

  .test_done:
    pop    edi
    pop    esi

    jnz    patch_error

    add    [patch_IP], 9
    ret

remtag C_test ; <== TAG

remtag C_search ; <== TAG

patch_search:
    mov    edx, [eax]

    mov    ecx, [patch_FP]
    add    ecx, edx
    cmp    ecx, [patch_FM.dwSize]
    ja     patch_error

    mov    ecx, [patch_FM.dwSize]
    sub    ecx, edx
    sub    ecx, [patch_FP]
    inc    ecx

    push   esi
    push   edi

    mov    esi, [eax + 4]
    add    esi, [patch_DB]

    mov    edi, [patch_FP]
    add    edi, [patch_FM.lpData]

    dec    edi

    mov    al, [esi]

  .check_first_byte:
    inc    edi

    cmp    al, [edi]
    je     .possible_match

  .continue_search:
    dec    ecx
    jnz    .check_first_byte

    jmp    .error_pop

  .possible_match:
    pusha

    jmp    .match_test

  .match_more:
    mov    al, [esi]
    test   al, al
    jnz    .escape_done

    inc    esi
    mov    al, [esi]
    test   al, al
    jnz    .match_test

  .escape_done:
    cmp    al, [edi]
    jne    .match_done

  .match_test:
    inc    esi
    inc    edi

    dec    edx
    jnz    .match_more

  .match_done:
    popa

    jnz    .continue_search

    sub    edi, [patch_FM.lpData]
    mov    [patch_FP], edi

    pop    edi
    pop    esi

    add    [patch_IP], 9

    ret

  .error_pop:
    pop    edi
    pop    esi
    jmp    patch_error

remtag C_search ; <== TAG

remtag C_attrib ; <== TAG

patch_attrib:
    push   ebx
    mov    ebx, eax

    mov    edx, [ebx]
    add    edx, [patch_DB]

    mov    eax, sz_filename + 1024

    push   eax

    ; copy filename, expanding environment-variable strings
    invoke ExpandEnvironmentStrings, edx, eax, 1024

    pop    edx

    test   eax, eax
    jz     patch_error
    cmp    eax, 1024
    ja     patch_error

    push   edx

    invoke GetFileAttributes, edx

    pop    edx

    cmp    eax, 0xFFFFFFFF
    je     .error_pop

    and    eax, [ebx + 4]
    or     eax, [ebx + 8]

    invoke SetFileAttributes, edx, eax

    test   eax, eax
    jz     .error_pop

    pop    ebx

    add    [patch_IP], 13

    ret

  .error_pop:
    pop    ebx
    jmp    patch_error

remtag C_attrib ; <== TAG

patch_size:
    mov    edx, [eax]
    cmp    edx, [patch_FM.dwSize]
    jne    patch_error

    add    [patch_IP], 5
    ret

remtag C_crc ; <== TAG

patch_crc:
    push   ebx
    mov    ebx, [eax]

    stdcall is_crc32_asm, [patch_FM.lpData], [patch_FM.dwSize]

    cmp    eax, ebx
    pop    ebx
    jne    patch_error

    add    [patch_IP], 5
    ret

remtag C_crc ; <== TAG

remtag C_yesno ; <== TAG

patch_yesno:
    mov    edx, [eax]
    push   edx

    invoke SendMessage, [hDlg], WM_COMMAND, CMD_ASK, 0

    invoke WaitForSingleObject, [hMutex], INFINITE

    pop    eax

    cmp    [dwAnswer], 0
    je     .answer_done

    mov    eax, [patch_IP]
    add    eax, 5

  .answer_done:
    mov    [patch_IP], eax

    invoke ReleaseMutex, [hMutex]
    ret

remtag C_yesno ; <== TAG

patch_print:
    mov    edx, [eax]
    add    edx, [patch_DB]
    invoke SendMessage, [hDlg], WM_COMMAND, CMD_ADDSTATUS, edx

    add    [patch_IP], 5
    ret

patch_cls:
    invoke SendMessage, [hDlg], WM_COMMAND, CMD_CLSSTATUS, 0

    inc    [patch_IP]
    ret

patch_goto:
    mov    edx, [eax]
    mov    [patch_IP], edx
    ret

patch_onerror:
    mov    edx, [eax]
    mov    [patch_EH], edx

    add    [patch_IP], 5
    ret

remtag C_title ; <== TAG

patch_title:
    mov    edx, [eax]
    add    edx, [patch_DB]

    invoke SendMessage, [hDlg], WM_SETTEXT, 0, edx

    add    [patch_IP], 5
    ret

remtag C_title ; <== TAG

remtag C_regcreate ; <== TAG

patch_regcreate:
    cmp    [regkey], 0
    je     .reg_close_done

    pusha

    invoke RegCloseKey, [regkey]

    popa

  .reg_close_done:
    mov    [regkey], 0

    mov    ecx, [eax + 4]

    mov    edx, [eax + 8]
    add    edx, [patch_DB]

    cmp    dword [eax], 0
    je     .reg_open

    invoke RegCreateKeyEx, ecx, edx, \
           0, _empty, REG_OPTION_NON_VOLATILE, \
           KEY_WRITE, NULL, regkey, dwNum

    jmp    .reg_check_result

  .reg_open:
    invoke RegOpenKeyEx, ecx, edx, 0, KEY_WRITE, regkey

  .reg_check_result:
    cmp    eax, ERROR_SUCCESS
    jne    patch_error

    add    [patch_IP], 13
    ret

remtag C_regcreate ; <== TAG

remtag C_regset ; <== TAG

patch_regset:
    cmp    [regkey], 0
    je     patch_error

    mov    edx, [eax]         ; edx -> data
    add    edx, [patch_DB]    ;

    mov    ecx, [eax + 4]     ; ecx = length

    mov    eax, [eax + 8]     ; eax = type

    push   ecx                ; push length

    push   edx                ; push data

    push   eax                ; push type

    push   0                  ; push 0

    add    edx, ecx           ; edx -> valuename
    push   edx                ; push valuename

    push   [regkey]           ; push hKey

    call   [RegSetValueEx]

    cmp    eax, ERROR_SUCCESS
    jne    patch_error

    add    [patch_IP], 13
    ret

remtag C_regset ; <== TAG

remtag C_regdel ; <== TAG

patch_regdel:
    mov    ecx, [regkey]

    test   ecx, ecx
    jz     patch_error

    mov    edx, [eax]         ; edx -> valuename
    add    edx, [patch_DB]    ;

    invoke RegDeleteValue, ecx, edx

    cmp    eax, ERROR_SUCCESS
    jne    patch_error

    add    [patch_IP], 5
    ret

remtag C_regdel ; <== TAG

; ---

patch_error:
    mov    edx, [patch_EH]
    mov    [patch_IP], edx
    ret

patch_close:
    invoke UnmapViewOfFile, [patch_FM.lpData]
    invoke CloseHandle, [patch_FM.hFileMap]
    invoke CloseHandle, [patch_FM.hFile]
    mov    [patch_FM.lpData], 0
    ret

next_lpCmd:
    push   esi
    push   edi

    mov    esi, [lpCmd]

    mov    edi, sz_filename + 1024

    test   esi, esi
    jz     .done

    dec    esi

  .skip_ws:
    inc    esi
    mov    al, [esi]
    cmp    al, 20h
    je     .skip_ws

    test   al, al
    jz     .done

    xor    edx, edx

    dec    esi

  .change:
    xor    edx, ' '

  .next_char:
    inc    esi
    mov    al, [esi]

    cmp    al, dl
    jbe    .done

    cmp    al, '"'
    je     .change

    stosb
    jmp    .next_char

  .done:
    xor    eax, eax
    stosb

    mov    [lpCmd], esi

    pop    edi
    pop    esi

    ret

remtag C_crc ; <== TAG

is_crc32_asm:
    .len = 2*4 + 4 + 4
    .src = 2*4 + 4

    push   esi
    push   edi

    mov    esi, [esp + .src]  ; esi -> buffer
    mov    ecx, [esp + .len]  ; ecx =  length

    sub    eax, eax           ; crc = 0

    test   esi, esi
    jz     .c_exit

    test   ecx, ecx
    jz     .c_exit

    dec    eax                ; crc = 0xffffffff

    mov    edi, is_crctab_n   ; edi -> crctab

  .c_next_byte:
    xor    al, [esi]
    inc    esi

    mov    edx, 0x0f
    and    edx, eax

    shr    eax, 4

    xor    eax, [edi + edx*4]

    mov    edx, 0x0f
    and    edx, eax

    shr    eax, 4

    xor    eax, [edi + edx*4]

    dec    ecx
    jnz    .c_next_byte

    not    eax

  .c_exit:
    pop    edi
    pop    esi

    ret 8

    is_crctab_n dd 0x00000000, 0x1db71064, 0x3b6e20c8, 0x26d930ac, 0x76dc4190
                dd 0x6b6b51f4, 0x4db26158, 0x5005713c, 0xedb88320, 0xf00f9344
                dd 0xd6d6a3e8, 0xcb61b38c, 0x9b64c2b0, 0x86d3d2d4, 0xa00ae278
                dd 0xbdbdf21c

remtag C_crc ; <== TAG

; --- initialized data --------------------------------------------

call_table dd patch_quit
           dd patch_title
           dd patch_print
           dd patch_cls
           dd patch_start
           dd patch_onerror
           dd patch_open_rw
           dd patch_open_ro
           dd patch_open_dialog_rw
           dd patch_open_dialog_ro
           dd patch_open_argv_rw
           dd patch_open_argv_ro
           dd patch_backup
           dd patch_size
           dd patch_crc
           dd patch_offset
           dd patch_offset_add
           dd patch_offset_sub
           dd patch_write
           dd patch_test
           dd patch_search
           dd patch_yesno
           dd patch_goto
           dd patch_attrib
           dd patch_regcreate
           dd patch_regset
           dd patch_regdel

    dopatch  dd 0

remtag C_open_dialog_rw ; <== TAG

    filter db 'All files (*.*)',0,'*.*',0,0

    ofn OPENFILENAME sizeof.OPENFILENAME, 0, 0, filter, 0, 0, 1, \
                     sz_filename + 1024, \
                     1024, \
                     0, 0, \
                     current_dir, \
                     0, \
                     OFN_EXPLORER or OFN_LONGNAMES or OFN_FILEMUSTEXIST or OFN_HIDEREADONLY, \
                     0

remtag C_open_dialog_rw ; <== TAG

    current_dir db '.'
    _empty      db 0

dlgsize:
    dlgw        dd 334
    dlgh        dd 212

colors:
    text_color  dd 0x00A0D0A0
    bk_color    dd 0x00000000

import_strings db 'kernel32.dll',0
               db 'GetModuleHandleA',0
               db 'GetCommandLineA',0
               db 'CreateMutexA',0
               db 'GetLastError',0
               db 'ExpandEnvironmentStringsA',0
               db 'GetFileAttributesA',0
               db 'SetFileAttributesA',0
               db 'CreateFileA',0
               db 'GetFileSize',0
               db 'CreateFileMappingA',0
               db 'MapViewOfFile',0
               db 'UnmapViewOfFile',0
               db 'WriteFile',0
               db 'CloseHandle',0
               db 'CreateThread',0
               db 'ResumeThread',0
               db 'WaitForSingleObject',0
               db 'ReleaseMutex',0
               db 0
               db 'user32.dll',0
;               db 'MessageBoxA',0
               db 'DialogBoxIndirectParamW',0
               db 'SendMessageA',0
               db 'PostMessageA',0
               db 'EnableWindow',0
               db 'SetFocus',0
               db 'GetDlgItem',0
               db 'EndDialog',0
               db 'ShowWindow',0
               db 0
               db 'gdi32.dll',0
               db 'GetStockObject',0
               db 'CreateSolidBrush',0
               db 'SetTextColor',0
               db 'SetBkColor',0
               db 0
               db 'comdlg32.dll',0
               db 'GetOpenFileNameA',0
               db 0
               db 'advapi32.dll',0
               db 'RegCreateKeyExA',0
               db 'RegOpenKeyExA',0
               db 'RegSetValueExA',0
               db 'RegDeleteValueA',0
               db 'RegCloseKey',0
               db 0
               db 0

virtual at import_strings
    ; from kernel32.dll
    GetModuleHandle   dd ?
    GetCommandLine    dd ?
    CreateMutex       dd ?
    GetLastError      dd ?
    ExpandEnvironmentStrings dd ?
    GetFileAttributes dd ?
    SetFileAttributes dd ?
    CreateFile        dd ?
    GetFileSize       dd ?
    CreateFileMapping dd ?
    MapViewOfFile     dd ?
    UnmapViewOfFile   dd ?
    WriteFile         dd ?
    CloseHandle       dd ?
    CreateThread      dd ?
    ResumeThread      dd ?
    WaitForSingleObject dd ?
    ReleaseMutex      dd ?
    ; from user32.dll
;    MessageBox        dd ?
    DialogBoxIndirectParam dd ?
    SendMessage       dd ?
    PostMessage       dd ?
    EnableWindow      dd ?
    SetFocus          dd ?
    GetDlgItem        dd ?
    EndDialog         dd ?
    ShowWindow        dd ?
    ; from gdi32.dll
    GetStockObject    dd ?
    CreateSolidBrush  dd ?
    SetTextColor      dd ?
    SetBkColor        dd ?
    ; from comdlg32.dll
    GetOpenFileName   dd ?
    ; from advapi32.dll
    RegCreateKeyEx    dd ?
    RegOpenKeyEx      dd ?
    RegSetValueEx     dd ?
    RegDeleteValue    dd ?
    RegCloseKey       dd ?
end virtual

; --- dialog template ---------------------------------------------

sz_filename db 0

patch_start:
    call   next_lpCmd

    invoke SendMessage, [hDlg], WM_COMMAND, CMD_START, 0

    invoke WaitForSingleObject, [hMutex], INFINITE

    inc    [patch_IP]

    invoke ReleaseMutex, [hMutex]
    ret

fix_dialog_size:
    mov    eax, [dlgw]

    add    [dlgw_fixup_1], ax
    add    [dlgw_fixup_2], ax
    add    [dlgw_fixup_3], ax
    add    [dlgw_fixup_4], ax
    add    [dlgw_fixup_5], ax
    add    [dlgw_fixup_6], ax

    mov    eax, [dlgh]

    add    [dlgh_fixup_1], ax
    add    [dlgh_fixup_2], ax
    add    [dlgh_fixup_3], ax
    add    [dlgh_fixup_4], ax
    add    [dlgh_fixup_5], ax
    add    [dlgh_fixup_6], ax
    add    [dlgh_fixup_7], ax

    ret

align 4
dlg_main:
    dd DS_SETFONT or DS_MODALFRAME or DS_3DLOOK or DS_CENTER or DS_SETFOREGROUND or \
       WS_POPUP or WS_CAPTION or WS_SYSMENU
    dd 0                      ; extended style
    dw 6                      ; # items
    dw 0                      ; x
    dw 0                      ; y
dlgw_fixup_1:
    dw 0                      ; cx
dlgh_fixup_1:
    dw -1 -4                  ; cy

    dw 0                      ; no menu

    dw 0                      ; no class

    du 0

    du 8,'MS Sans Serif',0

    ; edit
align 4
    dd ES_MULTILINE or ES_LEFT or ES_AUTOVSCROLL or ES_READONLY or \
       WS_VISIBLE or WS_BORDER or WS_CHILD
    dd 0
    dw 5                      ; x
    dw 5                      ; y
dlgw_fixup_2:
    dw -5-5                   ; cx
dlgh_fixup_2:
    dw -5-20-5                ; cy
    dw IDC_STATUS             ; id

    dw 0xffff
    dw 0x0081                 ; edit

    dw 0

align 4
    dd 0                      ; no creation data

    ; static
align 4
    dd SS_LEFTNOWORDWRAP or SS_NOPREFIX or \
       WS_VISIBLE or WS_CHILD or WS_DISABLED
    dd 0
    dw 5                      ; x
dlgh_fixup_7:
    dw -5-10 -5               ; y
    dw 100                    ; cx
    dw 10                     ; cy
    dw -1                     ; id

    dw 0xffff
    dw 0x0082                 ; static

    du '[ apatch v1.11 ]',0

align 4
    dd 0                      ; no creation data

    ; button
align 4
    dd BS_PUSHBUTTON or BS_CENTER or BS_TEXT or BS_VCENTER or \
       WS_VISIBLE or WS_TABSTOP or WS_CHILD or WS_DISABLED
    dd 0
dlgw_fixup_3:
    dw -5-60-5-60 -1          ; x
dlgh_fixup_3:
    dw -5-15 -2               ; y
    dw 60                     ; cx
    dw 14                     ; cy
    dw IDC_PATCH              ; id

    dw 0xffff
    dw 0x0080                 ; button

    du '&Patch',0

align 4
    dd 0                      ; no creation data

    ; button
align 4
    dd BS_PUSHBUTTON or BS_CENTER or BS_TEXT or BS_VCENTER or \
       WS_VISIBLE or WS_TABSTOP or WS_CHILD or WS_DISABLED
    dd 0
dlgw_fixup_4:
    dw -5-60 -1               ; x
dlgh_fixup_4:
    dw -5-15 -2               ; y
    dw 60                     ; cx
    dw 14                     ; cy
    dw IDC_QUIT               ; id

    dw 0xffff
    dw 0x0080                 ; button

    du '&Quit'

align 4
    dd 0                      ; no creation data

    ; button
align 4
    dd BS_PUSHBUTTON or BS_CENTER or BS_TEXT or BS_VCENTER or \
       WS_TABSTOP or WS_CHILD or WS_DISABLED
    dd 0
dlgw_fixup_5:
    dw -5-60-5-60 -1          ; x
dlgh_fixup_5:
    dw -5-15 -2               ; y
    dw 60                     ; cx
    dw 14                     ; cy
    dw IDC_YES                ; id

    dw 0xffff
    dw 0x0080                 ; button

    du '&Yes',0

align 4
    dd 0                      ; no creation data

    ; button
align 4
    dd BS_PUSHBUTTON or BS_CENTER or BS_TEXT or BS_VCENTER or \
       WS_TABSTOP or WS_CHILD or WS_DISABLED
    dd 0
dlgw_fixup_6:
    dw -5-60 -1               ; x
dlgh_fixup_6:
    dw -5-15 -2               ; y
    dw 60                     ; cx
    dw 14                     ; cy
    dw IDC_NO                 ; id

    dw 0xffff
    dw 0x0080                 ; button

    du '&No'

align 4
    dd 0                      ; no creation data

    window_name db "apatch",0

; make sure sz_filename buffer is 2048 bytes large
times ((2048 - ($ - sz_filename)) / 7) db "apatch",0
times (2048 - ($ - sz_filename)) db 0

; --- uninitialized data ------------------------------------------

virtual at __INIT
    patch_FP dd ?    ; file position
    patch_CB dd ?    ; code base
    patch_IP dd ?    ; instruction pointer
    patch_DP dd ?    ; data pointer
    patch_FM FILEMAP ; file mapping
    if SAFE_DIST_INIT < ($ - patch_FP)
      error: virtual data at __INIT too large
    end if
end virtual

; this places the uninitialized data on top of the entrypoint
virtual at __PATCH
    hDll     dd ?
    hInst    dd ?
    lpCmd    dd ?
    hMutex   dd ?
    ThreadId dd ?
    hThread  dd ?
    hDlg     dd ?
    hBkBrush dd ?
    hStatus  dd ?
    hBFile   dd ?
    dwNum    dd ?
    dwAnswer dd ?
    patch_DB dd ?    ; data base
    ; flags for mapping file (default is readwrite)
    CFFlags  dd ?
    CFMFlags dd ?
    patch_EH dd ?    ; error handler
    MVFlags  dd ?
    autobak  dd ?
    regkey   dd ?
    if SAFE_DIST_PATCH < ($ - hDll)
      error: virtual data at __PATCH too large
    end if
end virtual

align 16

__PATCHDATA:

__END:
    dd    dlgsize - __SECTION_START
    dd    colors - __SECTION_START
    dd    __CRYPTED_DATA - esi_label + 0fh
    dd    cmp_edi_end - __SECTION_START
    dd    __CRYPTED_DATA - __SECTION_START
    dd    __PACKED_DATA - __SECTION_START
    dd    x1 - __SECTION_START
    dd    x2 - __SECTION_START
    dd    x3 - __SECTION_START
