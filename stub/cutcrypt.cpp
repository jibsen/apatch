//
// apatch - simple patching engine
//
// cut and crypt stub
//
// Copyright 1999-2014 Joergen Ibsen
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include <iostream>
#include <fstream>
#include <cstdio>

#include <windows.h>

#include "../apatch.hpp"

using namespace std;

namespace {

void crypt(char *data, unsigned int length,
           unsigned int x1, unsigned int x2, unsigned int x3)
{
	unsigned int *p = (unsigned int *)data;

	unsigned int limit = (length + 3) / 4;

	for (unsigned int i = 0; i < limit; ++i)
	{
		unsigned int x = x1 + x2 + x3;
		unsigned long long tmp = (unsigned long long)0x2000 * (unsigned long long)x;
		x = tmp % (unsigned long long)0xfffffffb;
		x3 = x2;
		x2 = x1;
		x1 = x;
		p[i] ^= x;
	}
}

};

int main(int argc, char *argv[])
{
	ifstream from(argv[1], ios::binary);

	if (!from)
	{
		cout << "ERROR: unable to open file\n";
		return 1;
	}

	size_t stub_size = from.seekg(0, ios::end).tellg();
	from.seekg(0);

	vector<char> stub(stub_size, 0);

	// read stub
	if (!from.read(&stub[0], stub_size))
	{
		cout << "ERROR: unable to read file\n";
		return 1;
	}

	// find end of stub data and cut
	for (size_t p = stub_size - 4; p > 0; p -= 4)
	{
		if (*((unsigned int*)&stub[p]) != 0)
		{
			stub_size = p + 4;
			break;
		}
	}

	// encrypt stub
	crypt(&stub[0], stub_size, STUB_CRYPT_1, STUB_CRYPT_2, STUB_CRYPT_3);

	// save to disk
	{
		ofstream st("stub.dat", ios::binary);
		st.write(&stub[0], stub_size);
	}

	return 0;
}
