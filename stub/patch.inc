
S_OK             = 0
S_FALSE          = 1

macro push [arg]
{
  if arg eqtype ""
    call @f
    db arg,0
    @@:
  else
    push arg
  end if
}

macro align value,fill
{
  if fill eq
    align value
  else
    local ..size
    virtual
      @@: align value
      ..size = $-@b
    end virtual
    times ..size db fill
  end if
}

struc FILEMAP
{
   .hFile    dd ?
   .hFileMap dd ?
   .dwSize   dd ?
   .lpData   dd ?
   .size     =  $ - .hFile
}

macro zeroalign value
 {
   times ((value-1) - (RVA $ + value-1) mod value) db 0
 }

IDI_ICON             = 100h

IDD_MAIN             = 2000h
IDC_STATUS           = 2001h
IDC_PATCH            = 2002h
IDC_QUIT             = 2003h
IDC_YES              = 2004h
IDC_NO               = 2005h

CMD_START            = 2010h
CMD_CLSSTATUS        = 2011h
CMD_ADDSTATUS        = 2012h
CMD_ASK              = 2013h
CMD_DONE             = 2014h

ERROR_SUCCESS        = 0
ERROR_ALREADY_EXISTS = 183

INFINITE             = -1

;IDOK                 = 1
;IDCANCEL             = 2
;IDABORT              = 3
;IDRETRY              = 4
;IDIGNORE             = 5
;IDYES                = 6
;IDNO                 = 7
;IDCLOSE              = 8
;IDHELP               = 9

DLGW                 = 334
DLGH                 = 212

EM_HIDESELECTION     = WM_USER + 63

DM_SETDEFID          = WM_USER + 1

OEM_FIXED_FONT       = 10
ANSI_FIXED_FONT      = 11

WHITE_BRUSH          = 0
LTGRAY_BRUSH         = 1
GRAY_BRUSH           = 2
DKGRAY_BRUSH         = 3
BLACK_BRUSH          = 4
NULL_BRUSH           = 5
HOLLOW_BRUSH         = NULL_BRUSH
WHITE_PEN            = 6
BLACK_PEN            = 7
NULL_PEN             = 8
OEM_FIXED_FONT       = 10
ANSI_FIXED_FONT      = 11
ANSI_VAR_FONT        = 12
SYSTEM_FONT          = 13
DEVICE_DEFAULT_FONT  = 14
DEFAULT_PALETTE      = 15
SYSTEM_FIXED_FONT    = 16
STOCK_LAST           = 16

macro m2m m2, m1 {

  push m1
  pop m2
}

C_quit           = 0
C_title          = 1
C_print          = 2
C_cls            = 3
C_start          = 4
C_onerror        = 5
C_open_rw        = 6
C_open_ro        = 7
C_open_dialog_rw = 8
C_open_dialog_ro = 9
C_open_argv_rw   = 10
C_open_argv_ro   = 11
C_backup         = 12
C_size           = 13
C_crc            = 14
C_offset         = 15
C_offset_add     = 16
C_offset_sub     = 17
C_write          = 18
C_test           = 19
C_search         = 20
C_yesno          = 21
C_goto           = 22
C_attrib         = 23
C_regcreate      = 24
C_regset         = 25
C_regdel         = 26
