//
// apatch - simple patching engine
//
// build code v1.11
//
// Copyright 1999-2014 Joergen Ibsen
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include <iostream>
#include <fstream>
#include <cstdio>
#include <ctime>

#include <windows.h>

#include "apatch.hpp"
#include "parser.hpp"
#include "weeder.hpp"
#include "symbol.hpp"
#include "coder.hpp"
#include "optimizer.hpp"
#include "resourcer.hpp"
#include "emitter.hpp"
#include "pprinter.hpp"
#include "remdead.hpp"

#include "util.hpp"
#include "crcfile.hpp"

#include "aplib.h"

using namespace std;
using namespace apatch;

namespace {

// error handler
void main_error(char *what)
{
	printf("\n\007[m] error : %s\n", what);

	exit(-1);
}

const unsigned char _stub[] = {

#include "stub/stub.h"

};
const int STUB_SIZE = sizeof (_stub);

int SWITCHd = 0;
int SWITCHc = 0;
int SWITCHo = 1;

void crypt(char *data, unsigned int length,
           unsigned int x1, unsigned int x2, unsigned int x3)
{
	unsigned int *p = (unsigned int *) data;

	unsigned int limit = (length + 3) / 4;

	for (unsigned int i = 0; i < limit; ++i)
	{
		unsigned int x = x1 + x2 + x3;
		unsigned long long tmp = (unsigned long long) 0x2000 * (unsigned long long) x;
		x = tmp % (unsigned long long) 0xfffffffb;
		x3 = x2;
		x2 = x1;
		x1 = x;
		p[i] ^= x;
	}
}

static void syntax()
{
	cout << "Licensed under the Apache License, Version 2.0.\n\n"
	     "  Syntax:  apatch [options] <script file> [output file]\n\n"
	     "  Options:\n"
	     "    -d  debug script\n"
	     "    -v  show version information\n\n"
	     "  'apatch -c <file>' can be used to calculate the CRC of the given file.\n\n";
}

static void version()
{
	cout << "  version     :  1.11.49\n"
	     "  license     :  Apache License, Version 2.0\n"
	     "  build time  :  " __DATE__ " " __TIME__ "\n"
	     "  patch stub  :  apatch Win32 2014.12.08\n"
#if defined(_MSC_VER)
	     "  compiler    :  Visual C++ " << _MSC_VER/100 << '.' << _MSC_VER%100 << "\n"
#elif defined(__GNUC__)
	     "  compiler    :  g++ (GCC) " __VERSION__ "\n"
#else
	     "  compiler    :  unknown\n"
#endif
	     "  assembler   :  FASM 1.71\n\n";
}

}

// =========================================================================
//  MAIN
// =========================================================================

int main(int argc, char *argv[])
{
	cout << "apatch simple patching engine v1.11\n"
	     "Copyright 1999-2014 Joergen Ibsen (www.ibsensoftware.com)\n\n";

	// write syntax when not enough parameters
	if (argc < 2)
	{
		syntax();
		return 1;
	}

	int scriptfilearg = 0, outfilearg = 0;

	// parse command line
	for (int i = 1; i < argc; i++)
	{
		if ((*argv[i] == '-') || (*argv[i] == '/'))
		{
			char *argptr = argv[i];

			while (*(++argptr))
			{
				switch (*argptr)
				{
				case 'c':
				case 'C':
					SWITCHc = 1;
					break;
				case 'd':
				case 'D':
					SWITCHd = 1;
					break;
				case 'o':
				case 'O':
					SWITCHo = 0;
					break;
				case 'v':
				case 'V':
					SWITCHd = 1;
					version();
					exit(0);
					break;
				default:
					syntax();
					cout << "\007[m] error : options : unknown switch '"
					     << *argptr << "'\n";
					exit(0);
				}
			}

		} else {

			if (scriptfilearg != 0)
			{
				if (outfilearg == 0) outfilearg = i;
			} else scriptfilearg = i;
		}
	}

	// print syntax if no script file
	if (scriptfilearg == 0)
	{
		syntax();
		return 1;
	}

	if (SWITCHc != 0)
	{
		cout << "- checking file '" << argv[scriptfilearg] << "'\n\n";
		printf("  CRC = 0x%08X\n", crc_file(argv[scriptfilearg]));
		return 0;
	}

	SCRIPT script;

	cout << "- reading script";
	{
		// open script file
		ifstream scriptfile(argv[scriptfilearg], ios::binary);

		if (!scriptfile) main_error("unable to open script file");

		// get script file size
		size_t scriptsize = scriptfile.seekg(0, ios::end).tellg();

		scriptfile.seekg(0, ios::beg);

		// resize script buffer
		script.buffer.resize(scriptsize);

		// read script file into buffer
		scriptfile.read(&(script.buffer[0]), scriptsize);
	}
	cout << '\n';

	cout << "- compiling script ";
	{
		cout << '.';
		parser::parse(&script);
		cout << '.';
		weeder::weed(&script);
		cout << '.';
		symbol::symbol(&script);

		if (SWITCHd)
		{
			cout << "\n\n=== pretty-print ===\n";
			pprinter::pretty_print(&script);
			cout << "======= done =======\n";

			return 0;
		}

		cout << '.';
		coder::code(&script);

		if (SWITCHo)
		{
			cout << '.';
			optimizer::optimize(&script);
		}

		cout << '.';
		resourcer::resource(&script);
		cout << '.';
		emitter::emit(&script);
	}
	cout << " done\n";

	/*
	   // output patch code and data sections to files for debugging
	   {
	      ofstream code("code.bin", ios::binary);
	      ofstream data("data.bin", ios::binary);

	      code.write(&script.code[0], script.code.size());
	      data.write(&script.data[0], script.data.size());
	   }
	*/

	cout << "- building patch ";

	cout << '.';
	vector<char> stub(_stub, _stub + STUB_SIZE);

	/*
	   // encrypt stub and save to disk
	   crypt(&stub[0], STUB_SIZE, STUB_CRYPT_1, STUB_CRYPT_2, STUB_CRYPT_3);
	   {
	      ofstream st("stub.dat", ios::binary);
	      st.write(&stub[0], STUB_SIZE);
	   }
	*/

	// decrypt stub
	crypt(&stub[0], STUB_SIZE, STUB_CRYPT_1, STUB_CRYPT_2, STUB_CRYPT_3);

	unsigned int colors_offs, dlgsize_offs;
	unsigned int crypt_cmp_offs, crypted_data_offs, packed_data_offs;
	unsigned int x1_offs, x2_offs, x3_offs, eax_test_val;

	// get offsets stored in the stub, and remove offsets
	{
		// at the end of the stub, there are a number of dwords which
		// contain offsets and values needed for creating the patch
		// executable (they are put there by the assembler)

		// get a few pointers to PE structures in the exe
		IMAGE_DOS_HEADER      *mzHeader   = (IMAGE_DOS_HEADER *) &stub[0];
		IMAGE_NT_HEADERS      *peHeader   = (IMAGE_NT_HEADERS *) ((unsigned int) &stub[0] + mzHeader->e_lfanew);
		IMAGE_SECTION_HEADER  *scnHeader  = (IMAGE_SECTION_HEADER *) ((unsigned int) peHeader + sizeof (IMAGE_NT_HEADERS));
		IMAGE_SECTION_HEADER  *scn2Header = (IMAGE_SECTION_HEADER *) ((unsigned int) scnHeader + sizeof (IMAGE_SECTION_HEADER));

		// all the offsets we get from the end of the stub are relative
		// to the code section (which is the second)
		unsigned int section_offs = scn2Header->PointerToRawData;

		// dlgsize_offs is the offset of two dwords which contain the
		// desired width and height of the dialog
		dlgsize_offs = section_offs + get_dword(&stub[STUB_SIZE - 36]);
		// colors_offs is the offset of two dwords which contain the
		// colors used for text and background in the edit control
		colors_offs = section_offs + get_dword(&stub[STUB_SIZE - 32]);
		// eax_test_val is the value that eax contains before the
		// three tests which may not succeed
		eax_test_val = section_offs + get_dword(&stub[STUB_SIZE - 28]);
		// crypt_cmp_offs is the offset of the value used to check for
		// the end of decryption
		crypt_cmp_offs = section_offs + get_dword(&stub[STUB_SIZE - 24]);
		// crypted_data_offs is the offset where the encrypted data starts
		crypted_data_offs = section_offs + get_dword(&stub[STUB_SIZE - 20]);
		// packed_data_offs is the offset where the packed data starts
		packed_data_offs = section_offs + get_dword(&stub[STUB_SIZE - 16]);
		// x1, x2, x3 are the offsets of the three initial values for the PRNG
		x1_offs = section_offs + get_dword(&stub[STUB_SIZE - 12]);
		x2_offs = section_offs + get_dword(&stub[STUB_SIZE - 8]);
		x3_offs = section_offs + get_dword(&stub[STUB_SIZE - 4]);

		// remove offsets from stub
		stub.erase(stub.end() - 36, stub.end());
	}

	cout << '.';
	// remove unused sections of code from the patch
	{
		remdead::remove_dead(stub, script.used_codes, emitter::NUM_CODES);
	}

	// set width, height and color
	{
		set_dword(&stub[dlgsize_offs], ((script.dlgw * 16) / 3) + 4 + 10);
		set_dword(&stub[dlgsize_offs + 4], ((script.dlgh * 111) / 15) + 5 + 30);
		set_dword(&stub[colors_offs], script.text_color);
		set_dword(&stub[colors_offs + 4], script.bk_color);
	}

	// append patch data to stub
	{
		// header: dd offset_of_code, dd offset_of_data
		append_dword(stub, 8);
		append_dword(stub, 8 + script.code.size());

		// add code and data to stub
		stub.insert(stub.end(), script.code.begin(), script.code.end());
		stub.insert(stub.end(), script.data.begin(), script.data.end());
	}

	// the unpacked data is from packed_data_offs to the end
	unsigned int unpacked_size = stub.size() - packed_data_offs;
	unsigned int packed_size;

	cout << '.';
	// pack stub
	{
		// get memory for call to aP_pack
		vector<char> workmem(aP_workmem_size(unpacked_size));
		vector<char> destination(aP_max_packed_size(unpacked_size));

		// compress the part of the stub which should be compressed
		packed_size = aP_pack ((unsigned char *) &stub[packed_data_offs],
		                       (unsigned char *) &destination[0],
		                       unpacked_size,
		                       (unsigned char *) &workmem[0],
		                       0,
		                       0);

		// copy packed data into stub, replacing the original data
		stub.erase(stub.begin() + packed_data_offs, stub.end());
		stub.insert(stub.end(), destination.begin(), destination.begin() + packed_size);
	}

	cout << '.';
	// encrypt stub
	{
		// adjust cmp for end of decryption
		unsigned int end = get_dword(&stub[crypt_cmp_offs]) + packed_size + 10;
		set_dword(&stub[crypt_cmp_offs], end);

		// initialize PRNG
		srand(time(0));

		// select seeds x1, x2, x3 for the patch PRNG, so they do not
		// conflict with the three tests against eax
		unsigned int x1, x2, x3;
		{
			unsigned int val;

			do val = (rand() * RAND_MAX) + rand();
			while (val == eax_test_val);
			x1 = val | eax_test_val;
			do val = (rand() * RAND_MAX) + rand();
			while (val <  eax_test_val);
			x2 = val;
			do val = (rand() * RAND_MAX) + rand();
			while (val == eax_test_val);
			x3 = val & (~eax_test_val);
		}

		// set the three seed values for the patch PRNG
		set_dword(&stub[x1_offs], x1);
		set_dword(&stub[x2_offs], x2);
		set_dword(&stub[x3_offs], x3);

		// the crypted data is from crypted_data_offs to the end
		unsigned int crypt_length = stub.size() - crypted_data_offs;

		// add a little extra space (crypt works on 4 bytes at a time)
		stub.insert(stub.end(), 10, 0);

		// encrypt
		crypt(&stub[crypted_data_offs], crypt_length, x1, x2, x3);

		// remove the extra space again
		stub.erase(stub.end() - 10, stub.end());

	}

	cout << '.';
	// finish stub executable
	{
		// get a few pointers to PE structures in the exe
		IMAGE_DOS_HEADER      *mzHeader   = (IMAGE_DOS_HEADER *) &stub[0];
		IMAGE_NT_HEADERS      *peHeader   = (IMAGE_NT_HEADERS *) ((unsigned int) &stub[0] + mzHeader->e_lfanew);
		IMAGE_SECTION_HEADER  *scnHeader  = (IMAGE_SECTION_HEADER *) ((unsigned int) peHeader + sizeof (IMAGE_NT_HEADERS));
		// IMAGE_FILE_HEADER     *fileHeader = &peHeader->FileHeader;
		IMAGE_OPTIONAL_HEADER *optHeader  = &peHeader->OptionalHeader;

		// get a pointer to the section header of the code section
		IMAGE_SECTION_HEADER  *scn2Header = (IMAGE_SECTION_HEADER *) ((unsigned int) scnHeader + sizeof (IMAGE_SECTION_HEADER));

		// image_real_size is the size of the stub
		unsigned int image_real_size = stub.size();
		unsigned int image_real_size_aligned = image_real_size + 0x1ff - ((image_real_size + 0x1ff) & 0x1ff);

		// section_real_size is the size of the code section
		unsigned int section_real_size = image_real_size - scn2Header->PointerToRawData;
		unsigned int section_real_size_aligned = section_real_size + 0x1ff - ((section_real_size + 0x1ff) & 0x1ff);

		// section_virtual_size is the size we will need for the code
		// section in order to be able to decompress inside it
		unsigned int section_virtual_size = section_real_size + unpacked_size + 1024; // TODO
		unsigned int section_virtual_size_aligned = section_virtual_size + 0x0fff - ((section_virtual_size + 0x0fff) & 0x0fff);

		// image_virtual_size is the virtual address of the code section
		// plus the virtual size we need it to be
		unsigned int image_virtual_size = scn2Header->VirtualAddress + section_virtual_size_aligned;
		unsigned int image_virtual_size_aligned = image_virtual_size + 0x0fff - ((image_virtual_size + 0x0fff) & 0x0fff);

		// set values in headers
		optHeader->SizeOfImage = image_virtual_size_aligned;
		scn2Header->Misc.VirtualSize = section_virtual_size_aligned;
		scn2Header->SizeOfRawData = section_real_size_aligned;

		// align image on file alignment (0x200)
		stub.insert(stub.end(), image_real_size_aligned - image_real_size, 0);
	}
	cout << " done\n";

	cout << "- writing patch executable ";
	{
		ofstream to;

		// open output file by argument or default name
		if (outfilearg)
			to.open(argv[outfilearg], ios::binary);
		else
			to.open("patch.exe", ios::binary);

		if (!to) main_error("unable to open output file");

		// write patch executable
		to.write(&stub[0], stub.size() );
	}
	cout << '(' << stub.size() << " bytes)\n";

	cout << "- done\n";

	return 0;
}
