//
// apatch - simple patching engine
//
// STATEMENT struct
//
// Copyright 1999-2014 Joergen Ibsen
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef STATEMENT_HPP_INCLUDED
#define STATEMENT_HPP_INCLUDED

#include <vector>
#include <string>

struct STATEMENT {

	STATEMENT() : next(0) {
		/* nothing */
	}

	~STATEMENT() {
		if (next) delete next;
	}

	enum { labelK, quitK, titleK, dlgsizeK, colorsK, printK, clsK, startK,
	       gotoK, onerrorK, openK, backupK, offsetK, sizeK, crcK, writeK,
	       testK, searchK, yesnoK, endK, attribK, compareK, replaceK,
	       importK, regcreateK, regsetK, regdelK
	     } kind;

	std::string S1, S2;
	std::vector<char> D1, D2;
	unsigned int I1, I2;

	STATEMENT *target;

	int csize, dsize;

	int cpos, dpos;

	int lineno;

	STATEMENT *next;
};

#endif // STATEMENT_HPP_INCLUDED
