//
// apatch - simple patching engine
//
// file comparison
//
// Copyright 1999-2014 Joergen Ibsen
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef FILECMP_HPP_INCLUDED
#define FILECMP_HPP_INCLUDED

#include <vector>

namespace apatch {

struct diff_t {
	size_t offset;
	std::vector<char> bytes;
};

std::vector<diff_t> file_compare(const char *name1, const char *name2);

std::vector<diff_t> file_compare_fc(const char *name);

}

#endif // FILECMP_HPP_INCLUDED
