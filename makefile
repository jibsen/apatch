##
## apatch - simple patching engine
##
## apatch makefile (NMAKE)
##
## Copyright 1999-2014 Joergen Ibsen
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
## http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##

target = apatch.exe
objs   = apatch.obj scanner.obj parser.obj weeder.obj symbol.obj coder.obj \
         optimizer.obj resourcer.obj emitter.obj pprinter.obj lex.obj \
         util.obj crcfile.obj filecmp.obj remdead.obj regedit.obj
libs   = aplib.lib user32.lib

cflags	= /nologo /c /W2 /O2 /EHsc /GL
cvars	= /DNDEBUG
lflags	= /nologo /release /subsystem:console,5.01 /ltcg
llibs	= aplib.lib user32.lib

all: $(target)

.cpp.obj::
	$(CXX) $(cdebug) $(cflags) $(cvars) /MP $<

.asm.obj:
	fasm $< $@

jasg.exe: jasg.cpp
	$(CXX) /nologo /W2 /O2 /EHsc $**

lextab.inc: apatch.jasg jasg.exe
	jasg -caeqm apatch.jasg

$(target): $(objs)
	link $(ldebug) $(lflags) /out:$@ $** $(llibs)
	mt -manifest apatch.manifest -outputresource:apatch.exe;1

clean:
	del /Q $(objs) $(target) lextab.inc jasg.exe jasg.obj

# dependencies
apatch.obj    : apatch.hpp script.hpp stub/stub.h
scanner.obj   : scanner.hpp lex.h lextab.inc
parser.obj    : parser.hpp script.hpp statement.hpp
weeder.obj    : weeder.hpp script.hpp statement.hpp
symbol.obj    : symbol.hpp script.hpp statement.hpp
coder.obj     : coder.hpp script.hpp statement.hpp filecmp.hpp
optimizer.obj : optimizer.hpp script.hpp statement.hpp
resourcer.obj : resourcer.hpp script.hpp statement.hpp
emitter.obj   : emitter.hpp script.hpp statement.hpp
pprinter.obj  : pprinter.hpp script.hpp statement.hpp
lex.obj       : lextab.inc
util.obj      : util.hpp
crcfile.obj   : crcfile.hpp
filecmp.obj   : filecmp.hpp
remdead.obj   : remdead.hpp
regedit.obj   : regedit.hpp
