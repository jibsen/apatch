//
// apatch - simple patching engine
//
// recourser
//
// Copyright 1999-2014 Joergen Ibsen
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include <iostream>
#include <string>
#include <cstdio>

#include "resourcer.hpp"

#include "util.hpp"
#include "crcfile.hpp"

namespace {

using namespace std;
using namespace apatch;

int cpos;
int dpos;

// error handler
void resource_error(char *what)
{
	printf("\n\007[r] error : %s\n", what);

	exit(-1);
}

// warning handler
void resource_warning(char *what)
{
	printf("[r] warning : %s\n", what);
}

void resource_label(STATEMENT *s)
{
	s->csize = 0;
	s->dsize = 0;
	s->cpos = cpos;
	s->dpos = dpos;

	cpos += s->csize;
	dpos += s->dsize;
}

void resource_quit(STATEMENT *s)
{
	s->csize = 1;
	s->dsize = 0;
	s->cpos = cpos;
	s->dpos = dpos;

	cpos += s->csize;
	dpos += s->dsize;
}

void resource_title(STATEMENT *s)
{
	s->csize = 5;
	s->dsize = s->S1.size() + 1;
	s->cpos = cpos;
	s->dpos = dpos;

	cpos += s->csize;
	dpos += s->dsize;
}

void resource_dlgsize(STATEMENT *s)
{
	s->csize = 0;
	s->dsize = 0;
	s->cpos = cpos;
	s->dpos = dpos;

	cpos += s->csize;
	dpos += s->dsize;
}

void resource_colors(STATEMENT *s)
{
	s->csize = 0;
	s->dsize = 0;
	s->cpos = cpos;
	s->dpos = dpos;

	cpos += s->csize;
	dpos += s->dsize;
}

void resource_print(STATEMENT *s)
{
	s->csize = 5;
	s->dsize = s->S1.size() + 1;
	s->cpos = cpos;
	s->dpos = dpos;

	cpos += s->csize;
	dpos += s->dsize;
}

void resource_cls(STATEMENT *s)
{
	s->csize = 1;
	s->dsize = 0;
	s->cpos = cpos;
	s->dpos = dpos;

	cpos += s->csize;
	dpos += s->dsize;
}

void resource_start(STATEMENT *s)
{
	s->csize = 1;
	s->dsize = 0;
	s->cpos = cpos;
	s->dpos = dpos;

	cpos += s->csize;
	dpos += s->dsize;
}

void resource_goto(STATEMENT *s)
{
	s->csize = 5;
	s->dsize = 0;
	s->cpos = cpos;
	s->dpos = dpos;

	cpos += s->csize;
	dpos += s->dsize;
}

void resource_onerror(STATEMENT *s)
{
	s->csize = 5;
	s->dsize = 0;
	s->cpos = cpos;
	s->dpos = dpos;

	cpos += s->csize;
	dpos += s->dsize;
}

void resource_attrib(STATEMENT *s)
{
	s->csize = 13;
	s->dsize = s->S1.size() + 1;
	s->cpos = cpos;
	s->dpos = dpos;

	cpos += s->csize;
	dpos += s->dsize;
}

void resource_open(STATEMENT *s)
{
	s->csize = 5;
	if ((s->I2 == 0) || (s->I2 == 2))
		s->dsize = s->S1.size() + 1;
	else
		s->dsize = 0;
	s->cpos = cpos;
	s->dpos = dpos;

	cpos += s->csize;
	dpos += s->dsize;
}

void resource_backup(STATEMENT *s)
{
	s->csize = 5;
	if (s->I1 == 1)
		s->dsize = s->S1.size() + 1;
	else
		s->dsize = 0;
	s->cpos = cpos;
	s->dpos = dpos;

	cpos += s->csize;
	dpos += s->dsize;
}

void resource_offset(STATEMENT *s)
{
	s->csize = 5;
	s->dsize = 0;
	s->cpos = cpos;
	s->dpos = dpos;

	cpos += s->csize;
	dpos += s->dsize;
}

void resource_size(STATEMENT *s)
{
	s->csize = 5;
	s->dsize = 0;
	s->cpos = cpos;
	s->dpos = dpos;

	cpos += s->csize;
	dpos += s->dsize;
}

void resource_crc(STATEMENT *s)
{
	s->csize = 5;
	s->dsize = 0;
	s->cpos = cpos;
	s->dpos = dpos;

	cpos += s->csize;
	dpos += s->dsize;
}

void resource_write(STATEMENT *s)
{
	s->csize = 9;
	s->dsize = s->D1.size();
	s->cpos = cpos;
	s->dpos = dpos;

	cpos += s->csize;
	dpos += s->dsize;
}

void resource_test(STATEMENT *s)
{
	s->csize = 9;
	s->dsize = s->D1.size();
	s->cpos = cpos;
	s->dpos = dpos;

	cpos += s->csize;
	dpos += s->dsize;
}

void resource_search(STATEMENT *s)
{
	s->csize = 9;
	s->dsize = s->D1.size();
	s->cpos = cpos;
	s->dpos = dpos;

	cpos += s->csize;
	dpos += s->dsize;
}

void resource_yesno(STATEMENT *s)
{
	s->csize = 5;
	s->dsize = 0;
	s->cpos = cpos;
	s->dpos = dpos;

	cpos += s->csize;
	dpos += s->dsize;
}

void resource_regcreate(STATEMENT *s)
{
	s->csize = 13;
	s->dsize = s->S1.size() + 1;
	s->cpos = cpos;
	s->dpos = dpos;

	cpos += s->csize;
	dpos += s->dsize;
}

void resource_regset(STATEMENT *s)
{
	s->csize = 13;
	switch (s->I1)
	{
	case 0:
		s->dsize = 4;
		break;
	case 1:
		s->dsize = s->S2.size() + 1;
		break;
	case 2:
		s->dsize = s->D2.size();
		break;
	}
	s->dsize += s->S1.size() + 1;
	s->cpos = cpos;
	s->dpos = dpos;

	cpos += s->csize;
	dpos += s->dsize;
}

void resource_regdel(STATEMENT *s)
{
	s->csize = 5;
	s->dsize = s->S1.size() + 1;
	s->cpos = cpos;
	s->dpos = dpos;

	cpos += s->csize;
	dpos += s->dsize;
}

void resource_end(STATEMENT *s)
{
	s->csize = 1;
	s->dsize = 0;
	s->cpos = cpos;
	s->dpos = dpos;

	cpos += s->csize;
	dpos += s->dsize;
}

void resource_statement(STATEMENT *s)
{
	if (s == 0) return;

	if (s->next) resource_statement(s->next);

	switch (s->kind) {
	case STATEMENT::labelK:
		resource_label(s);
		break;
	case STATEMENT::quitK:
		resource_quit(s);
		break;
	case STATEMENT::titleK:
		resource_title(s);
		break;
	case STATEMENT::dlgsizeK:
		resource_dlgsize(s);
		break;
	case STATEMENT::colorsK:
		resource_colors(s);
		break;
	case STATEMENT::printK:
		resource_print(s);
		break;
	case STATEMENT::clsK:
		resource_cls(s);
		break;
	case STATEMENT::startK:
		resource_start(s);
		break;
	case STATEMENT::gotoK:
		resource_goto(s);
		break;
	case STATEMENT::onerrorK:
		resource_onerror(s);
		break;
	case STATEMENT::attribK:
		resource_attrib(s);
		break;
	case STATEMENT::openK:
		resource_open(s);
		break;
	case STATEMENT::backupK:
		resource_backup(s);
		break;
	case STATEMENT::offsetK:
		resource_offset(s);
		break;
	case STATEMENT::sizeK:
		resource_size(s);
		break;
	case STATEMENT::crcK:
		resource_crc(s);
		break;
	case STATEMENT::compareK:
		resource_error("unhandled compare");
		break;
	case STATEMENT::writeK:
		resource_write(s);
		break;
	case STATEMENT::testK:
		resource_test(s);
		break;
	case STATEMENT::searchK:
		resource_search(s);
		break;
	case STATEMENT::replaceK:
		resource_error("unhandled replace");
		break;
	case STATEMENT::yesnoK:
		resource_yesno(s);
		break;
	case STATEMENT::importK:
		resource_error("unhandled import");
		break;
	case STATEMENT::regcreateK:
		resource_regcreate(s);
		break;
	case STATEMENT::regsetK:
		resource_regset(s);
		break;
	case STATEMENT::regdelK:
		resource_regdel(s);
		break;
	case STATEMENT::endK:
		resource_end(s);
		break;
	default:
		resource_error("unknown statement type");
		break;
	}
}

}

void apatch::resourcer::resource(SCRIPT *script)
{
	cpos = 1; // make room for initial quit
	dpos = 1; // make sure zero can be used as 'no param'

	resource_statement(script->statements);
}
